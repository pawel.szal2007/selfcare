// =====================================================================================

const { BigQuery } = require("@google-cloud/bigquery");
const bigquery = new BigQuery();

exports.getTableColumns = (scope) => {
  return new Promise(async (resolve, reject) => {
    const query = `SELECT * FROM users.INFORMATION_SCHEMA.COLUMNS WHERE table_name="user_${scope}"`;
    console.log({
      query,
    });
    const options = {
      query: query,
    };
    const [job] = await bigquery.createQueryJob(options);
    console.log(`Job ${job.id} started.`);
    // Wait for the query to finish
    const [rows] = await job.getQueryResults();

    let col_arr = [];
    if (rows) {
      for (const row of rows) {
        if (
          row.column_name != "timestamp" &&
          row.column_name != "user_id" &&
          row.column_name != "updated_at" &&
          row.column_name != "loa" &&
          row.column_name != "source"
        ) {
          col_arr.push(row.column_name);
        }
      }
    }

    col_arr.length > 0
      ? resolve(col_arr)
      : reject({
          error: 404,
          details: "scope table not found, no data found",
        });
  });
};

// get up to date one label record from table... or tables
exports.getLabel = (userId, label, tabel = "all") => {
  return new Promise(async (resolve, reject) => {
    const tables = require("../configuration").projectVariable
      .BIGQUERY_SCOPE_TABLES;
    if (tabel != "all") {
      const td = tables[tabel];
      if (td) {
        const query = `SELECT ${label} FROM (SELECT timestamp, ${label}
      , RANK() OVER(ORDER BY timestamp DESC) rank
 FROM ${td} WHERE user_id="${userId}") WHERE preferred_username!="" ORDER BY rank`;
        console.log({
          query,
        });
        const options = {
          query: query,
        };
        // Run the query as a job
        const [job] = await bigquery.createQueryJob(options);
        console.log(`Job ${job.id} started.`);
        // Wait for the query to finish
        const [row] = await job.getQueryResults();
        console.log({
          row,
        });
        resolve(row);
      } else
        reject(
          `table name: '${tabel}' doesn't exist in configuration file....`
        );
    } else {
      // searching all tables to find the proper label...
      const scopes = require("../configuration").projectVariable
        .BIGQUERY_SCOPES;
      let obj = [];
      for await (let scope of scopes) {
        const query = `SELECT ${label} FROM (SELECT timestamp, ${label}
      , RANK() OVER(ORDER BY timestamp DESC) rank
 FROM ${td} WHERE user_id="${userId}") WHERE preferred_username!="" ORDER BY rank`;
        const options = {
          query: query,
        };
        const [job] = await bigquery.createQueryJob(options);
        console.log(`Job ${job.id} started.`);
        // Wait for the query to finish
        const [rows] = await job.getQueryResults();
        rows && (obj = obj.concat(rows));
      }
      console.log({
        obj,
      });
      resolve(obj);
    }
  });
};

exports.getAllData = (userId) => {
  return new Promise(async (resolve) => {
    const scopes = require("../configuration").projectVariable.BIGQUERY_SCOPES;
    const tables = require("../configuration").projectVariable
      .BIGQUERY_SCOPE_TABLES;
    let data_obj = [];
    for await (const scope of scopes) {
      const query = `SELECT * FROM ${
        tables[scope.toUpperCase()]
      } WHERE user_id='${userId}'`;
      console.log({
        query,
      });
      const options = {
        query: query,
      };
      // Run the query as a job
      const [job] = await bigquery.createQueryJob(options);
      console.log(`Job ${job.id} started.`);
      // Wait for the query to finish
      const [rows] = await job.getQueryResults();
      // console.log("ALL DATA FOR SCOPE: ", `${scope}`);
      // console.log({ rows });
      data_obj = [...data_obj, ...rows];
    }
    // console.log({ data_obj });
    resolve(data_obj);
  });
};

exports.selectUpToDateAllData = (userID) => {
  return new Promise(async (resolve) => {
    const scopes = require("../configuration").projectVariable.BIGQUERY_SCOPES;
    // const tables = require("../configuration").projectVariable
    //   .BIGQUERY_SCOPE_TABLES;
    let data_obj = {};
    for await (const scope of scopes) {
      const upToDateRow = await selectUserScopeBigQueryData(userID, scope);
      if (upToDateRow)
        data_obj = {
          ...data_obj,
          ...upToDateRow,
        };
    }
    resolve(data_obj);
  });
};

// cyber-id-prod.users.user_id_ocr
const selectUserScopeBigQueryData = (userID, scope_table) => {
  return new Promise(async (resolve, reject) => {
    console.log("selectUserScopeBigQueryData...");
    // const object_arr = [];
    // const scopes = require("../configuration").projectVariable.BIGQUERY_SCOPES;
    const tables = require("../configuration").projectVariable
      .BIGQUERY_SCOPE_TABLES;

    // for await (const table of table_arr) {
    const query = `SELECT * FROM ${
      tables[scope_table.toUpperCase()]
    } WHERE user_id='${userID}'`;
    console.log({
      query,
    });
    const options = {
      query: query,
    };
    // Run the query as a job
    const [job] = await bigquery.createQueryJob(options);
    console.log(`Job ${job.id} started.`);
    // Wait for the query to finish
    const [rows] = await job.getQueryResults();
    // we have few rows and need to parse to one row with the most actual data
    let bq_data;
    rows.length > 0 && (bq_data = joinRowsData(rows));
    // object_arr.push(joinedRows);
    // }
    // const bq_data = await joinData(object_arr);
    resolve(bq_data);
  });
};

const joinRowsData = (data) => {
  const o = {};
  const firstItem = data[0];
  for (const property in firstItem) {
    if (
      property != "timestamp" &&
      property != "user_id" &&
      property != "loa" &&
      property != "source" &&
      property != "updated_at" &&
      firstItem[property] != " " &&
      firstItem[property] != ""
    ) {
      o[property] = {};
      o[property]["updated_at"] = new Date(firstItem.timestamp.value).getTime();
      o[property]["value"] = firstItem[property];
      o[property]["loa"] = firstItem.loa;
      o[property]["source"] = firstItem.source;
    }
  }

  for (let i = 1; i < data.length; i++) {
    if (data[i] != "undefined") {
      const rowData = data[i];
      for (const property in rowData) {
        if (
          property != "timestamp" &&
          property != "user_id" &&
          property != "loa" &&
          property != "source" &&
          property != "updated_at" &&
          rowData[property] != " " &&
          rowData[property] != ""
        ) {
          const rowTimestamp = new Date(rowData.timestamp.value).getTime();
          // if (typeof rowData.timestamp === 'object') {
          // rowTimestamp =
          // } else rowTimestamp = rowData.timestamp;
          if (o[property] != undefined) {
            if (o[property].updated_at < rowTimestamp) {
              o[property].updated_at = new Date(
                rowData.timestamp.value
              ).getTime();
              o[property].value = rowData[property];
              o[property].loa = rowData.loa;
              o[property].source = rowData.source;
            }
          } else {
            o[property] = {};
            // if (typeof rowData.timestamp === 'object') {
            o[property].updated_at = new Date(
              rowData.timestamp.value
            ).getTime();
            o[property].value = rowData[property];
            o[property].loa = rowData.loa;
            o[property]["source"] = rowData.source;
          }
        }
      }
    }
  }
  console.log("========= NEW JOINED OBJECT ===========");
  console.log({
    o,
  });
  return o;
};

const joinData = async (data_arr) => {
  return new Promise((resolve) => {
    console.log({
      data_arr,
    });
    let obj = {};
    for (let i = 0; i < data_arr.length; i++) {
      const data = data_arr[i];
      for (const property in data) {
        if (obj[property] != undefined) {
          if (obj[property].updated_at < data[property].updated_at) {
            obj[property] = data[property];
          }
        } else {
          obj[property] = data[property];
        }
      }
    }
    console.log("============= FINAL OBJECT ===============");
    console.log({
      obj,
    });
    resolve(obj);
  });
};

// Imports the Google Cloud client library
const { PubSub } = require("@google-cloud/pubsub");
const pubSubClient = new PubSub();

exports.publishToTopic = (userID, topic) => {
  return new Promise(async (resolve, reject) => {
    const topicName = topic.topicName;
    const dataBuffer = Buffer.from(JSON.stringify(topic.data));
    try {
      const messageId = await pubSubClient.topic(topicName).publish(dataBuffer);
      console.log(`Message ${messageId} published.`);
      resolve();
    } catch (error) {
      console.error(`Received error while publishing: ${error.message}`);
      process.exitCode = 1;
      reject(error);
    }
  });
};

// topics - array with obj
exports.publishMultipleToTopics = (userID, scopesObj) => {
  return new Promise(async (resolve) => {
    console.log("[publish multiple to topics]");
    for await (const scope of Object.keys(scopesObj)) {
      const topicName = scopesObj[scope].topicName;
      const dataBuffer = Buffer.from(JSON.stringify(scopesObj[scope].data));
      try {
        const messageId = await pubSubClient
          .topic(topicName)
          .publish(dataBuffer);
        console.log(`Message ${messageId} published.`);
      } catch (error) {
        console.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
      }
    }
    resolve();
  });
};

exports.selectUserScopeBigQueryData = selectUserScopeBigQueryData;
