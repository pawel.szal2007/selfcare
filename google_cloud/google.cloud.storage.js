const Storage = require("@google-cloud/storage");
const storage = new Storage.Storage();
const path = require("path");

// {keyFileName: "key.json"}

// frontend-test-presentation     ps-frontend-test
const bucketDestinationName = "ps-frontend-test";

exports.getUserFiles = (userID) => {
  return new Promise(async (resolve, reject) => {
    try {
      const obj = [];
      const [files] = await storage
        .bucket(`gs://${bucketDestinationName}/`)
        .getFiles({
          prefix: `${userID}/json_data/`, //tu moze jakeis ID uzytkownika zamiast pawel_s, najlepiej przekazywane z tokenu jakies id
        });
      console.log({ files });
      if (files && files.length > 0) {
        for (const file of files) {
          let loa, file_url, source;

          // source of data
          source = file.name.split("/")[2];

          // loa of data based on source
          if (
            source === "gov_rdo" ||
            source === "gov_nl" ||
            source === "gov_pesel"
          ) {
            loa = "high";
          } else loa = "medium";

          // add object
          obj.push({
            loa: loa,
            path: file.name,
            source: source,
            updated_at: file.metadata.timeCreated,
          });
        }
      }
      resolve(obj);
    } catch (err) {
      reject(err);
    }
  });
};

exports.uploadFileToBucket = (userID, service, file) => {
  return new Promise(async (resolve, reject) => {
    console.log("[UPLOAD FILE TO STORAGE BUCKET]", {
      file,
    });
    await storage
      .bucket(`gs://${bucketDestinationName}/`)
      .upload(file.tempFilePath, {
        destination: `${userID}/${service}/${file.type}/${file.name}`,
      })
      .then(() => {
        console.log(
          `${file.tempFilePath} uploaded to gs://${bucketDestinationName}/`
        );
        resolve({
          bucketPath: `${userID}/${service}/${file.type}/${file.name}`,
          bucketName: bucketDestinationName,
        });
      });
  });
};

exports.downloadFile = (filePath) => {
  return new Promise(async (resolve, reject) => {
    const filePathSplit = filePath.split("/");
    const destFileFolder = path.join(
      __dirname,
      "..",
      "assets",
      "public",
      "gcs_files",
      filePathSplit[0]
    );
    const fs = require("fs");
    // check if directory exists
    if (!fs.existsSync(destFileFolder)) {
      fs.mkdirSync(destFileFolder);
    }

    try {
      const destFileName = `${destFileFolder}/${
        filePathSplit[filePathSplit.length - 1]
      }`;
      const options = {
        destination: destFileName,
      };

      await storage
        .bucket(bucketDestinationName)
        .file(filePath)
        .download(options);
      // console.log(`gs://${bucket}/${filePath} downloaded to ${destFileName}.`);
      resolve(destFileName);
    } catch (err) {
      // console.log(err);
      reject(err);
    }
  });
};
