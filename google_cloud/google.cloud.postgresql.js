// ============ CLOUD SQL POSTGRES ============
let knex;
const uuid_generator = require("uuid");

const createTcpPool = async () => {
  // Extract host and port from socket address
  const dbSocketAddr = process.env.DB_HOST.split(":"); // e.g. '127.0.0.1:5432'

  // Establish a connection to the database
  try {
    knex = require("knex")({
      client: "pg",
      connection: {
        user: process.env.DB_USER, // e.g. 'my-user'
        password: process.env.DB_PASS, // e.g. 'my-user-password'
        database: process.env.DB_NAME, // e.g. 'my-database'
        host: dbSocketAddr[0], // e.g. '127.0.0.1'
        port: dbSocketAddr[1], // e.g. '5432'
      },
      pool: {
        max: 5,
        createRetryIntervalMillis: 200,
        acquireTimeoutMillis: 60000,
        createTimeoutMillis: 30000,
        idleTimeoutMillis: 600000,
      },
    });
  } catch (err) {
    throw err;
  }
};

const SELECT = (obj) => {
  return obj?.attr
    ? knex.select(obj.attr).from(obj.table)
    : knex.select().from(obj.table);
};

const selectUserPhoneNumbers = (obj) => {
  return knex
    .select("phone_number")
    .from("phones")
    .where("user_id", obj.user_id);
};

const selectAllPhoneNumbers = () => {
  return knex.select("phone_number").from("phones");
};

const selectUserEmails = (obj) => {
  return knex
    .select("email_address", "is_verified")
    .from("emails")
    .where("user_id", obj.user_id);
};

const selectUserPassword = (obj) => {
  return knex.select("password").from("users").where("user_id", obj.user_id);
};

const selectUserQuestion = (obj) => {
  return knex
    .select("answer")
    .from("security_questions")
    .where({ user_id: obj.user_id, question: obj.question });
};

const selectUserPIN = (obj) => {
  return knex.select("pin").from("users").where("user_id", obj.user_id);
};

const isBiometryEnrolled = (obj) => {
  return knex
    .select("is_biometry_enrolled")
    .from("users")
    .where("user_id", obj.user_id);
};

const selectUserSecurityQuestions = (obj) => {
  return knex
    .select("question")
    .from("security_questions")
    .where("user_id", obj.user_id);
};

const addUserPhoneNumber = (obj) => {
  return knex("phones").insert({
    phone_id: uuid_generator.v1(),
    user_id: obj.user_id,
    phone_number: obj.phone_number,
  });
};

const addUserSecurityQuestions = (obj) => {
  const user_id = obj.user_id;
  const data = [];
  for (const qn of obj.questions) {
    data.push({
      question_id: uuid_generator.v1(),
      user_id: user_id,
      question: qn.question,
      answer: qn.answer,
    });
  }
  return knex("security_questions").insert(data);
};

const deleteUserEmail = (obj) => {
  return knex("emails")
    .where("user_id", obj.user_id)
    .where("email_address", obj.email)
    .del();
};

const deleteUserPhone = (obj) => {
  return knex("phones")
    .where("user_id", obj.user_id)
    .where("phone_number", obj.phone_number)
    .del();
};

const setUserPIN = (obj) => {
  return knex("users").where("user_id", obj.user_id).update("pin", obj.pin);
};

const deletePIN = (obj) => {
  return knex("users").update({ pin: "" }).where("user_id", obj.user_id);
};

const deletePassword = (obj) => {
  return knex("users").update({ password: "" }).where("user_id", obj.user_id);
};

const deleteSecurityQuestion = (obj) => {
  return knex("security_questions")
    .delete()
    .where({ user_id: obj.user_id, question: obj.question });
};

const updateUserQuestion = (obj) => {
  return knex("security_questions")
    .update({ question: obj.question, answer: obj.answer })
    .where("user_id", obj.user_id)
    .where("question", obj.replaced);
};

const setUserPassword = (obj) => {
  return knex("users")
    .where("user_id", obj.user_id)
    .update("password", obj.password);
};

const setUserBiometryStatus = (obj) => {
  return knex("users")
    .where("user_id", obj.user_id)
    .update("is_biometry_enrolled", obj.isBiometryEnrolled);
};

const closeConnection = () => {
  knex.destroy();
  knex = null;
  return;
};

exports.connect = createTcpPool;
exports.select = SELECT;
exports.userPhones = selectUserPhoneNumbers;
exports.selectAllPhoneNumbers = selectAllPhoneNumbers;
exports.userEmails = selectUserEmails;
exports.userPassword = selectUserPassword;
exports.userQuestion = selectUserQuestion;
exports.userPIN = selectUserPIN;
exports.isBiometryEnrolled = isBiometryEnrolled;
exports.userSecurityQuestions = selectUserSecurityQuestions;
exports.addPhone = addUserPhoneNumber;
exports.addSecurityQuestions = addUserSecurityQuestions;
exports.deleteEmail = deleteUserEmail;
exports.deletePhone = deleteUserPhone;
exports.setPIN = setUserPIN;
exports.setPassword = setUserPassword;
exports.deletePIN = deletePIN;
exports.deletePassword = deletePassword;
exports.deleteSecurityQuestion = deleteSecurityQuestion;
exports.updateUserQuestion = updateUserQuestion;
exports.setUserBiometryStatus = setUserBiometryStatus;
exports.close = closeConnection;
// exports.test = test;
