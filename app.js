//jshint esversion:6
require("dotenv").config();
const { projectVariable } = require("./configuration");
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const ejs = require("ejs");
const bcrypt = require("bcrypt");
const gcBigquery = require("./google_cloud/google.cloud.bigquery");
const gcPostgresql = require("./google_cloud/google.cloud.postgresql");
const gcStorage = require("./google_cloud/google.cloud.storage");
const requestCountry = require("request-country");
const jwtDecode = require("jwt-decode");
const _ = require("lodash");
var https = require("https");
const fs = require("fs");
const saltRounds = 10;
const {
  language,
} = require(`${__dirname}/language_dictionary/lang_dictionary.js`);
const fileUpload = require("express-fileupload");
const WebSocket = require("./websocket/websocket");

const app = express();
let ws;

app.set("view engine", "ejs");

app.set("views", __dirname + "/assets/views");

app.use(
  bodyParser.urlencoded({
    limit: "100mb",
    parameterLimit: 50000,
    extended: true,
  })
);

app.use(
  bodyParser.json({
    limit: "100mb",
  })
);

app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: "./tmp/",
  })
);

app.use(bodyParser.json());

app.use(cookieParser());

app.use(express.static("assets/public"));

app.use(
  cors({
    origin: "http://localhost:8000",
    credentials: true,
    exposedHeaders: ["Content-Disposition"],
  })
);

// is user authenticated
const isAuthenticated = (req, res, next) => {
  if (req.cookies.tokens) {
    // console.log("[user logged in]");
    next();
  } else {
    // console.log("[user not logged in]");
    res.redirect("/");
  }
};

const getLanguage = (req) => {
  let lang;
  if (req.query.lang) {
    lang = req.query.lang;
  } else if (req.headers["accept-language"]) {
    lang = req.headers["accept-language"].split(",")[0].split("-")[0];
    if (lang != "pl" && lang != "en") {
      lang = "en";
    }
  } else lang = "pl";

  console.log("[browser language]", { lang });
  return lang;
};

const saveTokens = (response) => {
  const token_type = response.data.token_type;
  const access_token = response.data.access_token;
  const expires_in = response.data.expires_in;
  const scope = response.data.scope;
  const refresh_token = response.data.refresh_token;
  const id_token = response.data.id_token.id_token
    ? response.data.id_token.id_token
    : response.data.id_token;

  console.log(
    { token_type },
    { access_token },
    { expires_in },
    { scope },
    { refresh_token },
    { id_token }
  );

  const data = {
    token_type: token_type,
    access_token: access_token,
    expires_in: expires_in,
    scope: scope,
    refresh_token: refresh_token,
    id_token: id_token,
  };

  const jwt = require("jsonwebtoken");
  const tokens = jwt.sign(data, process.env.SECRET);
  return tokens;
};

const userIdToken = (userId) => {
  const jwt = require("jsonwebtoken");
  const data = {
    userId: userId,
  };
  const token = jwt.sign(data, process.env.SECRET);
  return token;
};

const refreshTokenRequest = (req) => {
  return new Promise((resolve, reject) => {
    console.log("[REFRESH TOKEN REQUEST]");
    // const { projectVariable } = require("./configuration");
    const refresh_token = jwtDecode(req.cookies.tokens).refresh_token;
    console.log({
      refresh_token,
    });
    console.log("[refresh token axios post]");
    const axios = require("axios");
    axios
      .post(
        `${projectVariable.OPEN_ID_REFRESH_TOKEN}&refresh_token=${refresh_token}`
      )
      .then((response) => {
        console.log("[refresh token response]", response.data);
        resolve(response);
      })
      .catch((err) => {
        console.log("[refresh token error]", err);
        reject(err);
      });
  });
};

// LOGIN
app.get("/", async (req, res) => {
  const lang = getLanguage(req);
  if (req.cookies.tokens) {
    res.redirect(`/main?lang=${lang}#home`);
  } else {
    res.render("login/login");
  }
});

// https://ssotest.phoneid.co/auth?client_id=cyber_test&redirect_uri=https://selfcare.topid.org/auth&response_type=code&scope=profile&nonce=12345
app.get("/loginWithOpenID", (req, res) => {
  res.redirect(projectVariable.OPEN_ID_CODE_ENDPOINT_LOGIN);
});

app.get("/registerWithOpenID", (req, res) => {
  res.redirect(projectVariable.OPEN_ID_CODE_ENDPOINT_REGISTER);
});

app.get("/auth", (req, res) => {
  const code = req.query.code;

  console.log({ code });
  console.log("[AUTHORIZATION GRANT REQUEST]");
  const https = require("https");
  const axios = require("axios");
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

  axios
    .post(`${projectVariable.OPEN_ID_AUTHORIZATION_GRANT_ENDPOINT}`, {
      client_id: "cyber_test",
      code: code,
      redirect_uri: "https://selfcare.topid.org/auth",
      grant_type: "authorization_code",
      client_secret: "abc",
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    })
    .then((response) => {
      console.log("[AUTHORIZATION GRANT RESPONSE]", { response });
      const tokens = saveTokens(response);
      console.log({ tokens });
      res.cookie("tokens", tokens);
      // save user id ---> userIdToken(userId) zwróci mi token ktory zapisuje do cookiesa pod "id"
      const userId = jwtDecode(response.data.id_token).user_id;
      const idToken = userIdToken(userId);
      res.cookie("id", idToken);
      res.redirect(`${projectVariable.VM_URL}/main`);
    })
    .catch((err) => {
      console.log("[AUTHORIZATION GRANT ERROR]", { err });
    });
});

app.get("/nlgov", (req, res) => {
  res.sendFile("assets/views/mijnOverheidIdentiteitBasis.html", {
    root: __dirname,
  });
});

app.get("/zoneinfo", (req, res) => {
  const countryCode = requestCountry(req);
  return res.status(200).send(countryCode);
});

/*
 **  POST w body: userId, sessionId, redirectUri, scope, loa
 **
 **
 */
app.post("/businessClient", async (req, res) => {
  try {
    let scopes = req.body.scope;
    scopes && (scopes = scopes.split(" "));
    const loa = req.body.loa;
    const userId = req.body.userId;
    const sessionId = req.body.sessionId;
    const redirectUri = req.body.redirectUri;
    const lang = getLanguage(req);

    console.log(
      { scopes },
      { loa },
      { userId },
      { sessionId },
      { redirectUri },
      { lang }
    );

    if (!scopes) return res.json({ error: 400, info: "body: scope not found" });
    if (!sessionId)
      return res.json({ error: 400, info: "body: sessionId not found" });
    if (!redirectUri)
      return res.json({ error: 400, info: "body: redirectUri not found" });
    if (!loa) return res.json({ error: 400, info: "body: loa not found" });
    if (!userId)
      return res.json({ error: 400, info: "body: userId not found" });
    if (loa != "low" && loa != "medium" && loa != "high")
      return res.json({
        error: 400,
        info: `loa: ${loa} is not recognized. Allowed: low, medium, high`,
      });
    if (
      !scopes.includes("profile") &&
      !scopes.includes("profile_extended") &&
      !scopes.includes("shopping") &&
      !scopes.includes("identity")
    ) {
      return res.json({
        error: 400,
        info: `scope: ${scopes} is not recognized. Allowed: profile, profile_extended, shopping, identity`,
      });
    }

    const newObj = [];
    const cookieData = [];
    for await (let scope of scopes) {
      const userData = await gcBigquery.selectUserScopeBigQueryData(
        userId,
        scope
      );

      console.log({ userData });

      const scope_columns = await gcBigquery.getTableColumns(scope);
      console.log({ scope_columns });

      const createCookiesData = (scope_names_arr, data) => {
        const cookie_obj = [];
        for (const name of scope_names_arr) {
          cookie_obj.push({
            scope: language.eng.scope[scope],
            dataLabel: name,
            dataValue: data && data[name] ? `${data[name].value}` : "",
            loa: data && data[name] ? data[name].loa : "unknown",
            updated_at: data && data[name] ? `${data[name].updated_at}` : 0,
          });
        }
        return cookie_obj;
      };

      const cookie = createCookiesData(scope_columns, userData);
      cookieData.concat(cookie);

      if (cookie) {
        for (const item of cookie) {
          if (!newObj[item.scope]) newObj[item.scope] = [];
          newObj[item.scope].push(item);
        }
      }
    }

    const data64 = Buffer.from(JSON.stringify(cookieData)).toString("base64");
    console.log({ data64 });

    const clientData = {
      sessionId: sessionId,
      redirectUri: redirectUri,
    };
    const clientData64 = Buffer.from(JSON.stringify(clientData)).toString(
      "base64"
    );

    console.log({ newObj });
    res.cookie("client", clientData64);
    res.cookie("clipboard", data64);
    const idToken = userIdToken(userId);
    res.cookie("id", idToken);
    return res.status(200).render("businessClient/clipboard-view", {
      data: newObj,
      dictionary: language[lang],
      loa_min: loa,
    });
  } catch (err) {
    return res.send(err);
  }
});

app.post("/businessClientUpdate", (req, res) => {
  // console.log("clipboard data.... express js", req.body.data);
  const lang = req.query.lang;
  const loa_min = req.body.data.loa_min;
  const newObj = [];
  const data = req.body.data.data;

  console.log({ data });

  if (data) {
    for (const item of data) {
      if (!newObj[item.scope]) newObj[item.scope] = [];
      newObj[item.scope].push(item);
    }
    for (const [key, value] of Object.entries(newObj)) {
      console.log({ key });
      console.log({ value });
    }
  }

  res.status(200).render("businessClient/content", {
    data: newObj,
    dictionary: language[lang],
    loa_min: loa_min,
  });
});

const getUsername = (userId) => {
  return new Promise(async (resolve) => {
    const record = await gcBigquery.getLabel(
      userId,
      "preferred_username",
      "PROFILE"
    );
    let username = record[0]?.preferred_username;
    console.log({ username });
    if (!username) {
      // sprawdź bigquery z numerem telefonu i adres email
      try {
        await gcPostgresql.connect();

        try {
          // get emails
          const emails = await gcPostgresql.userEmails({ user_id: userId });
          console.log(
            `there's no preffered username in bigquery, i've checked emails: ${emails}`
          );
          if (!emails[0]?.email_address) {
            console.log(
              `there is no email address i will have to check phones...`
            );
            // get phones
            const phones = await gcPostgresql.userPhones({ user_id: userId });
            if (!phones[0]?.phone_number) {
              console.log(
                `there's no phones: ${phones}, i dont have any username to show...`
              );
            } else username = phones[0].phone_number;
          } else username = emails[0].email_address;
        } catch (err) {
          console.log(err);
        }
      } catch (err) {
        console.log(err);
      }

      try {
        await gcPostgresql.close();
      } catch (err) {
        console.log(err);
      }
    }
    resolve(username);
  });
};

// jwtDecode(jwtDecode(req.cookies.tokens).id_token).given_name + " " + jwtDecode(jwtDecode(req.cookies.tokens).id_token).family_name,
app.get("/main", isAuthenticated, async (req, res) => {
  // req.query.lang
  let lang = getLanguage(req);
  const userId = jwtDecode(req.cookies.id)?.userId;
  const username = await getUsername(userId);

  // check security questions if there is no one - show a dialaog
  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  const security_questions = await getAuthenticationFactors({
    user_id: userId,
    factors: ["security_questions"],
  });
  console.log({ security_questions });

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }
  // check cookies clipboard and delete them
  res.clearCookie("clipboard");
  res.status(200).render("main/home", {
    security_questions: security_questions,
    username: username ? username : "",
    dictionary: language[lang],
  });
});

app.post("/home", isAuthenticated, async (req, res) => {
  const lang = req.query.lang;
  const userId = jwtDecode(req.cookies.id).userId;
  const username = await getUsername(userId);
  res.status(200).render("main/partials/home", {
    username: username ? username : "",
    dictionary: language[lang],
  });
});

// LOADING
app.post("/loading", (req, res) => {
  const lang = req.query.lang;
  console.log("[language]", {
    lang,
  });
  res.status(200).render("loading/loading-inner", {
    loadingText: language[lang].loading,
  });
});

app.post("/clipboard", isAuthenticated, (req, res) => {
  // console.log("clipboard data.... express js", req.body.data);
  const lang = req.query.lang;
  const newObj = [];
  console.log(req.body.data);
  if (req.body.data) {
    for (const item of req.body.data) {
      if (!newObj[item.scope]) newObj[item.scope] = [];
      newObj[item.scope].push(item);
    }
    for (const [key, value] of Object.entries(newObj)) {
      console.log({ key });
      console.log({ value });
    }
  }

  res.status(200).render("main/partials/clipboard", {
    data: newObj,
    dictionary: language[lang],
  });
});

app.post("/clipboard/send", (req, res) => {
  const csvData = req.body.data.csv;
  const sender = req.body.data.sender;
  const recipient = req.body.data.recipient;
  const password = req.body.data.password;
  const base64Title = Buffer.from(jwtDecode(req.cookies.id).userId).toString(
    "base64"
  );
  const csvFilePath = `${base64Title}.csv`;
  const zipFilePath = `./zip_files/${base64Title}.zip`;

  const nodemailer = require("nodemailer");
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    auth: {
      user: projectVariable.ACCOUNT.EMAIL.USER,
      pass: projectVariable.ACCOUNT.EMAIL.PASSWORD,
    },
  });

  console.log({ csvData }, { sender }, { recipient }, { base64Title });

  try {
    const { parse } = require("json2csv");
    const csv = parse(csvData, {
      withBOM: true,
      excelStrings: false,
    });
    // console.log({ csv });
    fs.writeFile(csvFilePath, csv, "utf8", (err) => {
      if (err) {
        console.log(
          "Some error occured - file either not saved or corrupted file saved."
        );
        return res.status(500).send({
          status: 500,
          details: "csv file either not saved or corrupted",
        });
      } else {
        console.log("csv file saved!");
        const spawn = require("child_process").spawn;
        const zip = spawn("zip", ["-P", password, zipFilePath, csvFilePath]);
        zip.on("exit", (code) => {
          try {
            fs.unlinkSync(csvFilePath);
            console.log("csv file removed");
          } catch (err) {
            console.log("csv file not removed!");
          }

          if (code === 0) {
            transporter.sendMail(
              {
                from: sender,
                to: recipient,
                subject: `CyberID - dane użytkownika`,
                text: "W załączniku znajdują się gotowe dane.",
                html: `<b>W załączniku znajdują się gotowe dane do pobrania. Plik został zaszyfrowany hasłem podanym na platformie CyberID.</b>
                    <h4>Pozdrawiamy,</h4>
                    <p style="margin-top: 0.5rem">zespół CyberID</p>
                  `,
                attachments: [
                  {
                    path: zipFilePath,
                  },
                ],
              },
              (err, info) => {
                try {
                  fs.unlinkSync(zipFilePath);
                  console.log("zip file removed");
                } catch (err) {
                  console.error("zip file not removed!");
                }
                if (err) {
                  console.log("Error occurred. " + err.message);
                  return res.status(500).send({
                    status: 500,
                    details: "email was not send",
                    message: err.message,
                  });
                }
                console.log("Message sent: %s", info.messageId);
                return res
                  .status(200)
                  .send({ status: 200, details: "email was sent" });
              }
            );
          } else {
            return res.status(500).send({
              status: 500,
              details: "zip file was not saved successfully",
            });
          }
        });
      }
    });
  } catch (err) {
    return res
      .status(500)
      .send({ status: 500, details: "error not known", message: err });
  }
});

app.get("/path/:path", (req, res) => {
  try {
    const userId = jwtDecode(req.cookies.id).userId;
    const path = `${req.params.path.toUpperCase()}_URI`;
    res.send(`${projectVariable[path]}?id=${userId}`);
  } catch (err) {
    res.status(404).send({ error: 404, details: "path not found" });
  }
});

app.post("/krs/data", (req, res) => {
  try {
    const name = req.body.data.name;
    const surname = req.body.data.surname;
    const nip = req.body.data.nip;
    // const name = "Bartosz";
    // const surname = "Oleśny";
    const krsPersonId = req.body.data.krsPersonId;
    const checkedData = req.body.data.checkedData;
    const userId = jwtDecode(req.cookies.id).userId;

    if (!name)
      return res.json({ status: 400, details: "no name params found" });
    if (!surname)
      return res.json({
        status: 400,
        details: "cannot get user surname",
      });
    // if (!surname)
    //   return res.json({ status: 400, details: "cannot get user surname" });

    const krsURI = `${projectVariable.KRS_URI}`;
    const axios = require("axios");
    axios
      .get(krsURI, {
        params: {
          firstName: name,
          lastName: surname,
          // nip: nip,
          krsNumber: krsPersonId,
        },
      })
      .then(async (response) => {
        const response_data = response.data;
        console.log({ response_data });

        if (response_data.Profile) {
          response_data.Profile.loa = response_data.Loa;
          response_data.Profile.source = response_data.source;
          response_data.Profile.updated_at = response_data.updated_at;
        }

        const { convertToBigQuerySchema } = require("./mapper/bqMapper");
        const bq_data = convertToBigQuerySchema({
          userId: userId,
          data: response_data?.Profile,
          checkedData: checkedData,
        });

        console.log({ bq_data });
        return res.send({ status: 200, details: "okk", data: bq_data });
      })
      .catch((err) => {
        console.log({ err });
        return res.send(err);
      });
  } catch (err) {
    console.log({ err });
    return res.send(err);
  }
});

app.get("/vat/data", (req, res) => {
  try {
    // 012121071
    // req.params.regon
    const regon = req.query.regon;
    // req.params.nip
    // 5310005448
    const nip = req.query.nip;
    const userId = jwtDecode(req.cookies.id).userId;
    // const bank_account = req.params.bank_account;

    if (!regon && !nip)
      return res.json({
        status: 400,
        details: "no regon, NIP or bank account params found",
      });

    let vatObj;
    if (nip)
      vatObj = {
        uri: projectVariable.VAT_NIP_URI,
        params: {
          nip: nip,
        },
      };
    else if (regon)
      vatObj = {
        uri: projectVariable.VAT_REGON_URI,
        params: {
          regon: regon,
        },
      };

    console.log({ vatObj });
    const axios = require("axios");
    axios
      .get(vatObj.uri, {
        params: vatObj.params,
      })
      .then(async (response) => {
        const response_data = response.data.result;
        console.log({ response_data });
        const { convertToBigQuerySchema } = require("./mapper/bqMapper");
        const checkedData = await getUserSelectedData(userId);
        const bq_data = convertToBigQuerySchema({
          userId: userId,
          data: response_data,
          checkedData: checkedData,
        });
        console.log({ bq_data });
        return res.send({ status: 200, details: "okk", data: bq_data });
      })
      .catch((err) => {
        console.log({ err });
        return res.send(err);
      });
  } catch (err) {
    console.log({ err });
    return res.send(err);
  }
});

app.post("/ceidg/data", (req, res) => {
  try {
    // 012121071
    // req.params.regon
    const regon = req.body.data?.regon;
    // req.params.nip
    // 5310005448
    const nip = req.body.data?.nip;
    const checkedData = req.body.data?.checkedData;
    const userId = jwtDecode(req.cookies.id).userId;
    // const bank_account = req.params.bank_account;

    if (!regon && !nip)
      return res.json({
        status: 400,
        details: "no regon, NIP or bank account params found",
      });

    let ceidgObj;
    if (nip && regon) {
      ceidgObj = {
        uri: projectVariable.CEIDG_URI,
        params: {
          regon: regon,
          nip: nip,
        },
      };
    } else if (nip)
      ceidgObj = {
        uri: projectVariable.CEIDG_URI,
        params: {
          nip: nip,
          // regon: "",
        },
      };
    else if (regon)
      ceidgObj = {
        uri: projectVariable.CEIDG_URI,
        params: {
          regon: regon,
        },
      };

    console.log({ ceidgObj });
    const axios = require("axios");
    axios
      .get(ceidgObj.uri, {
        params: ceidgObj.params,
      })
      .then(async (response) => {
        const response_data = response.data;
        console.log({ response_data });
        const { convertToBigQuerySchema } = require("./mapper/bqMapper");
        // const checkedData = await getUserSelectedData(userId);
        const bq_data = convertToBigQuerySchema({
          userId: userId,
          data: response_data,
          checkedData: checkedData,
        });
        console.log({ bq_data });
        return res.send({ status: 200, details: "okk", data: bq_data });
      })
      .catch((err) => {
        console.log({ err });
        return res.send(err);
      });
  } catch (err) {
    console.log({ err });
    return res.send(err);
  }
});

app.post("/api/facebook", (req, res) => {
  const accessToken = req.body.data.accessToken;
  const userId = jwtDecode(req.cookies.id).userId;
  const FBUserId = req.body.data.userID;
  const checkedData = req.body.data?.checkedData;
  console.log({ accessToken }, { FBUserId });
  // wywolanie janka i przekazanie mu tokena do apki...
  const params = {
    accessToken: accessToken,
    userId: FBUserId,
  };
  console.log({ params });
  const axios = require("axios");
  axios
    .get(`${projectVariable.FACEBOOK_URI}`, {
      params: params,
    })
    .then((response) => {
      const response_data = response.data;
      console.log({ response_data });
      const { convertToBigQuerySchema } = require("./mapper/bqMapper");
      // const checkedData = await getUserSelectedData(userId);
      const bq_data = convertToBigQuerySchema({
        userId: userId,
        data: response_data.data,
        checkedData: checkedData,
      });
      console.log({ bq_data });
      return res.send({ status: 200, details: "OK", data: bq_data });
    })
    .catch((err) => {
      console.log({ err });
      return res.status(400).send(err);
    });
  // return res.status(200).send("OK!");
});

app.post("/ocr/:documentType", async (req, res) => {
  try {
    console.log("download document scan: ", req.params.documentType);
    const path = require("path");
    let file = req.files.file;
    const checkedData = req.body.checkedData;
    const documentType = req.params.documentType;
    const userId = jwtDecode(req.cookies.id).userId;
    // const consentId = req.body.consentId
    // console.log({consentId});
    console.log({ documentType });
    console.log({ checkedData });
    let tiffPath;
    if (
      path.extname(file.name) != ".pdf" &&
      path.extname(file.name) != ".tiff"
    ) {
      console.log("[convert to tiff]");
      const { convertToTiff } = require("./OCR/file_converter");
      tiffPath = await convertToTiff(file);
      file = {
        name: `${file.name}.tiff`,
        type: documentType,
        tempFilePath: tiffPath,
      };
    } else {
      file = {
        name: `${file.name}`,
        type: documentType,
        tempFilePath: file.tempFilePath,
      };
    }
    // jwtDecode(req.cookies.id).userId
    const { bucketPath } = await gcStorage.uploadFileToBucket(
      userId,
      "ocr",
      file
    );
    const { unlink } = require("fs/promises");
    tiffPath && (await unlink(tiffPath));

    try {
      const data = await runOCRScript(
        bucketPath,
        documentType,
        userId,
        checkedData
      );

      const { convertToBigQuerySchema } = require("./mapper/bqMapper");
      const bq_data = convertToBigQuerySchema({
        userId: userId,
        data: data,
        checkedData: checkedData.split(","),
      });
      return res.status(200).send(bq_data);
    } catch (err) {
      return res
        .status(400)
        .send({ error: "error occured during OCR process", info: err });
    }
  } catch (err) {
    console.log(err);
    res.status(404).send(err);
  }
});

const runOCRScript = (bucketPath, documentType, userId, dataChecked) => {
  return new Promise(async (resolve, reject) => {
    const { exec } = require("child_process");
    await exec(
      `node ${__dirname}/OCR/index.js filePath=${bucketPath} fileType=${documentType} user_id=${userId} data=${dataChecked}`,
      (error, stdout, stderr) => {
        if (error !== null) {
          console.log("[ocr exec error]", { error });
          // res.status(404).send(error);
          reject(err);
        } else {
          console.log({ stdout });
          const data = JSON.parse(stdout);
          console.log({ data });
          // data.consentId = consentId
          // res.status(200).send(data);
          resolve(data);
        }
      }
    );
  });
};

app.get("/uniqueCode", async (req, res) => {
  const userData = jwtDecode(jwtDecode(req.cookies?.tokens)?.id_token);
  const userId = jwtDecode(req.cookies.id).userId;
  // get token for specific user
  const codeGenerator = require("./generator/code_generator");
  const token = await codeGenerator.getUserCodeToken(userId, userData);
  console.log({ token });
  res.status(200).send(token);
});

app.get("/upToDateData", async (req, res) => {
  try {
    const userData = await gcBigquery.selectUpToDateAllData(
      jwtDecode(req.cookies.id).userId
    );
    console.log({
      userData,
    });
    // const govRDOData = await gcBigquery.getGOVRDOData(jwtDecode(req.cookies.id).userId);
    // const gcpData = await gcBigquery.joinData([ocrData, govRDOData]);
    return res.status(200).send(userData);
  } catch (err) {
    console.log({
      err,
    });
    return res.status(404).send(err);
  }
});

app.get("/settings/phone/add", async (req, res) => {
  // console.log("[phoneId] redirect uri");
  if (req.query.UserTag === undefined) {
    return res.send("error, no UserTag found");
  } else {
    console.log("UserTag: ", req.query.UserTag);
    let lang = getLanguage(req);
    // if (req.headers["accept-language"]) {
    //   lang = getLanguage(req.headers["accept-language"]);
    // } else lang = "pl";
    const base64url = require("base64url");
    const buffer = Buffer.from(base64url.decode(req.query.UserTag), "base64");
    const privateKey = fs.readFileSync(
      "certificatesPHONEID/sk_cid_topID_BV_C_1777"
    );
    let tokens;
    const user_id = jwtDecode(req.cookies.id).userId;
    console.log({ user_id });

    const crypto = require("crypto");
    let pKeyO = {};
    pKeyO.key = privateKey;
    pKeyO.padding = crypto.constants.RSA_PKCS1_OAEP_PADDING;

    try {
      const decrypted = crypto.privateDecrypt(pKeyO.key, buffer);
      console.log("decrypted UserTag: ", decrypted.toString("ascii"));
      const jsonUser = JSON.parse(decrypted);
      console.log("User MSISDN: ", jsonUser.userMSISDN);

      // let emails,
      let phones, factors;
      const phone_number = jsonUser.userMSISDN;
      // wrzucam do bazy numer telefonu...
      try {
        // OPEN CONNECTION TO DATABASE
        try {
          await gcPostgresql.connect();
        } catch (err) {
          console.log(err);
        }

        phones = await gcPostgresql.selectAllPhoneNumbers();

        // weryfikacja czy taki numer juz istnieje
        let number_exist = false;
        for (const phone of phones) {
          if (phone.phone_number === phone_number) {
            // istnieje
            number_exist = true;
            break;
          }
        }

        if (!number_exist) {
          await gcPostgresql.addPhone({
            user_id: user_id,
            phone_number: phone_number,
          });
        }

        const factors = await getAuthenticationFactors({
          user_id: user_id,
          factors: ["all"],
        });

        // wygenerowanie passphrase
        let passphrase;
        if (factors?.is_biometry_enrolled[0].is_biometry_enrolled) {
          passphrase = await generatePassphrase(userId);
        } else {
          passphrase = {
            passphrase: language[lang].passphrase,
            currentTimestamp: "",
            passphraseExpirationTime: "",
          };
        }

        // CLOSE CONNECTION TO DB
        try {
          await gcPostgresql.close();
        } catch (err) {
          console.log(err);
        }

        const username = await getUsername(user_id);
        // pin password i biometria bedzie jako true or false czyli jest albo jej nie ma
        return res.status(200).render("phoneid/phoneid_redirect.ejs", {
          dictionary: language[lang],
          username: username ? username : "",
          emails: factors?.emails,
          phones: factors?.phones,
          pin: factors?.pin,
          password: factors?.password,
          isBiometryEnrolled: {
            status: factors?.is_biometry_enrolled[0].is_biometry_enrolled,
            passphrase: passphrase,
          },
          securityQuestions: factors?.security_questions,
        });
        // res.set("Connection", "close");
        // res.close();
      } catch (err) {
        console.log(err);
      }
    } catch (err) {
      console.log("[phoneid error]", {
        err,
      });
      ws.sendMessage(user_id, {
        event: "phoneid",
        status: "error",
        info: err,
      });
    }
  }
});

app.post("/settings", isAuthenticated, async (req, res) => {
  try {
    const lang = req.query.lang;
    const e_show = req.query.e_show;
    console.log({ e_show });
    const userId = jwtDecode(req.cookies.id).userId;
    try {
      await gcPostgresql.connect();
    } catch (err) {
      console.log(err);
    }

    const factors = await getAuthenticationFactors({
      user_id: userId,
      factors: ["all"],
    });

    // CLOSE CONNECTION TO DB
    try {
      await gcPostgresql.close();
    } catch (err) {
      console.log(err);
    }

    console.log({ factors });

    // wygenerowanie passphrase
    let passphrase;
    if (factors?.is_biometry_enrolled[0].is_biometry_enrolled) {
      passphrase = await generatePassphrase(userId);
    } else {
      passphrase = {
        passphrase: language[lang].passphrase,
        currentTimestamp: "",
        passphraseExpirationTime: "",
      };
    }

    return res.status(200).render("main/partials/settings", {
      dictionary: language[lang],
      emails: factors?.emails,
      phones: factors?.phones,
      pin: factors?.pin,
      password: factors?.password,
      isBiometryEnrolled: {
        status: factors?.is_biometry_enrolled[0].is_biometry_enrolled,
        passphrase: passphrase,
      },
      securityQuestions: factors?.security_questions,
      e_show: e_show ? [e_show] : [],
    });
  } catch (err) {
    res.status(400).send(err);
  }
});

app.post("/settings/verifyPIN", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const pin = req.body.data.pin;
  let dbPINHash;
  console.log({ pin });
  if (!pin)
    return res.status(400).send({
      status: "err",
      error: "pin undefined",
      more: "please type pin",
    });

  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  try {
    const val = await gcPostgresql.userPIN({ user_id: userId });
    dbPINHash = val[0].pin;
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }

  console.log({ dbPINHash });
  console.log("porównuje piny... bcrypt");
  bcrypt.compare(pin, dbPINHash, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(400).send({
        status: "err",
        error: "bcrypt compare error",
        more: "error with bcrypt compare function...",
      });
    }

    if (result) {
      return res.status(200).send({
        status: "Success",
        info: "pin verification done",
        more: "pin is correct",
      });
    } else {
      return res.status(400).send({
        status: "err",
        error: "bcrypt compare error",
        more: "wrong pin",
      });
    }
  });
});

app.post("/settings/verifyPassword", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const password = req.body.data.password;
  let dbPasswordHash;

  if (!password)
    return res.status(400).send({
      status: "err",
      error: "password undefined",
      more: "please type password",
    });

  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  try {
    const val = await gcPostgresql.userPassword({ user_id: userId });
    dbPasswordHash = val[0].password;
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }

  bcrypt.compare(password, dbPasswordHash, async (err, result) => {
    if (err) {
      return res.status(400).send({
        status: "err",
        error: "bcrypt compare error",
        more: "wrong password",
      });
    }

    if (result) {
      return res.status(200).send({
        status: "Success",
        info: "password verification done",
        more: "password is correct",
      });
    } else {
      return res.status(400).send({
        status: "err",
        error: "bcrypt compare error",
        more: "wrong password",
      });
    }
  });
});

app.post("/settings/verifyQuestion", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const question = req.body.data.question;
  const answer = req.body.data.answer;
  let dbAnswer;

  console.log({ question }, { answer });

  if (!answer || !question)
    return res.status(400).send({
      status: "err",
      error: "answer or question undefined",
      more: "please type answer or check question",
    });

  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  try {
    const val = await gcPostgresql.userQuestion({
      user_id: userId,
      question: question,
    });
    console.log({ val });
    dbAnswer = val[0].answer;
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }

  if (answer === dbAnswer) {
    return res.status(200).send({
      status: "Success",
      info: "question verification done",
      more: "question answer is correct",
    });
  } else {
    return res.status(400).send({
      status: "err",
      error: "compare error",
      more: "wrong answer",
    });
  }
});

app.delete("/settings/userPIN", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  console.log("usuwam pin z bazy danych...");

  try {
    await gcPostgresql.connect();

    const count = await authenticationFactorsCount(userId);
    console.log(
      "uzytkownik ma zdefiniowane składniki uwierzytelniające w liczbie: ",
      { count }
    );

    if (count > 1) {
      try {
        await gcPostgresql.deletePIN({ user_id: userId });
        console.log("PIN zostal usuniety");
        res.status(200).send({
          status: "Success",
          info: "pin was deleted",
          more: "...",
        });
      } catch (err) {
        console.log(err);
        res.status(400).send({
          status: "err",
          error: "pin was not deleted",
          more: "...",
        });
      }
    } else {
      res.status(400).send({
        status: "AUTH COUNT",
        info: "NOT OK",
        more: "user must have defined almost one authentication factor",
      });
    }
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }
});

app.delete("/settings/userPassword", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;

  try {
    await gcPostgresql.connect();

    const count = await authenticationFactorsCount(userId);
    console.log(
      "uzytkownik ma zdefiniowane składniki uwierzytelniające w liczbie: ",
      { count }
    );

    if (count > 1) {
      try {
        const val = await gcPostgresql.deletePassword({ user_id: userId });
        res.status(200).send({
          status: "Success",
          info: "password was deleted",
          more: "...",
        });
      } catch (err) {
        console.log(err);
        res.status(400).send({
          status: "err",
          error: "password was not deleted",
          more: "...",
        });
      }
    } else {
      res.status(400).send({
        status: "AUTH COUNT",
        info: "NOT OK",
        more: "user must have defined almost one authentication factor",
      });
    }
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }
});

app.delete("/settings/securityQuestion", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const question = req.body.data.question;
  console.log({ question });
  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.deleteSecurityQuestion({
      user_id: userId,
      question: question,
    });
    res.status(200).send({
      status: "Success",
      info: "security question was deleted",
      more: "...",
    });
  } catch (err) {
    console.log(err);
    res.status(400).send({
      status: "err",
      error: "security question was not deleted",
      more: "..." + err,
    });
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }
});

app.post("/settings/changeQuestion", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const replaced = req.body.data.replaced;
  const question = req.body.data.question;
  const answer = req.body.data.answer;

  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  // SELECT EMAILS
  try {
    await gcPostgresql.updateUserQuestion({
      user_id: userId,
      replaced: replaced,
      question: question,
      answer: answer,
    });
    res.status(200).send({
      status: "Success",
      info: "security question was updated",
      more: "...",
    });
  } catch (err) {
    console.log(err);
    res.status(400).send({
      status: "err",
      error: "security question was not updated",
      more: "..." + err,
    });
  }

  // CLOSE CONNECTION TO DB
  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }
});

app.get("/settings/emails/refresh", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  let emails;
  // OPEN CONNECTION TO DATABASE
  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  // SELECT EMAILS
  try {
    emails = await gcPostgresql.userEmails({ user_id: userId });
  } catch (err) {
    console.log(err);
  }

  // CLOSE CONNECTION TO DB
  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }

  return res.status(200).send(emails);
});

app.post("/settings/changePIN", isAuthenticated, async (req, res) => {
  try {
    // 6138d38780a199f242867d32
    // jwtDecode(req.cookies.id).userId
    const userId = jwtDecode(req.cookies.id).userId;
    const oldPIN = req.body.data.old_pin;
    const newPIN = req.body.data.new_pin;
    let dbPINHash = "";
    // oldPINHash = "";
    console.log({ oldPIN, newPIN });

    if (!newPIN)
      return res.status(400).send({
        status: "err",
        error: "new pin undefined",
        more: "please type new pin",
      });

    try {
      await gcPostgresql.connect();
    } catch (err) {
      console.log(err);
    }

    try {
      const val = await gcPostgresql.userPIN({ user_id: userId });
      dbPINHash = val[0].pin;
    } catch (err) {
      console.log(err);
    }

    console.log({ dbPINHash });

    const savePINToDB = () => {
      bcrypt.hash(newPIN, saltRounds, async (err, hash) => {
        try {
          await gcPostgresql.setPIN({
            user_id: userId,
            pin: hash,
          });
          try {
            await gcPostgresql.close();
          } catch (err) {
            console.log(err);
          }
          return res.status(200).send({
            status: "Success",
            info: "pin changed",
            more: "pin successfully updated",
          });
        } catch (err) {
          console.log(err);
        }
      });
    };

    if (dbPINHash === null || dbPINHash === "") {
      savePINToDB();
    } else {
      console.log(
        "porownuje bcrypt compare old pin i hash z bazy: ",
        { oldPIN },
        { dbPINHash }
      );
      bcrypt.compare(oldPIN, dbPINHash, async (err, result) => {
        if (err) {
          console.log({ err });
          return res.status(200).send({
            status: "err",
            error: "bcrypt compare error",
            more: "old pin and new pin couldn't be compered, err",
          });
        }

        if (result) {
          savePINToDB();
        } else {
          return res.status(200).send({
            status: "err",
            error: "wrong pin",
            more: "old pin is not correct",
          });
        }
      });
    }
  } catch (err) {
    return res.status(400).send(err);
  }
});

app.post("/settings/changePassword", isAuthenticated, async (req, res) => {
  try {
    const userId = jwtDecode(req.cookies.id).userId;
    const oldPassword = req.body.data.old_password;
    const newPassword = req.body.data.new_password;
    let dbPasswordHash = "";
    // oldPINHash = "";
    console.log({ oldPassword, newPassword });

    if (!newPassword)
      return res.status(400).send({
        status: "err",
        error: "new password undefined",
        more: "please type new password",
      });

    try {
      await gcPostgresql.connect();
    } catch (err) {
      console.log(err);
    }

    try {
      const val = await gcPostgresql.userPassword({ user_id: userId });
      dbPasswordHash = val[0].password;
    } catch (err) {
      console.log(err);
    }

    console.log({ dbPasswordHash });

    const savePasswordToDB = () => {
      bcrypt.hash(newPassword, saltRounds, async (err, hash) => {
        try {
          await gcPostgresql.setPassword({
            user_id: userId,
            password: hash,
          });
          try {
            await gcPostgresql.close();
          } catch (err) {
            console.log(err);
          }
          return res.status(200).send({
            status: "Success",
            info: "password changed",
            more: "password successfully updated",
          });
        } catch (err) {
          console.log(err);
        }
      });
    };

    if (dbPasswordHash === null || dbPasswordHash === "") {
      savePasswordToDB();
    } else {
      bcrypt.compare(oldPassword, dbPasswordHash, async (err, result) => {
        if (err) {
          return res.status(200).send({
            status: "err",
            error: "bcrypt compare error",
            more: "old password and new password couldn't be compered, err",
          });
        }

        if (result) {
          savePasswordToDB();
        } else {
          return res.status(200).send({
            status: "err",
            error: "wrong password",
            more: "old password is not correct",
          });
        }
      });
    }
  } catch (err) {
    return res.status(400).send(err);
  }
});

app.post("/settings/securityQuestions", async (req, res) => {
  try {
    const data = req.body.data;
    const user_id = jwtDecode(req.cookies.id).userId;

    console.log({ data });

    try {
      await gcPostgresql.connect();
    } catch (err) {
      console.log(err);
    }

    try {
      await gcPostgresql.addSecurityQuestions({
        user_id: user_id,
        questions: data,
      });
    } catch (err) {
      console.log(err);
    }

    try {
      await gcPostgresql.close();
    } catch (err) {
      console.log(err);
    }

    return res.status(200).send({
      status: "ok",
      info: "updated successfully",
      more: "security questions added to postgresql",
    });
  } catch (err) {
    return res.send(err);
  }
});

app.get("/settings/securityQuestions", async (req, res) => {
  try {
    const user_id = jwtDecode(req.cookies.id).userId;
    let questions;
    try {
      await gcPostgresql.connect();
    } catch (err) {
      console.log(err);
    }
    try {
      questions = await gcPostgresql.userSecurityQuestions({
        user_id: user_id,
      });
    } catch (err) {
      console.log(err);
    }
    try {
      await gcPostgresql.close();
    } catch (err) {
      console.log(err);
    }

    return res.status(200).send(questions);
  } catch (err) {
    return res.send(err);
  }
});

app.post("/api/biometry/enrollments", async (req, res) => {
  try {
    let file = req.files.audioFile;
    const isBiometryEnrolled = req.body.isBiometryEnrolled;
    const verification = req.body.verification;
    const userId = jwtDecode(req.cookies.id).userId;
    console.log({ file }, { isBiometryEnrolled }, { verification });
    let url = `${projectVariable.OPEN_ID_BIOMETRY_ENROLLMENT}?userId=${userId}`;

    console.log(`czy uzytkownik ma biometrie? ${isBiometryEnrolled}`);

    if (isBiometryEnrolled === "false") {
      console.log("tworzę nowy profil biometryczny...");

      // próbuje stworzyc profil....
      try {
        const createdProfile = await createBiometryProfile({ url: url });
        console.log({ createdProfile });
      } catch (err) {
        console.log({ err });
      }

      url = `${projectVariable.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/enrollments`;
      console.log({ url });

      try {
        const data = await sendBiometrySample({
          url: url,
          file: file,
        });
        console.log({ data });

        if (data.enrollmentStatus === "Enrolled") {
          try {
            await gcPostgresql.connect();
          } catch (err) {
            console.log(err);
          }

          try {
            await gcPostgresql.setUserBiometryStatus({
              user_id: userId,
              isBiometryEnrolled: true,
            });
          } catch (err) {
            console.log(err);
          }

          try {
            await gcPostgresql.close();
          } catch (err) {
            console.log(err);
          }

          return res.status(200).send({
            status: "Success",
            info: "biometry profile created and enrolled successfully",
            more: data,
          });
        } else if (data.enrollmentStatus === "Enrolling") {
          return res.status(200).send({
            status: "Warning",
            info: "enrolling in process...",
            more: data,
          });
        } else {
          return res.status(400).send({
            status: "Error",
            info: "biometry was not enrolled successfully",
            more: data,
          });
        }
      } catch (err) {
        return res.status(400).send({
          status: "Error",
          info: "biometry was not enrolled successfully",
          more: err,
        });
      }
    } else {
      if (verification === "true") {
        url = `${projectVariable.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/verifications?userId=${userId}&threshold=0.5`;
      } else {
        url = `${projectVariable.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/enrollments`;
      }

      try {
        const data = await sendBiometrySample({
          url: url,
          file: file,
        });
        console.log({ data });
        if (verification === "true") {
          if (data.recognitionResult.includes("Error")) {
            return res.status(400).send({
              status: "Error",
              info: "biometry was not verify correctly",
              more: err,
            });
          } else {
            return res.status(200).send({
              status: "Success",
              info: "biometry verified successfully",
              more: data,
            });
          }
        } else {
          return res.status(200).send({
            status: "Success",
            info: "biometry enrolled successfully",
            more: data,
          });
        }
      } catch (err) {
        return res.status(400).send({
          status: "Error",
          info: "biometry was not verify/enrolled correctly",
          more: err,
        });
      }
    }
  } catch (err) {
    // return res.status(400).send("NOT OK");
  }
});

const createBiometryProfile = (obj) => {
  return new Promise((resolve, reject) => {
    const url = obj.url;
    const axios = require("axios");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    axios({
      method: "post",
      port: 8083,
      url: url,
    })
      .then(async (response) => {
        console.log({ response });
        // OPEN CONNECTION TO DATABASE
        // console.log(response.status);
        if (response.status === 200) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const sendBiometrySample = (obj) => {
  return new Promise((resolve, reject) => {
    const url = obj.url;
    const ffmpeg = require("fluent-ffmpeg");
    ffmpeg(obj.file.tempFilePath)
      .toFormat("wav")
      .on("error", (err) => {
        console.log("An error occurred: " + err.message);
      })
      .on("progress", (progress) => {
        console.log("Processing: " + progress.targetSize + " KB converted");
      })
      .on("end", () => {
        console.log("Processing finished !");
        const promise = new Promise((resolve) => {
          const FormData = require("form-data"); // npm install --save form-data
          var fd = new FormData();
          fd.append(
            "audioFile",
            fs.createReadStream("./uploads/enrollment_speach_append.wav")
          );
          const concat = require("concat-stream");
          fd.pipe(
            concat({ encoding: "buffer" }, (data) =>
              resolve({ data, headers: fd.getHeaders() })
            )
          );
        });

        promise.then(({ data, headers }) => {
          const axios = require("axios");
          process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
          axios({
            method: "post",
            port: 8083,
            url: url,
            data: data,
            headers,
          })
            .then((response) => {
              // console.log({ response });
              if (response.status === 200) {
                resolve(response?.data);
              } else reject(response);
            })
            .catch((response) => {
              // console.log({ response });
              reject(response);
            });
        });
      })
      .save("./uploads/enrollment_speach_append.wav");
  });
};

app.get("/settings/generatePassphrase", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const passphrase = await generatePassphrase(userId);
  console.log({ passphrase });
  return res.status(200).send(passphrase);
});

const generatePassphrase = (userId) => {
  return new Promise((resolve) => {
    const url = `${projectVariable.OPEN_ID_BIOMETRY_ENROLLMENT}/${userId}/passphrase?userId=${userId}`;
    const axios = require("axios");
    axios({
      method: "post",
      port: 8083,
      url: url,
    })
      .then((response) => {
        if (response?.data) {
          resolve(response.data);
        } else
          resolve({
            passphrase: "mój głos jest moim hasłem",
            currentTimestamp: "",
            passphraseExpirationTime: "",
          });
      })
      .catch((res) => {
        resolve({
          passphrase: "mój głos jest moim hasłem",
          currentTimestamp: "",
          passphraseExpirationTime: "",
        });
      });
  });
};

app.post("/consentId", async (req, res) => {
  try {
    // jwtDecode(req.cookies.id).userId
    // 61152f739f2e4a2881b543ff
    const userUUID = jwtDecode(req.cookies.id).userId;
    const service = req.body.data.service;
    const { getConsentID } = require("./blockchain/blockchain");
    const consentID = await getConsentID({
      userId: userUUID,
      sourceName: service,
    });
    res.status(200).send(consentID);
  } catch (err) {
    res.status(400).send(err);
  }
});

const getAuthenticationFactors = (dt) => {
  return new Promise(async (resolve) => {
    let obj = {};
    const user_id = dt.user_id;
    const factors = dt.factors;

    if (factors.includes("all") || factors.includes("emails")) {
      try {
        obj.emails = await gcPostgresql.userEmails({ user_id: user_id });
      } catch (err) {
        console.log(err);
      }
    }

    // SELECT phones
    if (factors.includes("all") || factors.includes("phones")) {
      try {
        obj.phones = await gcPostgresql.userPhones({ user_id: user_id });
      } catch (err) {
        console.log(err);
      }
    }

    // CHECK PASSWORD
    if (factors.includes("all") || factors.includes("password")) {
      try {
        obj.password = await gcPostgresql.userPassword({ user_id: user_id });
      } catch (err) {
        console.log(err);
      }
    }

    // CHECK PIN
    if (factors.includes("all") || factors.includes("pin")) {
      try {
        obj.pin = await gcPostgresql.userPIN({ user_id: user_id });
      } catch (err) {
        console.log(err);
      }
    }

    // check biometry
    if (factors.includes("all") || factors.includes("is_biometry_enrolled")) {
      try {
        obj.is_biometry_enrolled = await gcPostgresql.isBiometryEnrolled({
          user_id: user_id,
        });
      } catch (err) {
        console.log(err);
      }
    }

    // security questions
    if (factors.includes("all") || factors.includes("security_questions")) {
      try {
        obj.security_questions = await gcPostgresql.userSecurityQuestions({
          user_id: user_id,
        });
      } catch (err) {
        console.log(err);
      }
    }

    resolve(obj);
  });
};

const authenticationFactorsCount = (user_id) => {
  return new Promise(async (resolve) => {
    const factors = await getAuthenticationFactors({
      user_id: user_id,
      factors: ["all"],
    });
    // check if user has min two factors defined
    let count = 0;
    for (const key in factors) {
      console.log(`sprawdzam skladnik uwierzytelniajacy ${key}`);
      // if (key != "security_questions") {
      if (key === "is_biometry_enrolled") {
        if (factors[key][0].is_biometry_enrolled) {
          count++;
        }
      } else if (key === "security_questions") {
        factors[key].length > 0 && count++;
      } else {
        if (factors[key].length > 0) {
          for (const e of factors[key]) {
            if (key === "password" || key === "pin") {
              if (e[key] && e[key] != null) {
                count++;
              }
            } else if (key === "emails") {
              if (e["email_address"] && e["email_address"] != null) {
                count++;
              }
            } else if (key === "phones") {
              if (e["phone_number"] && e["phone_number"] != null) {
                count++;
              }
            }
          }
        }
      }
      console.log(`po weryfikacji zliaczyłem: ${count}`);
      // }
    }
    resolve(count);
  });
};

app.post("/settings/phones/delete", isAuthenticated, async (req, res) => {
  try {
    const lang = req.query.lang;
    const phone_number = req.body.data.phone_number;

    const user_id = jwtDecode(req.cookies.id).userId;

    try {
      await gcPostgresql.connect();

      const count = await authenticationFactorsCount(user_id);
      console.log(
        "uzytkownik ma zdefiniowane składniki uwierzytelniające w liczbie: ",
        { count }
      );

      if (count > 1) {
        await gcPostgresql.deletePhone({
          user_id: user_id,
          phone_number: phone_number,
        });
        await gcPostgresql.close();
        return res.status(200).send({
          status: 200,
          info: "OK",
          more: "phone number deleted successfully",
        });
      } else {
        await gcPostgresql.close();
        return res.status(400).send({
          status: "AUTH COUNT",
          info: "NOT OK",
          more: "user must have defined almost one authentication factor",
        });
      }
    } catch (err) {
      return res.status(400).send({ error: 400, info: err });
    }
  } catch (err) {
    res.status(400).send(err);
  }
});

app.get("/settings/phones/get", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  let phones;
  try {
    await gcPostgresql.connect();
  } catch (err) {
    console.log(err);
  }

  try {
    phones = await gcPostgresql.userPhones({ user_id: userId });
  } catch (err) {
    console.log(err);
  }

  try {
    await gcPostgresql.close();
  } catch (err) {
    console.log(err);
  }

  return res.status(200).send(phones);
});

app.post("/updateManually", async (req, res) => {
  try {
    const data = req.body.data;
    const userId = jwtDecode(req.cookies.id).userId;
    console.log({ data });
    const { convertToBigQuerySchema } = require("./mapper/bqMapper");
    const bq_data = convertToBigQuerySchema({
      userId: userId,
      checkedData: "all",
      data: data,
    });
    console.log({ bq_data });
    await gcBigquery.publishMultipleToTopics(userId, bq_data);
    return res.status(200).send("manual data published");
  } catch (err) {
    return res.send(err);
  }
});

app.post("/publishDataToTopic", async (req, res) => {
  try {
    let data = req.body.data;
    // const { addDataPackage } = require("./blockchain/blockchain");
    // const { getHash } = require("./crypto/crypto");
    console.log("published data to topic route:", { data });
    // 61152f739f2e4a2881b543ff
    // jwtDecode(req.cookies.id).userId
    const userId = jwtDecode(req.cookies.id).userId;
    await gcBigquery.publishMultipleToTopics(userId, data);
    return res.status(200).send("published");
  } catch (err) {
    return res.send({
      err,
    });
  }
});

app.post("/identities/email", isAuthenticated, async (req, res) => {
  const lang = req.query.lang;
  try {
    const response = await refreshTokenRequest(req);
    console.log("i have email response from refresh token....", response.data);
    const userData = jwtDecode(response.data.id_token.id_token);
    console.log("[current user data]", {
      userData,
    });
    const tokens = saveTokens(response);
    res.cookie("tokens", tokens);
    return res.status(200).render(`main/partials${req.path}`, {
      emails: userData.emails,
      dictionary: language[lang],
    });
  } catch (err) {
    res.status(400).send(err);
  }
});

app.post("/addNewEmail", isAuthenticated, (req, res) => {
  try {
    // const { projectVariable } = require("./configuration");
    const email = req.body.email;
    // console.log({ email });
    const user_id = jwtDecode(req.cookies.id).userId;
    // console.log({ user_id });
    const client_id = projectVariable.OPEN_ID_CLIENT_ID;
    console.log({ email }, { user_id }, { client_id });
    const https = require("https");
    const post_data = {
      email: email,
      client_id: client_id,
      user_id: user_id,
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    };

    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer xxxxxx",
    };
    console.log({ post_data });
    const axios = require("axios");
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    axios
      .post(`${projectVariable.OPEN_ID_EMAIL_ENDPOINT}`, post_data, {
        headers: headers,
      })
      .then((response) => {
        console.log({ response });
        console.log("[open id email response]", response.data);
        res.status(200).send(response.data);
      })
      .catch((err) => {
        console.log("[open id email error]", err);
        res.status(400).send(err);
      });
    // return res.status(200).send("ok");
  } catch (err) {
    return res.status(400).send(err);
  }
});

app.post("/settings/emails/delete", isAuthenticated, async (req, res) => {
  // const { projectVariable } = require("./configuration");
  // const lang = req.query.lang;
  const email = req.body.data.email;
  const user_id = jwtDecode(req.cookies.id).userId;

  // OPEN CONNECTION TO DATABASE
  try {
    await gcPostgresql.connect();
    const count = await authenticationFactorsCount(user_id);
    console.log(
      "użytkownik ma zdefiniowane skłądniki uwierzytelniające w liczbie: ",
      { count }
    );
    if (count >= 2) {
      await gcPostgresql.deleteEmail({ user_id: user_id, email: email });
      await gcPostgresql.close();
      return res
        .status(200)
        .send({ status: 200, info: "email deleted successfully" });
    } else {
      await gcPostgresql.close();
      return res.status(400).send({
        status: "AUTH COUNT",
        info: "NOT OK",
        more: "user must have defined almost one authentication factor",
      });
    }
  } catch (err) {
    console.log(err);
    return res.status(400).send({ error: 400, info: err });
  }
});

app.post("/settings/emails/resend", async (req, res) => {
  const email = req.body.data.email;
  const clientId = projectVariable.OPEN_ID_CLIENT_ID;
  const url = projectVariable.OPEN_ID_RESEND_EMAIL_ENDPOINT;

  console.log({ email }, { clientId }, { url });

  const https = require("https");
  const post_data = {
    email: email,
    client_id: clientId,
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
  };

  const headers = {
    "Content-Type": "application/json",
    Authorization: "Bearer xxxxxx",
  };
  console.log({ post_data });
  const axios = require("axios");
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  axios
    .post(url, post_data, {
      headers: headers,
    })
    .then((response) => {
      console.log({ response });
      console.log("[open id email resend response]", response.data);
      return res.status(200).send(response.data);
    })
    .catch((err) => {
      console.log("[open id email resend error]", err);
      return res.status(400).send(err);
    });
});

app.post("/identities/phones", isAuthenticated, async (req, res) => {
  const lang = req.query.lang;
  try {
    const response = await refreshTokenRequest(req);
    const userData = jwtDecode(response.data.id_token.id_token);
    console.log("[current user data]", {
      userData,
    });
    let phones;
    phones = userData.phone_numbers;
    const tokens = saveTokens(response);
    res.cookie("tokens", tokens);
    return res.status(200).render(`main/partials${req.path}`, {
      phones: phones,
      dictionary: language[lang],
    });
  } catch (err) {
    res.status(400).send(err);
  }
});

app.post(
  [
    "/identities/identities",
    "/profiles/profiles",
    "/profiles/investment",
    "/profiles/business",
    "/profiles/lending",
    // "/profiles/driver",
    // "/profiles/driver",
    // "/profiles/persona",
    "/profiles/budget",
  ],
  isAuthenticated,
  (req, res) => {
    const lang = req.query.lang;
    res.status(200).render(`main/partials${req.path}`, {
      dictionary: language[lang],
    });
  }
);

app.post(
  [
    "/identities/profile",
    "/identities/profile_extended",
    "/identities/shopping",
    "/identities/identity",
  ],
  isAuthenticated,
  async (req, res) => {
    const lang = req.query.lang;
    try {
      const splittedPath = req.path.split("/");
      const scope = splittedPath[splittedPath.length - 1].toUpperCase();
      console.log("downloading data from table: ", { scope });
      const userData = await gcBigquery.selectUserScopeBigQueryData(
        jwtDecode(req.cookies.id).userId,
        scope
      );
      console.log({ userData });

      res.render(`main/partials${req.path}`, {
        data: userData,
        dictionary: language[lang],
      });
    } catch (err) {
      console.log({
        err,
      });
      res.status(401).send(err);
    }
  }
);

app.post("/permissions", isAuthenticated, async (req, res) => {
  try {
    const lang = req.query.lang;
    const e_show = req.query.e_show;
    const userId = jwtDecode(req.cookies.id).userId;
    const blockchain = require("./blockchain/blockchain");
    const permissions = await blockchain.getPermissions({
      userId: userId,
      onlyActive: false,
    });
    console.log({ permissions });
    res.status(200).render(`main/partials${req.path}`, {
      dictionary: language[lang],
      permissions: permissions,
      e_show: e_show ? [e_show] : [],
    });
  } catch (err) {
    res.status(404).send(err);
  }
});

app.post("/permissions/update", async (req, res) => {
  const userId = jwtDecode(req.cookies.id).userId;
  const access_token = jwtDecode(req.cookies.tokens).access_token;
  const refresh_token = jwtDecode(req.cookies.tokens).refresh_token;
  const permissionId = req.body.data.permission_id;
  const viewerId = req.body.data.viewerId;
  const scope = req.body.data.scope;
  const status = req.body.data.status;
  const type = req.body.data.type;

  console.log({ permissionId }, { viewerId }, { scope }, { status });

  try {
    const blockchain = require("./blockchain/blockchain");
    const data = await blockchain.updatePermission({
      userId: userId,
      permissionId: permissionId,
      viewerId: viewerId,
      scope: scope,
      status: status,
      type: type,
      access_token: access_token,
      refresh_token: refresh_token,
    });
    console.log({ data });
    return res.status(200).send({
      status: "Success",
      info: "permission update successfully",
      more: "...",
    });
  } catch (err) {
    console.log({ err });
    return res.status(400).send({
      status: "Error",
      err: "permission was not updated successfully",
      more: "..." + err,
    });
  }
});

app.post("/data", isAuthenticated, async (req, res) => {
  try {
    const lang = req.query.lang;
    const userId = jwtDecode(req.cookies.id).userId;
    let files;
    try {
      files = await gcStorage.getUserFiles(userId);
      console.log({ files });
    } catch (err) {
      console.log(err);
    }
    res.status(200).render(`main/partials${req.path}`, {
      data: files,
      dictionary: language[lang],
    });
  } catch (err) {
    res.status(404).send(err);
  }
});

app.post("/history", isAuthenticated, async (req, res) => {
  try {
    const lang = req.query.lang;
    const data_filter = req.query.filter;
    console.log({ data_filter });
    // jwtDecode(req.cookies.id).userId
    const userId = jwtDecode(req.cookies.id).userId;
    const historyData = await gcBigquery.getAllData(userId);
    const blockchain = require("./blockchain/blockchain");
    const consents = await blockchain.getConsents({ userId: userId });
    const authHistory = await blockchain.getAuthorizationHistory({
      userId: userId,
    });
    // console.log({ historyData }, { authHistory });
    res.status(200).render(`main/partials${req.path}`, {
      data: historyData,
      consents: consents,
      auth_history: authHistory,
      dictionary: language[lang],
      data_filter: data_filter ? data_filter : "",
    });
  } catch (err) {
    res.status(404).send(err);
  }
});

app.post("/api/gcs/download", async (req, res) => {
  const path = req.body.data.path;
  let url;
  console.log({ path });
  try {
    url = await gcStorage.downloadFile(path);
    console.log({ url });
  } catch (err) {
    console.log(err);
    return res.status(400).send(err);
  }

  setDeleteFile(120000, url);

  return res.status(200).send({ url: url });
});

const setDeleteFile = (time = 30000, path) => {
  const file_path = path;
  setTimeout(() => {
    try {
      const fs = require("fs");
      fs.unlinkSync(file_path);
      console.log(
        `file ${
          file_path.split("/")[file_path.split("/").length - 1]
        } was successfully removed`
      );
    } catch (err) {
      console.log(err);
      console.log(
        `file ${
          file_path.split("/")[file_path.split("/").length - 1]
        } was not removed`
      );
    }
  }, time);
};

app.post("/deviceAuthorization", async (req, res) => {
  const deviceCode = req.body.code;
  console.log("device authorization... code from network drive: ", {
    deviceCode,
  });
  const codeGenerator = require("./generator/code_generator");
  const userToken = await codeGenerator.verifyCode(deviceCode);
  if (userToken) {
    return res.status(200).send(userToken);
  } else {
    return res.status(404).send("code invalid");
  }
});

app.post("/scraper/:websiteService", async (req, res) => {
  try {
    // send token in header with user ID
    if (!req.headers.authorization) {
      return res.status(403).json({ error: "No credentials sent!" });
    }
    const token = req.headers.authorization;
    const service = req.params.websiteService;
    let file = req.files.file;
    // const token = req.body.token
    const userId = jwtDecode(token.split(" ")[1]).data.user_id;
    console.log({ service }, { file }, { token }, { userId });

    // consent to blockchain... zapisuej zgode na blockchainie i zwraca jej id albo po prostu zwraca jej id jak zgoda juz istnieje tylko modyfikuje timestamp
    const { getConsentID } = require("./blockchain/blockchain");
    const consentID = await getConsentID({
      userId: userId,
      sourceName: service,
    });
    console.log({ consentID });

    const path = require("path");
    if (
      path.extname(file.name) != ".html" &&
      path.extname(file.name) != ".mhtml"
    ) {
      return res.status(401).send({
        error: "Error - wrong extension",
        info: `The extension of file ${path.extname(
          file.name
        )} is incorrect. Possible values: html or mhtml`,
      });
    }

    file = {
      name: `${file.name}`,
      type: service,
      tempFilePath: file.tempFilePath,
    };

    const { bucketPath, bucketName } = await gcStorage.uploadFileToBucket(
      userId,
      "scraper",
      file
    );
    console.log({ bucketPath });
    try {
      const checkedData = await getUserSelectedData(userId);
      await runScraper(file, bucketName, bucketPath)
        .then(async (data) => {
          console.log({ data });
          const { convertToBigQuerySchema } = require("./mapper/bqMapper");
          const bq_data = convertToBigQuerySchema({
            userId: userId,
            checkedData: checkedData,
            data: data,
          });
          // await gcBigquery.publishMultipleToTopics(userId, bq_data);
          res.status(200).send("ok");
          // inform frontend that process completed
          ws.sendMessage(userId, {
            event: "network_drive_update",
            status: "done",
            info: "webscraping data update done",
            source: service,
            data: bq_data,
            json: data.jsonStoragePath,
          });
        })
        .catch((err) => {
          return res.status(401).send({
            error: "err",
            info: `error occured while processing scraper script`,
          });
        });
    } catch (err) {
      console.log(err);
    }
  } catch (err) {
    res.status(400).send(err);
  }
});

const runScraper = (file, bucketName, bucketPath) => {
  return new Promise(async (resolve, reject) => {
    const { exec } = require("child_process");
    console.log(
      `////////// wywołanie skrapera z parametrami bucketName=${bucketName} bucketPath=${bucketPath} service=${file.type}`
    );
    await exec(
      `node ${__dirname}/scraper/app.js bucketName=${bucketName} bucketPath=${bucketPath} service=${file.type}`,
      async (error, stdout, stderr) => {
        if (error !== null) {
          console.log("[scraper exec error]", { error });
          reject(error);
        } else {
          console.log({ stdout });
          try {
            const data = JSON.parse(stdout);
            resolve(data);
            // await gcBigquery.publishToTopic(userId, topicData);
          } catch (err) {
            reject(err);
          }
        }
      }
    );
  });
};

app.post("/custom_protocol_check", (req, res) => {
  try {
    console.log("custom protocol check...");
    const customProtocolCheck = require("custom-protocol-check");
    const protocol = req.body.data.protocol;
    console.log({ protocol });
    if (!protocol) {
      return res
        .status(400)
        .json({ status: 400, details: "no protocol body data" });
    }

    customProtocolCheck(
      `${protocol}`,
      () => {
        console.log("Custom protocol not found.");
        res
          .status(404)
          .json({ status: 404, details: "custom protocol not found" });
      },
      () => {
        console.log("Custom protocol found and opened the file successfully.");
        res
          .status(200)
          .json({ status: 200, details: "success! protocol found" });
      },
      5000
    );
  } catch (err) {}
});

app.get("/logout", (req, res) => {
  res.clearCookie("tokens");
  res.redirect("/");
});

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}

var privateKey = fs.readFileSync("ssl-cert/privkey.pem", "utf8");
var certificate = fs.readFileSync("ssl-cert/fullchain.pem", "utf8");

var credentials = { key: privateKey, cert: certificate };

//pass in your credentials to create an https server
var httpsServer = https.createServer(credentials, app);
httpsServer.listen(8443, () => {
  console.log("https server is running on port 8443...");
});

ws = WebSocket.ws(httpsServer);

const getUserSelectedData = (userId) => {
  return new Promise((resolve) => {
    ws.onMessage(userId, (client) => {
      client.onmessage = async (message) => {
        try {
          const message_json = JSON.parse(message.data.toString());
          if (message_json.event && message_json.event === "response_data") {
            if (message_json.checked) {
              //   if (message_json.data.length > 0) {
              console.log(message_json);

              const dataChecked = message_json.data;
              resolve(dataChecked);
            }
          }
        } catch (err) {
          console.log(
            "Error while parsing websocket message with user selected data"
          );
          resolve("all");
        }
      };
    });
    try {
      ws.sendMessage(userId, { event: "get_data", checked: true });
    } catch (err) {
      resolve("all");
    }
  });
};
