exports.ws = (server) => {
  const { WebSocketServer } = require("ws");
  const LocalStorage = require("node-localstorage").LocalStorage,
    localStorage = new LocalStorage("./websocket/ws_users");

  const wss = new WebSocketServer({
    noServer: true,
    path: "/websocket/",
  });

  wss.getUniqueID = () => {
    s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return s4() + s4() + "-" + s4();
  };

  wss.on("connection", (ws) => {
    console.log("a new client connected...");
    ws.id = wss.getUniqueID();
    console.log("Client.ID: " + ws.id);

    ws.on("message", (message) => {
      const message_json = JSON.parse(message.toString());
      if (message_json.event && message_json.event === "connect") {
        if (message_json.data && message_json.data.userId) {
          console.log({ message_json });

          // check if user already has some websocket and terminate this
          const length = localStorage.length;
          for (let i = 0; i < length; i++) {
            const userId = localStorage.key(i);
            const wsId = localStorage.getItem(userId);
            if (userId === message_json.data.userId) {
              if (wsId != ws.id) {
                for (const client of wss.clients) {
                  if (client.id === wsId) {
                    console.log("client already exists... closing " + wsId);
                    client.close();
                  }
                }
              }
            }
          }

          const userId = message_json.data.userId;
          localStorage.setItem(userId, ws.id);
          ws.send(
            JSON.stringify({
              event: "accept",
              data: { info: "user connected successfully", wsId: ws.id },
            })
          );
        } else {
          ws.send(
            JSON.stringify({
              event: "error",
              data: {
                info:
                  'websocket error - no data send or userId not found in message of type "register"',
              },
            })
          );
        }
      } else if (message_json.event && message_json.event === "ping") {
        console.log("ping from client...");
        ws.send(JSON.stringify({ event: "pong" }));
      }
    });

    ws.on("error", (error) => {
      console.log({ error });
    });

    ws.on("close", (ev) => {
      const length = localStorage.length;
      for (let i = 0; i < length; i++) {
        const userId = localStorage.key(i);
        const wsId = localStorage.getItem(userId);
        if (ws.id === wsId) {
          localStorage.removeItem(userId);
        }
      }
      console.log("disconnected and deleted: ", ws.id);
      console.log(`event: ${ev}`);
    });
  });

  wss.on("close", function close(ev) {
    console.log("wss close..." + ev);
  });

  const sendMessage = (_userId, message) => {
    try {
      const userId = _userId;
      const wsId = localStorage.getItem(userId);
      console.log("[WS] sending a message to: ", {
        wsId,
      });
      if (wsId) {
        for (const client of wss.clients) {
          if (client.id === wsId) {
            client.send(JSON.stringify(message));
          }
        }
      } else throw "no wsId found - user is not connected";
    } catch (err) {
      throw err;
    }
  };

  const onMessage = (_userId, callback) => {
    try {
      const userId = _userId;
      const wsId = localStorage.getItem(userId);
      console.log("[WS] on message for client: ", { wsId });
      for (const client of wss.clients) {
        if (client.id === wsId) {
          callback(client);
        }
      }
    } catch (err) {
      throw err;
    }
  };

  server.on("upgrade", (request, socket, head) => {
    wss.handleUpgrade(request, socket, head, (websocket) => {
      wss.emit("connection", websocket, request);
    });
  });

  return {
    sendMessage: sendMessage,
    onMessage: onMessage,
  };
};
