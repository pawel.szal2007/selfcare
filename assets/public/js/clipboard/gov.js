(() => {
  const govRDOButton =
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='gov_rdo']";
  const govPESELButton =
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='gov_pesel']";
  const govNL =
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='gov_nl']";
  const rejectButton = $(
    ".download__modal .download__modal-content .download__modal-content-header .modal-button-reject"
  );
  const linkedInButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='linkedin']"
  );

  const downloadModalHandler = (button) => {
    showCustomModal("download");
    $(rejectButton).unbind("click");
    $(rejectButton).on("click", () => {
      closeCustomModal("download");
      getDataFromNetworkDriver($(button).data("name"));
    });
  };

  const getDataFromNetworkDriver = async (service) => {
    const lang = $("nav.navbar .language__active").data("active-language");
    console.log(window.getCurr);
    let socket = socketConnect();
    mainBackgroundView({ event: "show" });
    const text = loadingFullScreen(
      {
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].network_drive_update,
      },
      () => {
        socket.close(1000);
      }
    );

    const token = await GET("/uniqueCode");
    const data = parseJwt(token);
    const url = `networkdrivepw:code=${data.code}/service=${service}`;

    var sUsrAg = navigator.userAgent;

    if (sUsrAg.indexOf("Firefox") > -1) {
      openNewWindow({
        event: "show",
        name: "cyber-networkdrive",
        url: url,
        height: 330,
      });
    } else window.location.href = url;

    socket.onclose = (event) => {
      if (event.wasClean) {
        console.log(
          `[close] Websocket connection closed cleanly, code=${event.code} reason=${event.reason}`
        );
      } else if (event.code === 1006) {
        alert(`websocket connection closed with event code 1006`);
        console.log(`websocket connection closed with event code 1006`);
        socket = socketConnect();
        getWSMessage(socket);
      }
    };

    const getWSMessage = (socket) => {
      socket.onmessage = async (event) => {
        const message = JSON.parse(event.data);
        if (message.event === "get_data") {
          if (message.checked) {
            // console.log({ message });
            const checkedData = getCheckedDataLabels_Array();
            // console.log({ checkedData });
            socket.send(
              JSON.stringify({
                event: "response_data",
                checked: true,
                data: checkedData,
              })
            );
          }
        }
        if (message.event && message.event === "accept") {
          console.log({ message });
        }
        if (message.event === "network_drive_update") {
          console.log("network drive update: ", { message });
          if (message.status === "done") {
            const data = message.data;
            const jsonPath = message.jsonStoragePath;
            $(text).html(language[lang].updating);
            loadingFullScreen({ event: "close" });
            const [acceptButton, rejectButton] = userDataView({
              event: "show",
              background: "transparent",
              source: message.source,
              loa: data?.loa,
              data: data,
              jsonStoragePath: message?.json,
            });

            window.focus();

            acceptButton && $(acceptButton).unbind("click");
            acceptButton &&
              $(acceptButton).on("click", async () => {
                // dataView({ event: "close" });
                // console.log("{user accepted data}");
                userDataView({ event: "close" });
                const text = loadingFullScreen({
                  event: "show",
                  background: "transparent",
                  text:
                    "jeszcze tylko chwila, trwa aktualizacja Twoich danych...",
                });
                loadingFullScreen({ event: "hide", object: "close_button" });
                const response = await POST("/publishDataToTopic", data);
                // console.log({ response });
                setTimeout(async () => {
                  await update();
                  mainBackgroundView({ event: "close" });
                  loadingFullScreen({ event: "close" });
                }, 5000);
              });

            rejectButton && $(rejectButton).unbind("click");
            rejectButton &&
              $(rejectButton).on("click", () => {
                // dataView({ event: "close" });
                // console.log("{user rejected data}");
                mainBackgroundView({ event: "close" });
                userDataView({ event: "close" });
              });
          } else {
            $(text).html(language[lang].updating_error);
          }
          socket.close(1000);
        }
      };
    };
    getWSMessage(socket);
  };

  $(govRDOButton).unbind("click");
  $(govRDOButton).on("click", (e) => {
    downloadModalHandler(e.currentTarget);
  });

  $(govPESELButton).unbind("click");
  $(govPESELButton).on("click", (e) => {
    downloadModalHandler(e.currentTarget);
  });

  $(govNL).unbind("click");
  $(govNL).on("click", (e) => {
    downloadModalHandler(e.currentTarget);
  });

  $(linkedInButton).unbind("click");
  $(linkedInButton).on("click", (e) => {
    downloadModalHandler(e.currentTarget);
  });
})();
