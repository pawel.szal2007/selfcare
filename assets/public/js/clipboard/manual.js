(() => {
  const manualMenuButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='manual']"
  );
  const reject = $(
    ".clipboard__manager .clipboard__manager-button.clipboard__manager-manual-reject"
  );
  const save = $(
    ".clipboard__manager .clipboard__manager-button.clipboard__manager-manual-save"
  );
  const selectAll = $(".clipboard__page .select__all");

  $(manualMenuButton).unbind("click");
  $(manualMenuButton).on("click", () => {
    updateManually();
  });

  var updateManually = async () => {
    // const checkboxes = $('.clipboard__list-item .form-check-input')
    for (const checkbox of checkboxes) {
      const value = $(checkbox).next().children()[0];
      const itemRow = $(checkbox).parent().parent();
      if ($(checkbox).is(":checked")) {
        const valueText = $(value).html();
        const secondaryText = $(itemRow).find(
          ".clipboard__list-item__secondary-text"
        );
        const dataLabel = $(secondaryText).data("label");
        console.log({ dataLabel });

        if (dataLabel === "zoneinfo") {
          $(value).is("span") &&
            // onblur='spanReset(this)'
            $(value).replaceWith(
              `<input class="clipboard__temp-input-popover clipboard__list-item__primary-text" value='${valueText}' data-bs-container=".clipboard__card-list" data-bs-toggle="popover" data-bs-placement="right" data-bs-content="sprawdź czy sugerowana strefa zamieszkania jest poprawna"/>`
            );

          const res = await GET("/zoneinfo");
          console.log({ res }, $(secondaryText).prev());
          $(secondaryText).prev().val(res);

          // show popover
          var popover = new bootstrap.Popover(
            document.querySelector(".clipboard__temp-input-popover"),
            {
              container: ".clipboard__list-item",
              trigger: "manual",
            }
          );
          popover.show();
          setTimeout(() => {
            popover.hide();
          }, 4000);
        } else {
          $(value).is("span") &&
            // onblur='spanReset(this)'
            $(value).replaceWith(
              `<input class="clipboard__temp-input clipboard__list-item__primary-text" value='${valueText}' />`
            );
        }

        $(checkbox).remove();
      } else $(itemRow).remove();
    }
    $(selectAll).remove();

    // check if category and loa needs to be displayed none beacuse there is no items
    const containers = $(".clipboard__page .clipboard__item-container");
    // console.log({ containers });
    for (const container of containers) {
      // console.log({ container });
      const items = $(container).find(".clipboard__list-item");
      // console.log(items.length);
      if (items.length === 0) {
        $(container).remove();
      }
    }

    // change clipboard manager menu
    const menuContent = $(".clipboard__manager .clipboard__manager-content");
    const menuContentAlt = $(
      ".clipboard__manager .clipboard__manager-content-alternative"
    );

    $(menuContent).css("opacity", "0");
    $(menuContentAlt).css("z-index", "0");
    $(menuContentAlt).css("opacity", "1");
  };
})();

var spanReset = (e) => {
  let txt = e.value;
  const label = $(e).next().data("label");
  $(e).is("input") &&
    $(e).replaceWith(
      `<span class="clipboard__list-item__primary-text">${txt}</span>`
    );
};

var manualModReject = async () => {
  await postCookiesData();
};

var manualModSave = async (e) => {
  // pokazac jakies łądowanie
  $(e).replaceWith(`<div class="small__loader"></div>`);
  const fields = $(".clipboard__list-item__primary-text");
  const dataObj = {};
  for (const field of fields) {
    const label = $(field).next().data("label");
    const value = $(field).val();
    dataObj[label] = value;
  }
  dataObj.loa = "low";
  dataObj.source = "manually";
  // console.log({ dataObj });
  const response = await POST("/updateManually", dataObj);
  // console.log({ response });
  setTimeout(async () => {
    await update();
  }, 3000);
};
