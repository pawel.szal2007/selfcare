(() => {
  const facebookButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='facebook']"
  );
  // const linkedInButton = $(
  //   ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='linkedin']"
  // );
  const closeButton = $(".loading__transparent .loading__transparent-close");

  // // console.log({ facebookButton });
  const getLoginResponse = () => {
    return new Promise((resolve) => {
      FB.getLoginStatus((response) => {
        resolve(response);
      });
    });
  };
  //
  const logInToFacebook = () => {
    return new Promise((resolve, reject) => {
      FB.login(
        (response) => {
          console.log({ response });
          if (response.authResponse) {
            resolve(response);
          } else {
            reject({
              error: "err",
              message: "User cancelled login or did not fully authorize.",
            });
          }
        },
        {
          scope:
            "public_profile,email,user_link,user_gender,user_location,user_hometown,user_birthday",
        }
      );
    });
  };
  //
  const getFacebookData = async (authResponse) => {
    const lang = $("nav.navbar .language__active").data("active-language");
    const text = loadingFullScreen({
      event: "show",
      background: "var(--main-background)",
      icon: '<i class="fab fa-facebook-square fa-2x"></i>',
      text: "pobieranie danych z facebook...",
    });
    $(closeButton).unbind("click");
    $(closeButton).on("click", () => {
      loadingFullScreen({ event: "close" });
    });
    console.log("getting facebook data... ", { authResponse });
    try {
      const checkedData = getCheckedDataLabels_Array();
      authResponse.checkedData = checkedData;
      const response = await POST("/api/facebook", authResponse);
      const data = JSON.parse(response).data;
      console.log({ data });
      loadingFullScreen({ event: "close" });
      showData(data);
    } catch (err) {
      console.log(err);
      $(text).html(language[lang].get_data_error);
      // mainBackgroundView({ event: "close" });
      loadingFullScreen({ event: "visible", object: "close_button" });
      return;
    }
  };

  $(facebookButton).on("click", async () => {
    try {
      let response = await getLoginResponse();
      console.log({ response });
      if (response.status === "connected") {
        console.log("user logged in");
        getFacebookData(response.authResponse);
      } else {
        console.log("user not logged, please log in");
        response = await logInToFacebook();
        console.log({ response });
        console.log("user logged in");
        getFacebookData(response.authResponse);
      }
    } catch (err) {
      console.log({ err });
    }
  });

  const showData = (data) => {
    mainBackgroundView({ event: "show" });
    const [acceptButton, rejectButton] = userDataView({
      event: "show",
      background: "transparent",
      // source: source,
      // loa: loa,
      data: data,
    });

    $(acceptButton).unbind("click");
    $(acceptButton).on("click", async () => {
      // dataView({ event: "close" });
      // console.log("{user accepted data}");
      userDataView({ event: "close" });
      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        text: "jeszcze tylko chwila, trwa aktualizacja Twoich danych...",
      });
      loadingFullScreen({ event: "hide", object: "close_button" });
      const response = await POST("/publishDataToTopic", data);
      // console.log({ response });
      setTimeout(async () => {
        await update();
        mainBackgroundView({ event: "close" });
        loadingFullScreen({ event: "close" });
      }, 5000);
    });

    $(rejectButton).unbind("click");
    $(rejectButton).on("click", () => {
      // dataView({ event: "close" });
      // console.log("{user rejected data}");
      mainBackgroundView({ event: "close" });
      userDataView({ event: "close" });
    });
  };
})();
