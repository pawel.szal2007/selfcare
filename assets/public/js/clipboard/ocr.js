// const ocrModalButtons = $(".upload__modal .upload__modal-button");

var consentId;
var uploadFile = async () => {
  try {
    const active_lang = $("nav.navbar .language__active").data(
      "active-language"
    );
    const fileInput = document.getElementById("upload__document-input");
    const select = $(".upload__modal .upload__modal-content-title");
    const documentsDropdown = $(
      ".upload__modal .upload__modal-content #upload__documents-dropdown"
    );
    const documentType = $(documentsDropdown).val();
    const file = fileInput.files[0];
    if (!file) {
      $(select).html("dodaj plik!");
      return;
    }
    showCustomModal("consent");
    if (
      $.data(
        $(
          ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
        ).get(0),
        "events"
      ) &&
      $.data(
        $(
          ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
        ).get(0),
        "events"
      ).click
    ) {
      // console.log("on click exists...");
      return;
    } else {
      $(
        ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
      ).unbind("click");
      $(
        ".consent__modal .consent__modal-content .consent__modal-content-button.agree"
      ).on("click", async (e) => {
        try {
          closeCustomModal("consent");

          mainBackgroundView({ event: "show" });
          const text = loadingFullScreen({
            event: "show",
            text: "proszę czekać, zapisujemy Twoją zgodę...",
            background: "transparent",
          });

          loadingFullScreen({ event: "hide", object: "close_button" });

          try {
            consentId = await getConsent(documentType);
            // console.log({
            //   consentId,
            // });
          } catch (err) {
            $(text).html("niepowodzenie, zgoda nie została zapisana :(");
            loadingFullScreen({ event: "visible", object: "close_button" });
            mainBackgroundView({ event: "close" });
            // socket && socket.close(1000);
            throw err;
          }

          clearUploadModal();

          $(text).html(
            "proszę czekać, odczytujemy właśnie Twój dokument, może to zająć kilka minut"
          );
          const checkedData = getCheckedDataLabels_Array();
          const fd = new FormData();
          fd.append("file", file);
          fd.append("checkedData", checkedData);

          $.ajax({
            url: `/ocr/${documentType}`,
            type: "POST",
            data: fd,
            contentType: false,
            processData: false,
            error: (err) => {
              $(text).html(
                "niestety coś poszło nie tak, nie udało nam się prawidłowo wydobyć Twoich danych, spróbuj ponownie później"
              );

              loadingFullScreen({ event: "visible", object: "close_button" });
              mainBackgroundView({ event: "close" });
            },
            success: (data) => {
              console.log({
                data,
              });
              loadingFullScreen({ event: "close" });
              let source, loa;
              for (const key in data) {
                source = data[key].data.source;
                loa = data[key].data.loa;
                break;
              }
              const [acceptButton, rejectButton] = userDataView({
                event: "show",
                background: "transparent",
                source: source,
                loa: loa || "low",
                data: data || {},
              });

              $(acceptButton).unbind("click");
              $(acceptButton).on("click", async () => {
                userDataView({ event: "close" });
                const text = loadingFullScreen({
                  event: "show",
                  background: "transparent",
                  text:
                    "jeszcze tylko chwila, trwa aktualizacja Twoich danych...",
                });
                loadingFullScreen({ event: "hide", object: "close_button" });
                const response = await POST("/publishDataToTopic", data);
                // console.log({ response });
                setTimeout(async () => {
                  await update();
                  mainBackgroundView({ event: "close" });
                  loadingFullScreen({ event: "close" });
                }, 5000);
              });

              $(rejectButton).unbind("click");
              $(rejectButton).on("click", () => {
                // dataView({ event: "close" });
                // console.log("{user rejected data}");
                mainBackgroundView({ event: "close" });
                userDataView({ event: "close" });
              });
            },
          });
        } catch (err) {
          mainBackgroundView({ event: "close" });
          loadingFullScreen({ event: "close" });
          clearUploadModal();
        }
      });
    }
  } catch (err) {
    clearUploadModal();
  }
};

$(".upload__modal .upload__modal-content .upload__button").unbind("click");
$(".upload__modal .upload__modal-content .upload__button").on("click", () => {
  uploadFile();
});

var clearUploadModal = () => {
  const select = $(".upload__modal .upload__modal-content-title");
  const uploadModal = $(".upload__modal");
  const dataView = $(
    ".upload__modal .upload__modal-content .upload__modal-data"
  );
  // const ocrButtons = $('.upload__modal .upload__modal-button');
  $(select).html("wybierz plik:");
  $(uploadModal).css("display", "none");
  $(dataView).html("");
};

$(".upload__modal .upload__modal-close").unbind("click");
$(".upload__modal .upload__modal-close").on("click", (e) => {
  clearUploadModal();
});
