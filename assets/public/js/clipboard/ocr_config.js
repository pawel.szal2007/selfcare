var ocrConfiguration = {
  identity_card: {
    service: "identity_card",
    topicName: "topic-user-id-ocr",
  },
  passport: {
    service: "passport",
    topicName: "topic-user-passport-ocr",
  },
  gov_id_pdf: {
    service: "gov_id_pdf",
    topicName: "topic-user-gov-id-pdf-ocr",
  },
};
