(() => {
  const ceidgButton = $(
    ".clipboard__manager .clipboard__manager-edit .dropdown-menu .dropdown-item[data-name='ceidg']"
  );
  const radiosButtons = $(".ceidg__modal .ceidg__modal-content .content-radio");
  const ceidgInput = $(".ceidg__modal .ceidg__modal-content .content-input");
  const acceptButton = $(".ceidg__modal .ceidg__modal-content .content-button");
  const closeButton = $(".ceidg__modal .ceidg__modal-content .content-close");

  $(ceidgButton).unbind("click");
  $(ceidgButton).on("click", async () => {
    try {
      showCustomModal("ceidg");
    } catch (err) {
      console.log({ err });
    }
  });

  $(acceptButton).unbind("click");
  $(acceptButton).on("click", async () => {
    const lang = $("nav.navbar .language__active").data("active-language");
    let radioOption;
    for (const radio of radiosButtons) {
      if ($(radio).is(":checked")) {
        radioOption = $(radio).val();
      }
    }
    const inputValue = $(ceidgInput).val();
    if (inputValue && radioOption) {
      closeCustomModal("ceidg");

      mainBackgroundView({ event: "show" });

      const text = loadingFullScreen({
        event: "show",
        background: "transparent",
        icon: '<span class="material-icons-outlined">storage</span>',
        text: language[lang].get_data_ceidg,
      });
      loadingFullScreen({ event: "hide", object: "close_button" });
      let data;
      const checkedData = getCheckedDataLabels_Array();
      try {
        let response;
        if (radioOption === "nip") {
          response = await POST(`/ceidg/data`, {
            checkedData: checkedData,
            nip: inputValue,
          });
        } else if (radioOption === "regon") {
          response = await POST(`/ceidg/data`, {
            checkedData: checkedData,
            regon: inputValue,
          });
        } else {
          console.log("radio option ma wartosc: ", { radioOption });
        }
        data = JSON.parse(response).data;
      } catch (err) {
        $(text).html(language[lang].get_data_error);
        loadingFullScreen({ event: "visible", object: "close_button" });
        return;
      }
      loadingFullScreen({ event: "close" });
      const [acceptButton, rejectButton] = userDataView({
        event: "show",
        background: "transparent",
        data: data,
      });

      $(acceptButton).unbind("click");
      $(acceptButton).on("click", async () => {
        userDataView({ event: "close" });
        const text = loadingFullScreen({
          event: "show",
          background: "transparent",
          text: language[lang].updating,
        });
        loadingFullScreen({ event: "hide", object: "close_button" });
        const response = await POST("/publishDataToTopic", data);

        setTimeout(async () => {
          try {
            await update();
          } catch (err) {
            $(text).html(language[lang].updating_error);
          }
          mainBackgroundView({ event: "close" });
          loadingFullScreen({ event: "close" });
        }, 5000);
      });

      $(rejectButton).unbind("click");
      $(rejectButton).on("click", () => {
        mainBackgroundView({ event: "close" });
        userDataView({ event: "close" });
      });
    } else {
      // console.log("error: no input value or no radio option passed...");
    }
  });

  $(closeButton).unbind("click");
  $(closeButton).on("click", () => {
    closeCustomModal("ceidg");
  });
})();
