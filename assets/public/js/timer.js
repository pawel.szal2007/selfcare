var script_tag = document.getElementById("timer_script");
var query = script_tag.src.replace(/^[^\?]+\??/, "");
// Parse the querystring into arguments and parameters
var vars = query.split("&");
var args = {};
for (var i = 0; i < vars.length; i++) {
  var pair = vars[i].split("=");
  // decodeURI doesn't expand "+" to a space.
  args[pair[0]] = decodeURI(pair[1]).replace(/\+/g, " ");
}
var stop_text = args["stop_text"];
var start_text = $("#instruction_biometry").text();
var processing_text = "Trwa aktualizacja próbek głosowych, proszę czekać...";
console.log({ stop_text });
console.log({ start_text });

var timer = document.querySelector(".record-card .timer");
var watch = new Stopwatch(timer);

function start() {
  watch.start();
}

function stop() {
  watch.stop();
}

function stopWhenOn() {
  watch.stop();
  watch.reset();
}

function Stopwatch(elem) {
  var time = 0;
  var offset;
  var interval;

  function lapOn() {
    var lapTime = lap_box.appendChild(document.createElement("li"));
    lapTime.innerHTML = elem.textContent;
    lapTime.classList = "lapItem";
  }

  function lapOff() {
    return;
  }

  function update() {
    if (this.isOn) {
      time += delta();
    }
    elem.textContent = timeFormatter(time);
  }

  function delta() {
    var now = Date.now();
    var timePassed = now - offset;

    offset = now;

    return timePassed;
  }

  function timeFormatter(time) {
    time = new Date(time);

    var minutes = time.getMinutes().toString();
    var seconds = time.getSeconds().toString();
    //var milliseconds = time.getMilliseconds().toString();

    if (minutes.length < 2) {
      minutes = "0" + minutes;
    }

    if (seconds.length < 2) {
      seconds = "0" + seconds;
    }

    /*while (milliseconds.length < 3) {
            milliseconds = '0' + milliseconds;
        }*/

    var result = minutes + " : " + seconds; // + ' . ' + milliseconds;

    return result;
  }

  this.start = function () {
    interval = setInterval(update.bind(this), 1);
    offset = Date.now();
    this.isOn = true;
  };

  this.stop = function () {
    clearInterval(interval);
    interval = null;
    this.isOn = false;
  };

  this.reset = function () {
    time = 0;
    lap_box.innerHTML = "";
    interval = null;
    this.isOn = false;
    update();
  };

  this.lapOn = function () {
    lapOn();
  };

  this.lapOff = function () {
    lapOff();
  };

  this.isOn = false;
}

$(document).ready(function () {
  $("#record").removeClass("pulse");
});

$("#record").on("click", function (event) {
  console.log({ event });
  var constraints = { audio: true };
  navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
    $(event.currentTarget).toggleClass("pulse");
    const instructionContent = $("#instruction_biometry").data("content");
    if (instructionContent === "start") {
      timer = document.querySelector(".record-card .timer");
      watch = new Stopwatch(timer);
      $("#instruction_biometry").text(stop_text);
      $("#instruction_biometry").css("color", "var(--bs-danger)");
      $("#instruction_biometry").data("content", "stop");
    } else if (instructionContent === "stop") {
      if (verification) {
        $("#instruction_biometry").text(
          "trwa weryfikacja próbek głosowych, proszę czekać..."
        );
      } else $("#instruction_biometry").text(processing_text);

      $("#instruction_biometry").css("color", "var(--sidebar-active)");
      $("#instruction_biometry").data("content", "processing");
    }
    recordingProcessing(stream);
  });
});
