var script_tag = document.getElementById("record_script");
var query = script_tag.src.replace(/^[^\?]+\??/, "");
// Parse the querystring into arguments and parameters
var vars = query.split("&");
var args = {};
for (var i = 0; i < vars.length; i++) {
  var pair = vars[i].split("=");
  // decodeURI doesn't expand "+" to a space.
  args[pair[0]] = decodeURI(pair[1]).replace(/\+/g, " ");
}
var redirectUri = args["redirectUri"];
var isBiometryEnrolled = args["isBiometryEnrolled"];
var verification = false;
console.log("redirectUri in record.js: ", redirectUri);
console.log(`is biometry enrolled? ${isBiometryEnrolled}`);

var errorMsgElement = document.querySelector("span#errorMsg");
var recordedVideo = document.querySelector("video#recorded");
var recordButton = document.querySelector("button#record");
var playButton = document.querySelector("button#play");

var mediaRecorder;
var recordedBlobs;
var superBuffer;

var recording = false;
//var timer = document.querySelector('.timer');
//var watch = new Stopwatch(timer);
var sleep = (milliseconds) => {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

var changeToBasicState = () => {
  // zmiana wyświetlania
  $("#record").prop("disabled", false);
  $("#instruction_biometry").text(
    "Wciśnij ikonę mikrofonu by rozpocząć nagrywanie i zaktualizować biometrię głosową"
  );
  $("#instruction_biometry").css("color", "black");
  $("#instruction_biometry").data("content", "start");
  $(".timer").text("00 : 00");
};

var recordingProcessing = (stream) => {
  console.log("udalo sie kliknac");
  if (!recording) {
    console.log("rozpoczynam nagrywanie");
    recording = true;
    watch.isOn ? stop() : start();
    startRecording(stream);
  } else {
    console.log("koncze nagrywanie");
    recording = false;
    watch.isOn ? stop() : start();
    mediaRecorder.stop();
    $(".login-card").css("opacity", "0.4");
    $("#record").prop("disabled", true);
    document.body.classList.add("spinner");
    sleep(500).then(() => {
      let formData = new FormData();
      console.log("superBuffer: ", superBuffer);
      console.log("ready to send file to server....");
      formData.append("audioFile", superBuffer, "audioFile.mp3");
      formData.append("isBiometryEnrolled", isBiometryEnrolled);
      formData.append("verification", verification);
      console.log({ redirectUri });
      console.log({ verification });
      $.ajax({
        url: redirectUri,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        error: (err) => {
          console.log(err);
          if (verification) {
            showToast(
              "_danger-no-data",
              '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
              "ustawienia",
              `użytkownik nie został prawidłowo zweryfikowany`
            );
            // zamknac modal?
            $("#staticCredentialsBackdrop").modal("hide");
          } else {
            showToast(
              "_danger-no-data",
              '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
              "ustawienia",
              `biometria nie została zaktualizowana`
            );
          }

          changeToBasicState();
        },
        success: async (data) => {
          console.log({ data });

          if (data.status === "Error") {
            if (verification) {
              showToast(
                "_danger-no-data",
                '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
                "ustawienia",
                `użytkownik nie został prawidłowo zweryfikowany`
              );
              // zamknac modal?
              $("#staticCredentialsBackdrop").modal("hide");
            } else {
              showToast(
                "_danger-no-data",
                '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
                "ustawienia",
                "biometria głosowa nie została zaktualizowana, spróbuj ponownie później"
              );
            }
          } else if (data.status === "Warning") {
            showToast(
              "_warning-repeated",
              '<svg class="bi flex-shrink-0 me-2 blue" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>',
              "ustawienia",
              "żeby utworzyć profil biometryczny musisz nagrać minimum 20 sekundową próbkę, dodaj kolejne nagranie"
            );
          } else {
            if (verification) removeCredential();
            else {
              if (isBiometryEnrolled === "false") {
                showToast(
                  "_add-success",
                  '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
                  "ustawienia",
                  `profil biometryczny został utworzony`
                );
                // zmienić wyglad
                const passphrase = await GET("/settings/generatePassphrase");
                console.log(JSON.parse(passphrase));
                $("#instruction_biometry")
                  .parent()
                  .next()
                  .next()
                  .text(JSON.parse(passphrase).passphrase);
              } else {
                showToast(
                  "_add-success",
                  '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
                  "ustawienia",
                  `biometria została zaktualizowana`
                );
              }
            }
          }

          // zmiana wyświetlania
          changeToBasicState();
        },
      });
    });
  }
};

function handleDataAvailable(event) {
  console.log("handleDataAvailable", event);
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function startRecording(stream) {
  recordedBlobs = [];
  let options = { mimeType: "audio/webm" };
  try {
    var constraints = { audio: true };
    var chunks = [];

    // navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
    mediaRecorder = new MediaRecorder(stream);
    console.log(
      "Created MediaRecorder",
      mediaRecorder,
      "with options",
      options
    );
    mediaRecorder.onstop = (event) => {
      console.log("Recorder stopped: ", event);
      console.log("Recorded Blobs: ", recordedBlobs);
      superBuffer = new Blob(recordedBlobs, { type: "audio/webm" });
    };
    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.start();
    console.log("MediaRecorder started", mediaRecorder);
    // });

    // mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e) {
    console.error("Exception while creating MediaRecorder:", e);
    errorMsgElement.innerHTML = `Exception while creating MediaRecorder: ${JSON.stringify(
      e
    )}`;
    return;
  }
}

function stopRecording() {
  mediaRecorder.stop();
}

function handleSuccess(stream) {
  recordButton.disabled = false;
  console.log("getUserMedia() got stream:", stream);
  window.stream = stream;
}
