var language = {
  eng: {
    name: "english",
    short: "eng",
    search: "search",
    greeting: "hi, ",
    clipboard: "your clipboard is empty",
    data_view_text: "data derived from:",
    network_drive_update: "open network drive app to download data...",
    updating: "data update in progress...",
    updating_error: "the error occured while saving the data...",
    get_data_krs: "reading data from KRS service...",
    get_data_vat: "reading data from VAT service...",
    get_data_ceidg: "reading data from CEIDG service...",
    get_data_error:
      "error occured while downloading data, please try again later",
    accept: "accept",
    email: {
      name: "email",
      empty: `sorry, you haven't got any email addresses :(`,
    },
    phone: {
      name: "phone",
      empty: `sorry, you haven't got any phone numbers :(`,
    },
    ocr: {
      title: "OCR data:",
      identity_card: {
        national_id: "personal number",
        document_number: "identity card number",
        nationality: "nationality",
        place_of_birth: "place of birth",
        issued_date: "date of issue",
        valid_date: "expiry date",
        issuer_name: "issuing authority",
        name: "surname",
        given_name: "given name",
        date_of_birth: "date of birth",
        gender: "gender",
        family_name: "family name",
      },
      passport: {
        name: "surname",
        given_name: "given name",
        nationality: "nationality",
        date_of_birth: "date of birth",
        national_id: "PESEL number",
        gender: "gender",
        place_of_birth: "place of birth",
        issued_date: "date of issue",
        issuer_name: "issuing authority",
        valid_date: "expiry date",
      },
    },
    scope: {
      profile: "identity / profile",
      profile_extended: "identity / extended",
      shopping: "identity / shopping",
      identity: "identity / identity",
    },
    tooltips: {
      delete_all: "delete all items",
      delete_item: "delete item",
      manage: "manage data",
    },
    tooltips_loa: {
      low: "data will be saved with low level of assurance",
      medium: "data will be saved with medium level of assurance",
      high: "data will be saved with high level of assurance",
      unknown: "data level of assurance not defined",
    },
    source: {
      gov_rdo: "gov.pl identity card register",
      gov_pesel: "gov.pl PESEL register",
      id_document_scan: "identity card scan",
      passport_document_scan: "passport scan",
      ceidg: "CEIDG",
      krs: "KRS",
      vat: "VAT register",
      facebook: "facebook",
      linkedin: "linkedIn",
      empty: "no data found",
      gov_id_pdf: "identity card PDF from gov.pl",
    },
    toast: {
      empty: "is empty.",
      already__added: "is already added to your clipboard",
      added_succes: "was added to clipboard!",
    },
    modal: {
      yes: "yes",
      no: "no",
      warning: "warning",
      delete_all: "do you want to remove all data from clipboard?",
    },
    custom_modal: {
      upload: {
        title: "choose file:",
        upload: "upload",
        reject: "reject",
        accept: "accept",
        back: "back",
        waiting: "please wait, it may takes a few minutes...",
        document_type: "document type",
        select: [
          "identity card",
          "passport",
          "pdf from identity card register (gov.pl)",
        ],
      },
      download: {
        title: "do you want to download network drive application?",
        download: "download",
        reject: "no, I already have",
        instruction: "instruction",
        instruction_subtitle: "recommended way of downloading data",
        instruction_point: {
          1: "download and install network drive application (if you already have one, run this app from computer righ bottom corner)",
          2: "choose from menu a website from which you want to download data (gov identity card register or gov PESEL register)",
          3: "you will be redirected on gov platform, please login",
          4: "the application automatically downloads your data and let you know when the process is finished",
          5: 'go back here and confirm above steps by clicking the button "done" below',
        },
        instruction__alternative:
          "(you need to log in into network drive app? generate login code below)",
        generate_code: "generate code",
        confirm: "done",
        login_code_text:
          '<span class="modal__underneath-hide material-icons-outlined">arrow_upward</span>your login code',
      },
      krs: {
        labels: ["name", "last name", "type NIP number", "type KRS number"],
      },
      vat: {
        labels: ["NIP", "REGON", "bank account"],
      },
    },
    loa: {
      name: "level of data confidance:",
      low: "low",
      medium: "medium",
      high: "high",
      none: "unknown",
    },
    settings: "settings",
    logout: "logout",
    sidebar: [
      "home",
      "identities",
      "profile",
      "extended",
      "shopping",
      "identity",
      // "e-mail",
      // "phones",
      "profile",
      "investment",
      "business",
      "lending",
      // "driver",
      // "persona",
      "budget",
      "permissions",
      "data",
      "history",
    ],
    page: {
      home: { name: "home" },
      identities: {
        name: "idenitties",
        page: {
          profile: {
            name: "profile",
            dataLabels: {
              name: "surname",
              given_name: "given name",
              preferred_username: "preffered username",
              locale: "locale",
              zoneinfo: "zone info",
            },
          },
          extended: {
            name: "extended",
            dataLabels: {
              nickname: "nickname",
              picture: "image",
              date_of_birth: "date of birth",
              gender: "gender",
              profile_url: "profile url",
              website: "website",
              education: "education",
            },
          },
          shopping: {
            name: "shopping",
            dataLabels: {
              weight: "weight",
              height: "height",
              clothing_size: "clothing size",
              shoe_size: "shoe size",
              delivery_method: "delivery method",
              address: "address",
              pickup_address: "pickup address",
              payment_type: "payment type",
            },
          },
          identity: {
            name: "identity",
            dataLabels: {
              family_name: "family name",
              middle_name: "middle name",
              nationality: "nationality",
              document_name: "document name",
              document_number: "identity card number",
              issued_country: "issued country",
              issuer_name: "issuing authority",
              issued_date: "date of issue",
              valid_date: "expiry date",
              national_id: "personal number",
              street: "street",
              city: "place",
              zip: "zip code",
              country: "country",
            },
          },
          email: {
            name: "email",
            empty: `sorry, you haven't got any email addresses :(`,
          },
          phones: { name: "phones" },
        },
      },
      profile: {
        name: "profile",
        page: {
          investment: {
            name: "investment",
            dataLabels: {
              savings: "savings",
              deposit: "deposit",
              rate_of_return: "expected rate of return",
              stop_loss: "acceptable portfolio loss ",
            },
          },
          business: { name: "business" },
          lending: {
            name: "lending",
            dataLabels: {
              incomes: "income",
              monthly_income: "monthly income (PLN)",
              fixed_charges: "fixed charges (PLN)",
              other_expenses: "other expanses (PLN)",
              // "incomes", "monthly_income", "fixed_charges", "other_expenses"
            },
          },
          driver: { name: "driver" },
          persona: { name: "persona" },
          budget: { name: "budget" },
        },
      },
      permissions: { name: "permissions" },
      data: { name: "data" },
      history: { name: "history" },
    },
    aboutUs: "about us",
    contact: "contact",
    rules: "rules",
    all: {
      name: "surname",
      given_name: "given name",
      preferred_username: "preffered username",
      middle_name: "middle name",
      locale: "locale",
      zoneinfo: "zone info",
      nickname: "nickname",
      picture: "image",
      date_of_birth: "date of birth",
      gender: "gender",
      profile_url: "profile url",
      website: "website",
      education: "education",
      weight: "weight",
      height: "height",
      clothing_size: "clothing size",
      shoe_size: "shoe size",
      delivery_method: "delivery method",
      address: "address",
      pickup_address: "pickup address",
      payment_type: "payment type",
      family_name: "family name",
      nationality: "nationality",
      document_name: "document name",
      document_number: "identity card number",
      issued_country: "issued country",
      issuer_name: "issuing authority",
      issued_date: "date of issue",
      valid_date: "expiry date",
      national_id: "personal number",
      street: "street",
      city: "place",
      zip: "zip code",
      country: "country",
      savings: "savings",
      deposit: "deposit",
      rate_of_return: "expected rate of return",
      stop_loss: "acceptable portfolio loss",
      incomes: "income (PLN)",
      monthly_income: "monthly income (PLN)",
      fixed_charges: "fixed charges",
      other_expenses: "other expanses (PLN)",
      identity_card: "identity card",
      passport: "passport",
    },
  },
  pl: {
    name: "polski",
    short: "pl",
    search: "szukaj",
    greeting: "witaj, ",
    clipboard: "twój koszyk jest pusty",
    data_view_text: "dane pobrane ze źródła:",
    network_drive_update:
      "otwórz aplikację dysku sieciowego, aby pobrać dane z wybranego serwisu...",
    updating: "dane zostały zapisane, trwa aktualizacja...",
    updating_error: "wystąpił błąd przy zapisywaniu danych...",
    get_data_krs: "trwa pobieranie danych z krajowego rejestru sądowego...",
    get_data_vat: "trwa pobieranie danych z rejestru podatników VAT...",
    get_data_ceidg:
      "trwa pobieranie danych z centralnej ewidencji i informacji o działalności gospodarczej...",
    get_data_error:
      "wystąpił błąd pobierania danych z serwisu, spróbuj ponownie później",
    accept: "zatwierdź",
    email: {
      name: "email",
      empty: `nie masz dodanych żadnych adresów mailowych :(`,
    },
    phone: {
      name: "phone",
      empty: `nie masz dodanych żadnych numerów telefonów :(`,
    },
    ocr: {
      title: "dane OCR dokumentu:",
      id_document_scan: {
        national_id: "numer PESEL",
        document_number: "numer dowodu tożsamości",
        nationality: "narodowość",
        place_of_birth: "miejsce urodzenia",
        issued_date: "data wydania",
        valid_date: "data ważności",
        issuer_name: "organ wydający",
        name: "nazwisko",
        given_name: "imię",
        date_of_birth: "data urodzenia",
        gender: "płeć",
        family_name: "nazwisko rodowe",
      },
      passport: {
        name: "nazwisko",
        given_name: "imię",
        nationality: "narodowość",
        date_of_birth: "data urodzenia",
        national_id: "numer PESEL",
        gender: "płeć",
        place_of_birth: "miejsce urodzenia",
        issued_date: "data wydania",
        issuer_name: "organ wydający",
        valid_date: "data ważnośći",
      },
    },
    scope: {
      profile: "tożsamości / profil",
      profile_extended: "tożsamości / rozszerzony",
      shopping: "tożsamości / zakupy",
      identity: "tożsamości / identyfikacja",
    },
    source: {
      gov_rdo: "rejestr dowodów osobistych gov.pl",
      gov_pesel: "rejestr PESEL gov.pl",
      id_document_scan: "skan dowodu osobistego",
      passport_document_scan: "skan paszportu",
      ceidg: "Centralna Ewidencja i Informacja o Działalnośći Gospodarczej",
      krs: "Krajowy Rejestr Sądowy",
      vat: "rejestr podatników VAT",
      facebook: "facebook",
      linkedin: "linkedIn",
      empty: "brak danych źródłowych",
      gov_id_pdf: "rejestr dowodów osobistych (gov.pl) - dokument PDF",
    },
    toast: {
      empty: "brak danej.",
      already__added: "jest już w schowku.",
      added_succes: "dodano do schowka!",
    },
    tooltips: {
      delete_all: "usuń wszystko",
      delete_item: "usuń ze schowka",
      manage: "zarządzaj danymi",
    },
    tooltips_loa: {
      low: "dane zostaną zapisane z niskim poziomem zaufania",
      medium: "dane zostaną zapisane ze średnim poziomem zaufania",
      high: "dane zostaną zapisane z wysokim poziomem zaufania",
      unknown: "poziom zaufania danych nie został określony",
    },
    modal: {
      yes: "tak",
      no: "nie",
      warning: "ostrzeżenie",
      delete_all: "czy chcesz usunąć wszystkie dane ze schowka?",
    },
    custom_modal: {
      upload: {
        title: "wybierz plik:",
        upload: "prześlij",
        reject: "odrzuć",
        accept: "akceptuj",
        back: "powrót",
        waiting: "proszę czekać, może to zająć kilka minut...",
        document_type: "typ dokumentu",
        select: [
          "dowód osobisty",
          "paszport",
          "pdf pobrany z rejestru dowodów osobistych",
        ],
      },
      download: {
        title: "upewnij się, że masz zainstalowaną aplikację dysku sieciowego",
        download: "pobierz aplikację",
        reject: "mam już zainstalowaną",
        instruction: "instrukcja",
        instruction_subtitle: "rekomendowany sposób pobierania danych",
        instruction_point: {
          1: "pobierz i zainstaluj aplikację dysku sieciowego (jeśli posiadasz już aplikację, uruchom ją z prawego dolnego menu komputera)",
          2: "wybierz z menu po lewej stronie serwis gov, z którego chcesz pobrać dane (rejestr dowodu osobistego lub rejestr PESEL)",
          3: 'zostaniesz przekierowany na stronę platformy "mój gov", zaloguj się przy użyciu loginu i hasła lub profilu zaufanego',
          4: "aplikacja automatycznie pobierze Twoje dane z wybranego rejestru i poinformuje Cię o zakończeniu procesu aktualizacji",
          5: 'wróć tutaj i potwierdź wykonane kroki klikając poniżej przycisk "zrobione"',
        },
        instruction__alternative:
          "(musisz zalogować się do dysku sieciowego? wygeneruj poniżej kod logowania)",
        generate_code: "wygeneruj kod logowania",
        confirm: "zrobione",
        login_code_text:
          '<span class="modal__underneath-hide material-icons-outlined">arrow_upward</span>twój kod logowania',
      },
      krs: {
        labels: ["imię", "nazwisko", "podaj numer NIP", "podaj numer KRS"],
      },
      vat: {
        labels: ["NIP", "REGON", "konto bankowe"],
      },
    },
    loa: {
      name: "poziom zaufania danych:",
      low: "niski",
      medium: "średni",
      high: "wysoki",
      none: "nieokreślony",
    },
    settings: "ustawienia",
    logout: "wyloguj",
    sidebar: [
      "strona główna",
      "tożsamości",
      "profil",
      "rozszerzony",
      "zakupy",
      "identyfikacja",
      // "e-mail",
      // "telefon",
      "profil",
      "inwestycyjny",
      "biznesowy",
      "kredytowy",
      // "kierowca",
      // "persona",
      "budżet",
      "zgody",
      "dane",
      "historia",
    ],
    page: {
      home: { name: "strona główna" },
      identities: {
        name: "tożsamości",
        page: {
          profile: {
            name: "profil",
            dataLabels: {
              name: "nazwisko",
              given_name: "imię",
              preferred_username: "preferowana nazwa użytkownika",
              locale: "miejscowość",
              zoneinfo: "strefa zamieszkania",
            },
          },
          extended: {
            name: "rozszerzony",
            dataLabels: {
              nickname: "pseudonim",
              picture: "zdjęcie",
              date_of_birth: "data urodzenia",
              gender: "płeć",
              profile_url: "adres URL profilu",
              website: "strona internetowa",
              education: "wykształcenie",
            },
          },
          shopping: {
            name: "zakupy",
            dataLabels: {
              weight: "waga",
              height: "wzrost",
              clothing_size: "rozmiar odzieży",
              shoe_size: "rozmiar obuwia",
              delivery_method: "preferowana dostawa",
              address: "adres",
              pickup_address: "adres dostawy",
              payment_type: "preferowana forma płatności",
            },
          },
          identity: {
            name: "identyfikacja",
            dataLabels: {
              family_name: "nazwisko rodowe",
              middle_name: "imiona",
              nationality: "narodowość",
              document_name: "rodzaj dokumentu",
              document_number: "numer dowodu tożsamości",
              issued_country: "kraj wydania",
              issuer_name: "organ wydający",
              issued_date: "data wydania",
              valid_date: "termin ważnośći",
              national_id: "numer PESEL",
              street: "ulica",
              city: "miejsce",
              zip: "kod pocztowy",
              country: "kraj",
            },
          },
          email: {
            name: "email",
            empty: `nie masz dodanych żadnych adresów mailowych :(`,
          },
          phones: { name: "numery telefonów" },
        },
      },
      profile: {
        name: "profil",
        page: {
          investment: {
            name: "inwestycyjny",
            dataLabels: {
              savings: "posidany poziom aktywów oszczędnośći",
              deposit: "w co lokowane są środki",
              rate_of_return: "oczekiwana stopu zwrotu z inwestycji",
              stop_loss: "wysokość dopuszczalnej straty portfela",
            },
          },
          business: { name: "biznesowy" },
          lending: {
            name: "kredytowy",
            dataLabels: {
              incomes: "przychody",
              monthly_income: "miesięczne przychody (PLN)",
              fixed_charges: "stałe obciążenia (PLN)",
              other_expenses: "pozostałe miesięczne wydatki (PLN)",
              // "incomes", "monthly_income", "fixed_charges", "other_expenses"
            },
          },
          driver: { name: "kierowca" },
          persona: { name: "persona" },
          budget: { name: "budżet" },
        },
      },
      permissions: { name: "zgody" },
      data: { name: "dane" },
      history: { name: "historia" },
    },
    aboutUs: "o nas",
    contact: "kontakt",
    rules: "zasady",
    all: {
      name: "nazwisko",
      given_name: "imię",
      middle_name: "drugie imię",
      preferred_username: "preferowana nazwa użytkownika",
      locale: "miejscowość",
      zoneinfo: "strefa zamieszkania",
      nickname: "pseudonim",
      picture: "zdjęcie",
      date_of_birth: "data urodzenia",
      gender: "płeć",
      profile_url: "adres URL profilu",
      website: "strona internetowa",
      education: "wykształcenie",
      weight: "waga",
      height: "wzrost",
      clothing_size: "rozmiar odzieży",
      shoe_size: "rozmiar obuwia",
      delivery_method: "preferowana dostawa",
      address: "adres",
      pickup_address: "adres dostawy",
      payment_type: "preferowana forma płatności",
      family_name: "nazwisko rodowe",
      nationality: "narodowość",
      document_name: "rodzaj dokumentu",
      document_number: "numer dowodu tożsamości",
      issued_country: "kraj wydania",
      issuer_name: "organ wydający",
      issued_date: "data wydania",
      valid_date: "termin ważnośći",
      national_id: "numer PESEL",
      street: "ulica",
      city: "miejsce",
      zip: "kod pocztowy",
      country: "kraj",
      savings: "posidany poziom aktywów oszczędnośći",
      deposit: "w co lokowane są środki",
      rate_of_return: "oczekiwana stopu zwrotu z inwestycji",
      stop_loss: "wysokość dopuszczalnej straty portfela",
      incomes: "przychody",
      monthly_income: "miesięczne przychody (PLN)",
      fixed_charges: "stałe obciążenia (PLN)",
      other_expenses: "pozostałe miesięczne wydatki (PLN)",
      identity_card: "dowód osobisty",
      passport: "paszport",
    },
  },
};
