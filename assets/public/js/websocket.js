const socketConnect = () => {
  const socket = new WebSocket("wss://selfcare.topid.org/websocket/");

  socket.onopen = (event) => {
    try {
      const token = readCookie("id")[1];
      const userId = parseJwt(token).userId;
      socket.send(
        JSON.stringify({ event: "connect", data: { userId: userId } })
      );
    } catch (err) {
      // console.log(err);
    }
  };

  socket.onerror = (error) => {
    console.log({ error });
    alert(`[error] ${error}`);
  };

  return socket;
};

const getCheckedDataLabels_Array = () => {
  const arr = [];
  const checkboxes = $(".clipboard__list-item .form-check-input");
  for (const checkbox of checkboxes) {
    if ($(checkbox).is(":checked")) {
      arr.push(
        $(checkbox)
          .parent()
          .find(".clipboard__list-item__secondary-text")
          .data("label")
      );
      // object[$(checkbox).parent().find('.clipboard__list-item__secondary-text').data('label')] = $(checkbox).parent().find('.clipboard__list-item__secondary-text').data('label');
    }
  }
  return arr;
};
