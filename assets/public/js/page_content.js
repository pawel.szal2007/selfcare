$(document).ready(() => {
  // check user data collapsible elements on page
  const userDataRows = $('.page .page-content [data-bs-toggle="collapse"]');
  let currentElement;
  for (const dataRow of userDataRows) {
    $(dataRow).on("click", (e) => {
      // const collapsibleElements = $(".page .page-content .user__data-collapse");
      const collapsibleElements = $(".page .page-content .collapse");
      for (const element of collapsibleElements) {
        $(element).parent().parent().removeClass("main-background");
        $(element).collapse("hide");
      }
      currentElement != e.currentTarget &&
        $(e.currentTarget).parent().addClass("main-background");
      // $(e.currentTarget).find(".user__data-collapse").collapse("show");
      $(e.currentTarget).find(".collapse").collapse("show");
      if (currentElement != e.currentTarget) {
        currentElement = e.currentTarget;
      } else {
        currentElement = "";
        $(e.currentTarget).parent().removeClass("main-background");
      }
    });
  }
});
