"use strict";
var login = $(".login-page");
!login && (window.location.hash = "home");

// language choose option
const dataLanguages = $("[data-language]");
for (const data of dataLanguages) {
  $(data).on("click", (e) => {
    // console.log(e.currentTarget);
    // console.log("zmiana jezyka");
    const lang = $(e.currentTarget).data("language");
    // // console.log("new language: ", { lang });
    $("nav.navbar .language__active").html($(e.currentTarget).html());
    $("nav.navbar .language__active").data("active-language", lang);
    setWebsiteLanguage(lang);
  });
}

$(document).ready(async () => {
  $('[data-bs-toggle="tooltip"]').tooltip();
  // set active sidebar elements
  const listItems = $("nav.sidebar li");
  if (!$(".clipboard__page")) {
  }
  // set language
  const language = $("nav.navbar .language__active").data("active-language");
  let cookieHash = readCookie("clipboard");
  let cookieDataCounter = 0;
  // console.log(cookieHash);
  if (cookieHash && cookieHash.length > 0) {
    const cookiesData = JSON.parse(
      decodeURIComponent(b64DecodeUnicode(decodeURIComponent(cookieHash[1])))
    );
    // let cookie = readCookie("clipboard");

    // let cookiesData = cookie.split("%");
    cookieDataCounter = cookiesData.length;
  }
  showBadgeCounter(cookieDataCounter);
});

const downloadGCSFile = async (e) => {
  console.log({ e });
  // Remote URL for the file to be downloaded
  const path = $(e).data("path");
  console.log({ path });

  try {
    const response = await POST("/api/gcs/download", { path: path });
    const jsonRes = JSON.parse(response);
    console.log({ jsonRes });
    const splitURL = jsonRes.url.split("/");
    const filename = splitURL[splitURL.length - 1];

    let url = "";
    let add = false;
    for (const e of splitURL) {
      if (e === "gcs_files" || add) {
        add = true;
        url += `/${e}`;
      }
    }

    console.log({ url });
    fetch(url)
      .then((response) => response.blob())
      .then((blob) => {
        const blobURL = URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = blobURL;
        a.style.display = "none";
        a.classList.add("fdfsdfssfsf");
        a.innerHTML =
          "ifjdvdfvndkfjkjvndkfjvndfkvbdkjfvndf kfjvndfnvlfdvfkdjvn";
        if (filename && filename.length) a.download = filename;
        document.body.appendChild(a);

        a.click();
        $(a).remove();
      })
      .catch((error) => {
        console.error(error);
      });
  } catch (err) {
    console.log(err);
  }
};

const setWebsiteLanguage = async (lang) => {
  // const dataParams = findGetParameter("data");
  const loa = findGetParameter("loa");
  const scope = findGetParameter("scope");
  // console.log({ scope }, { loa });

  if (history.pushState && lang) {
    const params = window.location.search;
    const currentLang = findGetParameter("lang");
    const newParams = params.replace(`lang=${currentLang}`, `lang=${lang}`);
    // console.log({ params }, { newParams });
    var newurl =
      window.location.protocol +
      "//" +
      window.location.host +
      window.location.pathname +
      newParams +
      `${window.location.hash}`;
    window.history.pushState({ path: newurl }, "", newurl);
  }

  const greeting = $(".user__greeting .greeting-text").html(
    language[lang].greeting
  );
  const settings = $(".user__dropdown-settings").html(language[lang].settings);
  const logout = $(".user__dropdown-logout").html(language[lang].logout);
  const sidebarNames = $("nav.sidebar .list__item-name");
  for (let i = 0; i < sidebarNames.length; i++) {
    $(sidebarNames[i]).html(language[lang].sidebar[i]);
  }
  const aboutUs = $("footer .footer__about").html(language[lang].aboutUs);
  const contact = $("footer .footer__contact").html(language[lang].contact);
  const rules = $("footer .footer__rules").html(language[lang].rules);

  // load current dynamic content page
  // let activeSidebarElement = $("nav.sidebar .list-item.active a");
  const path = window.location.hash.split("#")[1];
  // console.log({ path });
  // activeSidebarElement[0] &&
  //   (path = $(activeSidebarElement).attr("onclick").split('"')[1]);
  // // console.log({ path });
  if (path && path != "/clipboard" && path != "/businessClient") {
    try {
      const loadingContent = await POST("/loading", "");
      $(".page .page-section").html(loadingContent);
      const pageContent = await POST(path, "");
      $(".page .page-section").html(pageContent);
    } catch (err) {
      // console.log({ err });
      $(".page .page-section").html("Ups, coś poszło nie tak");
    }
  } else if (path) {
    // // console.log("clipboard to load....");
    // reload manage user data from clipboard page
    $(".clipboard__page").length > 0 && postCookiesData();
  }
  // else {
  //   window.location.href = `/businessClient?lang=${lang}&scope=${scope}&loa=${loa}`;
  // }
};

function findGetParameter(parameterName) {
  var result = null,
    tmp = [];
  var items = location.search.substr(1).split("&");
  for (var index = 0; index < items.length; index++) {
    tmp = items[index].split("=");
    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
  }
  return result;
}

// ------------------------------------------------------- //
// HEADER clipboard DROPDOWN
// ------------------------------------------------------ //
$(".clipboard-dropdown-menu").on("click", (e) => {
  !$(e.target).hasClass("manage") && e.stopPropagation();
});

// ------------------------------------------------------- //
// SIDEBAR RENDER PAGE
// ------------------------------------------------------ //
var renderPage = async (e, expressPath, data) => {
  try {
    console.log({ expressPath });
    updateMenu(e);
    window.location.hash = $(e).data("name");
    const loadingContent = await POST("/loading", "");
    $(".page .page-section").html(loadingContent);
    const pageContent = await POST(expressPath, data);
    if ($(e).data("name") === location.hash.split("#")[1]) {
      $(".page .page-section").html(pageContent);
    }
  } catch (err) {
    // console.log({ err });
    $(".page .page-section").html("Ups, coś poszło nie tak");
  }
};

const redirect = (path) => {
  const items = $("nav.sidebar .list-item .list__item-name");
  console.log(path.split("?")[0]);
  for (const item of items) {
    if ($(item).parent().attr("onclick").split('"')[1] === path.split("?")[0]) {
      renderPage($(item).parent()[0], path, "");
      break;
    }
  }
};

const POST = (expressPath, data) => {
  return new Promise((resolve, reject) => {
    // console.log({ expressPath });
    const language = $("nav.navbar .language__active").data("active-language");
    $.ajax({
      url: expressPath.includes("?")
        ? `${expressPath}&lang=${language}`
        : `${expressPath}?lang=${language}`,
      type: "POST",
      data: JSON.stringify({ data }),
      contentType: "application/json",
      dataType: "html",
      error: (err) => {
        reject(err);
        // $(".page .page-section").html('Ups, wystąpił błąd ładowania danych', {err})
      },
      success: (data) => {
        if (history.pushState && language) {
          const params = window.location.search;
          var newurl =
            window.location.protocol +
            "//" +
            window.location.host +
            window.location.pathname +
            (params ? `${params}` : `?lang=${language}`) +
            `${window.location.hash}`;
          window.history.pushState({ path: newurl }, "", newurl);
        }
        resolve(data);
        // $(".page .page-section").html(data)
        // // console.log($('.page-content').css('width'));
      },
    });
  });
};

const GET = (expressPath) => {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: expressPath,
      type: "GET",
      crossDomain: true,
      contentType: "application/json",
      dataType: "html",
      error: (err) => {
        reject(err);
        // $(".page .page-section").html('Ups, wystąpił błąd ładowania danych', {err})
      },
      success: (data) => {
        resolve(data);
        // $(".page .page-section").html(data)
        // // console.log($('.page-content').css('width'));
      },
    });
  });
};

const DELETE = (expressPath, data = null) => {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: expressPath,
      type: "DELETE",
      crossDomain: true,
      contentType: "application/json",
      dataType: "html",
      data: JSON.stringify({ data }),
      success: (result) => {
        console.log({ result });
        resolve(result);
      },
      error: (err) => {
        console.log({ err });
        reject(err);
        // $(".page .page-section").html('Ups, wystąpił błąd ładowania danych', {err})
      },
    });
  });
};

const UPDATE = (expressPath, data = null) => {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: expressPath,
      type: "UPDATE",
      crossDomain: true,
      contentType: "application/json",
      dataType: "html",
      data: JSON.stringify({ data }),
      success: (result) => {
        console.log({ result });
        resolve(result);
      },
      error: (err) => {
        console.log({ err });
        reject(err);
        // $(".page .page-section").html('Ups, wystąpił błąd ładowania danych', {err})
      },
    });
  });
};

const updateMenu = (e) => {
  const userClickItem = $(e);
  const listItems = $("nav.sidebar li");
  // remove all active elements --> remove background blue
  for (const item of listItems) {
    $(item).removeClass("active");
  }
  // add active (background blue) to user click element
  $(e).closest(".list-item").addClass("active");
  // show submenu (click a href) after user click on dropdown item only if dropdown menu is hidden (aria-expanded false)
  if (
    $(e).hasClass("list__item-dropdown") &&
    $(e).next().attr("aria-expanded") === "false"
  )
    $(e).next()[0].click();
  // if user click on diffrent element than drodown item -> collapse all dropdown menu items
  if (!$(e).hasClass("list__item-dropdown")) {
    const dropdownItems = $(".list__item-dropdown");
    for (const dropdownItem of dropdownItems) {
      $(dropdownItem).next().attr("aria-expanded") === "true" &&
        !$(e).hasClass("dropdown__item") &&
        $(dropdownItem).next()[0].click();
    }
  }
};

$(".header-toggle").click(() => {
  if ($("nav.sidebar").css("margin-left") === "0px") {
    $("nav.sidebar").css("margin-left", "-250px");
    $(".page-content").css("width", "100%");
    $(".header-toggle i").toggleClass("bx-menu-alt-right");
  } else {
    $("nav.sidebar").css("margin-left", "0px");
    $(".page-content").css("width", "calc(100% - 250px)");
    $(".header-toggle i").toggleClass("bx-menu-alt-right");
  }
  // $('nav.sidebar').css("margin-left") === "0px" ? ($('nav.sidebar').css('margin-left', '-250px') && $('.page-content').css('width', '100% !important') && $('.header-toggle i').toggleClass('bx-menu-alt-right')) : ($('nav.sidebar').css("margin-left", "0px") && $('.page-content').css('width', 'calc(100% - 250px)') && $('.header-toggle i').toggleClass('bx-menu-alt-right'));
});

var $myGroup = $("#dropdownGroup");

$myGroup.on("show.bs.collapse", () => {
  $myGroup.find(".show").collapse("hide");
});

// clipboard badge
var showBadgeCounter = (counter) => {
  $("#clipboard i").html("");
  if (counter > 0) {
    $("#clipboard i").append(
      `<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" style="font-size: .7rem">
        <span class="message-counter">` +
        counter +
        `</span>
        <span class="visually-hidden"></span>
      </span>`
    );
  }
};

const postCookiesData = async (expressPath = "/clipboard") => {
  // check if not business client page
  if (location.hash.split("#")[1] === "/businessClient") {
    expressPath = "/businessClientUpdate";
  } else window.location.hash = expressPath;

  let cookieHash = readCookie("clipboard");
  // console.log(cookieHash);
  let data = [];
  let req_data;
  if (cookieHash && cookieHash.length > 0) {
    const cookiesData = JSON.parse(
      decodeURIComponent(b64DecodeUnicode(decodeURIComponent(cookieHash[1])))
    );
    data = cookiesData;
    // let cookie = readCookie("clipboard");

    // let cookiesData = cookie.split("%");
    // for (const cookieData of cookiesData) {
    //   data.push(decodeURIComponent(cookieData));
    // }
  }
  // console.log({ data });

  if (expressPath === "/businessClientUpdate") {
    // console.log("jest business client update...");
    const loa_min = $(".warning .warning__text").data("loa");
    // console.log("minimalne loa: ");
    // console.log({ loa_min });
    req_data = { loa_min: loa_min, data: data };
  } else req_data = data;

  // postData(this, '/clipboard-data', data);
  // const expressPath = "/clipboard";
  const listItems = document.querySelectorAll("nav.sidebar li");
  listItems.forEach((listItem) => {
    listItem.classList.remove("active");
    listItem.querySelector("span").innerHTML.toLowerCase() ===
    expressPath.split("/")[expressPath.split("/").length - 1].toLowerCase()
      ? listItem.classList.add("active")
      : console.log();
  });

  try {
    const loadingContent = await POST("/loading", {});
    $(".page .page-section").html(loadingContent);
    // console.log({ expressPath });
    const clipboardContent = await POST(expressPath, req_data);
    $(".page .page-section").html(clipboardContent);
    // window.location.hash = expressPath;
  } catch (err) {
    // console.log({ err });
    $(".page .page-section").html("Ups, coś poszło nie tak");
  }
};

// ------------------------------------------------------- //
// PAGE CONTENT
// ------------------------------------------------------ //
// TOASTS
var showToast = (idPrefix, icon, title, text) => {
  const random = Math.floor(Math.random() * 100);
  $("#toasts-section").prepend(
    `<div id="` +
      idPrefix +
      random +
      `" class="toast mt-2" data-bs-delay="3000" role="alert" aria-live="assertive" aria-atomic="true">
          <div class="toast-header">
          <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
          <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
          </symbol>
          <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
          </symbol>
          <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
          </symbol>
          </svg>` +
      icon +
      `<strong class="me-auto">` +
      title +
      `</strong>
            <small> < 1 minute</small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
          </div>
          <div class="toast-body">` +
      text +
      `</div>
        </div>`
  );

  $("#toasts-section #" + idPrefix + random).toast("show");
  setTimeout(() => {
    document.getElementById(idPrefix + random).remove();
  }, 3500);
};

// clipboard with data
var addToClipboardButton = (e) => {
  const active_lang = $("nav.navbar .language__active").data("active-language");
  const pageBreadcrumbs = document.querySelector("ol.breadcrumb").children;
  let scope = "";
  for (var index = 0; index < pageBreadcrumbs.length; index++) {
    index === pageBreadcrumbs.length - 1
      ? (scope += pageBreadcrumbs[index].dataset.breadcrumbValue)
      : (scope += pageBreadcrumbs[index].dataset.breadcrumbValue + " / ");
  }
  const dataLabel = document.getElementById(
    e.parentElement.parentElement.parentElement.parentElement.id + "-label"
  ).dataset.label;
  // console.log({ dataLabel });
  // w przyszłości pomyslec tutaj nad innym wyciaganiem tych danych a nie z innerHTML
  let dataValue;
  if (dataLabel != "user_img") {
    dataValue = document.getElementById(
      e.parentElement.parentElement.parentElement.parentElement.id + "-value"
    ).innerHTML;
  } else {
    dataValue = document.getElementById(
      e.parentElement.parentElement.parentElement.parentElement.id + "-value"
    ).src;
  }

  const source = $(e).parent().parent().parent().find(".method").data("source");
  const updated = $(e)
    .parent()
    .parent()
    .parent()
    .parent()
    .parent()
    .find(".user__data-value")
    .data("updated");
  // console.log({ source });
  // if (dataValue === " ") {
  //   showToast('_danger-no-data', '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>', scope, `${dataLabel} ${language[location.hash.split('=')[1]].toast.empty}`);
  //   return;
  // }

  // console.log({ dataValue });
  let cookieHash = readCookie("clipboard");
  let cookiesData = [];
  let cookieDataCounter = 0;
  // console.log(cookieHash);
  if (cookieHash && cookieHash.length > 0) {
    cookiesData = JSON.parse(
      decodeURIComponent(b64DecodeUnicode(decodeURIComponent(cookieHash[1])))
    );
    // let cookie = readCookie("clipboard");
    // console.log({ cookiesData });
    // // console.log(decodeURIComponent(cookie));

    // let cookiesData = cookie.split("%");
    cookieDataCounter = cookiesData.length;
    for (const cookieData of cookiesData) {
      // console.log({ cookieData });
      if (cookieData.dataLabel === dataLabel && cookieData.scope === scope) {
        showToast(
          "_warning-repeated",
          '<svg class="bi flex-shrink-0 me-2 blue" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>',
          scope,
          `${language[active_lang].all[dataLabel]} ${language[active_lang].toast.already__added}`
        );
        return;
      }
    }
  }

  let loa = $(".headline__loa").data("loaLevel");

  const newCookieData = {
    dataLabel: dataLabel,
    dataValue: dataValue,
    updated_at: updated,
    scope: scope,
    loa: loa,
    source: source ? source : "",
  };

  cookiesData.push(newCookieData);
  // liczymy base64
  const encodedCookie = btoa(encodeURIComponent(JSON.stringify(cookiesData)));
  document.cookie = "clipboard=" + encodedCookie + ";path=/";
  cookieDataCounter++;
  showBadgeCounter(cookieDataCounter);
  showToast(
    "_add-success",
    '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
    scope,
    `${language[active_lang].all[dataLabel]} ${language[active_lang].toast.added_succes}`
  );
};

var showClipboardItems = (e) => {
  if (!$(e).hasClass("show")) {
    updateItemsInClipboard();
  }
};

const updateCookiesData = (newData) => {
  return new Promise(async (resolve) => {
    // console.log({ newData });
    let cookieHash = readCookie("clipboard");
    // console.log(cookieHash);
    if (cookieHash && cookieHash.length > 0) {
      const cookiesData = JSON.parse(
        decodeURIComponent(b64DecodeUnicode(decodeURIComponent(cookieHash[1])))
      );
      let cookieDataCounter = cookiesData.length;

      let newCookie = [];
      for await (const [index, cookieData] of cookiesData.entries()) {
        if (newData[cookieData.dataLabel] != undefined) {
          if (
            cookieData.updated_at < newData[cookieData.dataLabel].updated_at
          ) {
            cookieDataCounter--;
          } else {
            newCookie.push(cookieData);
          }
        } else newCookie.push(cookieData);
      }
      showBadgeCounter(cookieDataCounter);
      // console.log({ newCookie });
      document.cookie =
        "clipboard=" + btoa(encodeURIComponent(JSON.stringify(newCookie)));
      +";path=/";
      // document.cookie = 'clipboard=' + cookie[1] + '%' + JSON.stringify(newCookieData) + ";path=/" : document.cookie = 'clipboard=' + JSON.stringify(newCookieData) + ";path=/";
      resolve();
    } else {
      resolve();
    }
  });
};

var b64DecodeUnicode = (str) => {
  // Going backwards: from bytestream, to percent-encoding, to original string.
  return decodeURIComponent(
    atob(str)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
};

var updateItemsInClipboard = () => {
  // // console.log('update items in clipboard');
  const active_lang = $("nav.navbar .language__active").data("active-language");
  $("ul.clipboard-dropdown-menu").html("");
  let cookieHash = readCookie("clipboard");
  // console.log(cookieHash);
  if (cookieHash && cookieHash.length > 0) {
    // console.log(cookieHash[1]);
    const cookiesData = JSON.parse(
      decodeURIComponent(b64DecodeUnicode(decodeURIComponent(cookieHash[1])))
    );
    // let cookie = readCookie("clipboard");
    // let cookiesData = cookie.split("%");
    if (cookiesData.length != 0) {
      for (const [index, cookieData] of cookiesData.entries()) {
        // console.log({ cookieData });
        // const cookieData = JSON.parse(decodeURIComponent(cookieData));
        let borderBottom = "";
        index === 0
          ? (borderBottom = "border-none")
          : (borderBottom = "border-bottom");

        // get page or subpage from language dictionary with cookies scope
        const pages = cookieData.scope.split(" / ");
        let nextPage = language[active_lang];
        let page;
        let breadcrumbTranslated = "";
        for (let i = 0; i < pages.length; i++) {
          page = nextPage.page[pages[i]];
          i === pages.length - 1
            ? (breadcrumbTranslated += page.name)
            : (breadcrumbTranslated += page.name + " / ");
          nextPage = page;
        }
        // // console.log({page});
        // // console.log({breadcrumbTranslated});
        // i have a page in dictionary
        // ========================================
        if (cookieData.dataLabel != "user_img") {
          $("ul.clipboard-dropdown-menu").prepend(
            `<li>
                  <div class="clipboard-item dropdown-item ` +
              borderBottom +
              `">
                    <div class="row ms-2 me-2 justify-content-between">
                      <div class="clipboard__data col-10 col-md-8 my-auto">
                        <p class="clipboard__item-data clipboard__data-label text-wrap my-0" data-val="${cookieData.dataLabel}">` +
              page.dataLabels[cookieData.dataLabel] +
              `</p>
                        <p class="clipboard__item-data clipboard__data-value text-wrap my-0">` +
              cookieData.dataValue +
              `</p>
                      </div>
                      <div class="col-2 p-0 d-flex align-items-center justify-content-center" data-bs-toggle="tooltip" data-bs-placement="top" title="${language[active_lang].tooltips.delete_item}">
                        <a href="javascript:void(0);" onclick="deleteItemFromClipboard(this)"><i class='clipboard__item-delete bx bx-trash header-color'></i></a>
                      </div>
                    </div>
                    <div class="clipboard-source my-1 ms-2"><small data-val="${cookieData.scope}">` +
              breadcrumbTranslated +
              `</small></div>
                  </div>
                </li>`
          );
        } else {
          $("ul.clipboard-dropdown-menu").prepend(
            `<li>
                  <div class="clipboard-item dropdown-item ` +
              borderBottom +
              `">
                    <div class="row ms-2 me-2 justify-content-between">
                      <div class="clipboard__data col-10 col-md-8 my-auto">
                        <p class="clipboard__item-data clipboard__data-label text-wrap my-0" data-val="${cookieData.dataLabel}">` +
              page.dataLabels[cookieData.dataLabel] +
              `</p>
                        <img class="clipboard__item-data" src="` +
              cookieData.dataValue +
              `" alt="" style="display:inline-block; width:36px; height:45px;">
                      </div>
                      <div class="col-2 p-0 d-flex align-items-center justify-content-center" data-bs-toggle="tooltip" data-bs-placement="top" title="${language[active_lang].tooltips.delete_item}">
                        <a href="javascript:void(0);" onclick="deleteItemFromClipboard(this)"><i class='clipboard__item-delete bx bx-trash header-color'></i></a>
                      </div>
                    </div>
                    <div class="clipboard-source my-1 ms-2"><small data-val="${cookieData.scope}">` +
              breadcrumbTranslated +
              `</small></div>
                  </div>
                </li>`
          );
        }
      }

      $("ul.clipboard-dropdown-menu")
        .append(`<div class="d-flex align-items-center clipboard-button-apply">
    <span class="clipboard__button material-icons-outlined" onclick="deleteAllFromClipboard()" data-bs-toggle="tooltip" data-bs-placement="top" title='${language[active_lang].tooltips.delete_all}'>cleaning_services</span></a>
    <span class="clipboard__button manage material-icons-outlined" onclick='postCookiesData()' data-bs-toggle="tooltip" data-bs-placement="top" title='${language[active_lang].tooltips.manage}'>edit_note</span>
    </div>`);
    } else {
      $("ul.clipboard-dropdown-menu").append(
        `<p class="clipboard-empty m-0 p-4 ms-3">${language[active_lang].clipboard}</p>`
      );
    }
  } else {
    $("ul.clipboard-dropdown-menu").append(
      `<p class="clipboard-empty m-0 p-4 ms-3">${language[active_lang].clipboard}</p>`
    );
  }
};

var deleteItemFromClipboard = (e) => {
  const data = e.parentElement.parentElement.querySelectorAll(
    ".clipboard__item-data"
  );
  const scope = e.parentElement.parentElement.parentElement.querySelector(
    ".clipboard-source small"
  ).dataset.val;

  let cookieHash = readCookie("clipboard");
  // console.log(cookieHash);
  if (cookieHash && cookieHash.length > 0) {
    const cookiesData = JSON.parse(
      decodeURIComponent(b64DecodeUnicode(decodeURIComponent(cookieHash[1])))
    );
    // let cookie = readCookie("clipboard");
    let cookieDataCounter = 0;
    // let cookiesData = cookie.split("%");
    cookieDataCounter = cookiesData.length;
    for (let index = 0; index < cookiesData.length; index++) {
      let clipboardValue;
      if (data[0].dataset.val != "user_img") {
        clipboardValue = data[1].innerHTML;
      } else {
        clipboardValue = data[1].src;
      }
      if (
        cookiesData[index].dataLabel === data[0].dataset.val &&
        cookiesData[index].dataValue === clipboardValue &&
        cookiesData[index].scope === scope
      ) {
        cookiesData.splice(index, 1);
      }
    }

    document.cookie =
      "clipboard=" + btoa(encodeURIComponent(JSON.stringify(cookiesData)));
    +";path=/";
    cookieDataCounter--;
    showBadgeCounter(cookieDataCounter);
    updateItemsInClipboard();
    $(".clipboard__page").length > 0 && postCookiesData();
  }
};

var deleteAllFromClipboard = async () => {
  const active_lang = $("nav.navbar .language__active").data("active-language");
  const dictionary = language[active_lang];
  const text = dictionary.modal.delete_all;
  const buttonsText = [dictionary.modal.no, dictionary.modal.yes];
  // // console.log('jestem przed show modal');
  const clicked = await showModal(text, buttonsText);
  if (clicked.dataset.modalButton.toLowerCase() === "yes") {
    document.cookie = "clipboard=;path=/";
    showBadgeCounter();
    updateItemsInClipboard();
    $(".clipboard__page").length > 0 && postCookiesData();
  }
};

const showModal = (text, buttonsText) => {
  return new Promise((resolve) => {
    const active_lang = $("nav.navbar .language__active").data(
      "active-language"
    );
    const modal = $(".custom__modal");
    const buttons = $(
      ".custom__modal .custom__modal-content .custom__modal-content-button"
    );
    const warningText = $(
      ".custom__modal .custom__modal-content .custom__modal-content-warning"
    );
    const modalText = $(
      ".custom__modal .custom__modal-content .custom__modal-content-text"
    );
    $(warningText).html(language[active_lang].modal.warning);
    $(modalText).html(text);
    // change visibility and z-index
    $(modal).css({ display: "block", "z-index": "10" });
    for (let i = 0; i < buttons.length; i++) {
      $(buttons[i]).attr("value", buttonsText[i]);
      $(buttons[i]).on("click", (e) => {
        $(buttons).off();
        $(modal).css({ display: "none", "z-index": "-1" });
        resolve(e.currentTarget);
      });
    }
  });
};

var readCookie = (name) => {
  var result = document.cookie.match(new RegExp(name + "=([^;]+)"));
  return result;
};

// parse token
const parseJwt = (token) => {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  var jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map((c) => {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );

  return JSON.parse(jsonPayload);
};

const closeButton = $(".loading__transparent .loading__transparent-close");
const loadingFullScreen = (obj, callback = () => {}) => {
  const loading = $(".loading__transparent");
  const loadingContent = $(".loading__transparent .loading__content");
  const contentIcon = $(
    ".loading__transparent .loading__content .content-icon"
  );
  const contentText = $(
    ".loading__transparent .loading__content .content-text"
  );

  const background = obj?.background || "rgb(244,248,253)";
  if (obj.event && obj.event === "show") {
    // object background
    $(loading).css("background", background);
    // object icon
    $(contentIcon).html(
      `${
        obj.icon
          ? obj.icon
          : '<span class="material-icons-outlined">pending</span>'
      }`
    );
    // object text
    $(contentText).html(`${obj.text}`);
    // close
    $(closeButton).css("display", "block");

    $(loading).css("display", "flex");

    setTimeout(() => {
      $(loading).css("margin-left", "0");
    }, 200);

    $(closeButton).unbind("click");
    $(closeButton).on("click", () => {
      loadingFullScreen({ event: "close_all" });
      callback();
    });
  }

  if (obj.event && obj.event === "done") {
  }

  if (obj.event && obj.event === "close") {
    // object background
    $(loading).css("background", "transparent");
    // object icon
    $(contentIcon).html("");
    // object text
    $(contentText).html("");

    // $(loading).css("display", "none");

    $(loading).css("margin-left", "-100%");

    setTimeout(() => {
      $(loading).css("display", "none");
      $(loading).css("margin-left", "100%");
    }, 500);
  }

  if (obj.event && obj.event === "close_all") {
    mainBackgroundView({ event: "close" });
    loadingFullScreen({ event: "close" });
  }

  if (obj.event && obj.event === "hide") {
    if (obj.object && obj.object === "close_button") {
      $(closeButton).css("display", "none");
    }
  }

  if (obj.event && obj.event === "visible") {
    if (obj.object && obj.object === "close_button") {
      $(closeButton).css("display", "flex");
    }
  }

  return contentText;
};

var windows = {};
const appCustomWindow = (obj) => {
  const width = obj?.width || "550";
  const height = obj?.height || "650";
  const left = screen.width / 2 - width / 2;
  const top = (screen.height - height - 55) / 2;
  // const left = obj?.top || "0";
  const name = obj?.name || "cyberPW";
  const url = obj?.url || "_empty";

  // "menubar=1,resizable=1,width=550,height=650"
  if (obj.event === "show") {
    windows[name] = window.open(
      url,
      name,
      `menubar=1, resizable=1, width=${width}, height=${height}, top=${top}, left=${left}`
    );
  }

  if (obj.event === "close") {
    if (name) {
      const window = windows[name];
      if (window) {
        window.close();
        delete windows[name];
      }
    }
  }

  return windows[name];
};

const userDataView = (obj) => {
  // console.log({ obj });
  const lang = $("nav.navbar .language__active").data("active-language");
  const dataView = $(".data__view");
  const text = $(".data__view .data__view-text");
  const source = $(".data__view .data__view-source");
  const dataViewWrapper = $(".data__view .data__view-wrapper");
  const acceptButton = $(
    '.data__view .data__view-buttons .button[data-label="accept"]'
  );
  const rejectButton = $(
    '.data__view .data__view-buttons .button[data-label="reject"]'
  );

  if (obj.event && obj.event === "show") {
    const background = obj?.background || "rgb(244,248,253)";
    $(text).html(obj.text ? obj.text : `${language[lang].data_view_text}`);
    if ($.isEmptyObject(obj.data)) {
      $(acceptButton).css("display", "none");
      $(rejectButton).css("margin", "auto");
      $(rejectButton).html(
        `<i class="fas fa-times"></i> ${language[lang].custom_modal.upload.back}`
      );
      // brak danych, wypisujemy ze nie znalezlismy danych pasujacych
      if (!obj.jsonStoragePath) {
        $(source).html(`${language[lang].source.empty}`);
      } else {
        $(source).html(
          `<a href='javascript:void(0)' data-path=${obj.jsonStoragePath} onClick="downloadGCSFile(this)">nie znaleźliśmy danych, których poszukiwałeś - kliknij tutaj, aby sprawdzić dane, które zostały pobrane</a>`
        );
      }
      $(dataView).css("display", "flex");
      setTimeout(() => {
        $(dataView).css("margin-left", "0");
      }, 200);
      return [null, rejectButton];
    }

    $(acceptButton).css("display", "block");
    $(acceptButton).html(
      `<i class="fas fa-check"></i> ${language[lang].custom_modal.upload.accept}`
    );
    $(rejectButton).css("margin", "0");
    $(rejectButton).html(
      `<i class="fas fa-times"></i> ${language[lang].custom_modal.upload.reject}`
    );

    let src, loa;
    for (const key in obj.data) {
      src = obj.data[key].data.source.toLowerCase() || "unknown";
      loa = obj.data[key].data.loa.toLowerCase() || "unknown";
      break;
    }

    $(source).html(
      src
        ? `${language[lang].source[src]} <span class="ms-2 material-icons-outlined" data-bs-toggle="tooltip" data-bs-placement="right" title="${language[lang].tooltips_loa[loa]}">info</span>`
        : "brak źródła danych"
    );
    let color;
    if (loa === "high") color = "var(--bs-green)";
    else if (loa === "medium") color = "var(--bs-orange)";
    else if (loa === "low") color = "var(--bs-red)";
    else color = "black";
    $(source).attr("style", `color: ${color} !important`);
    const data = obj.data ? obj.data : "brak danych";
    $(dataView).css("background", background);
    for (const scope in obj.data) {
      $(dataViewWrapper).append(
        `<p class="scope">${language[lang].scope[scope]}</p><div class="data__view-group ${scope}"></div>`
      );
      const viewGroup = $(`.data__view .data__view-group.${scope}`);
      // console.log({ viewGroup });
      for (const key in obj.data[scope].data) {
        if (
          key != "timestamp" &&
          key != "user_id" &&
          key != "source" &&
          key != "loa" &&
          key != "updated_at" &&
          obj.data[scope].data[key] != "" &&
          obj.data[scope].data[key] != " "
        ) {
          $(viewGroup).append(`<div class="data__row">
            <p class="data__label">${language[lang].all[key]}</p>
            <span>: </span>
            <p class="data__value">${obj.data[scope].data[key]}</p>
          </div>
          <li role="separator" class="mdc-list-divider"></li>`);
        }
      }
    }

    $(dataView).css("display", "flex");

    setTimeout(() => {
      $(dataView).css("margin-left", "0");
    }, 200);

    $('[data-bs-toggle="tooltip"]').tooltip();

    return [acceptButton, rejectButton];
  }

  if (obj.event && obj.event === "close") {
    $(dataViewWrapper).html("");
    $(source).html("");
    $(acceptButton).unbind("click");
    $(rejectButton).unbind("click");

    $(dataView).css("margin-left", "-100%");

    setTimeout(() => {
      $(dataView).css("display", "none");
      $(dataView).css("margin-left", "100%");
    }, 500);
  }
};

const mainBackgroundView = (obj) => {
  const mainBackground = $(".main__background");
  if (obj.event && obj.event === "show") {
    $(mainBackground).css("display", "flex");

    setTimeout(() => {
      $(mainBackground).css("margin-left", "0");
    }, 200);
  }

  if (obj.event && obj.event === "close") {
    $(mainBackground).css("margin-left", "-100%");

    setTimeout(() => {
      $(mainBackground).css("display", "none");
      $(mainBackground).css("margin-left", "100%");
    }, 500);
  }
};
