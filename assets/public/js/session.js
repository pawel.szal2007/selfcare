var warningTimeout = 180000;
var timoutNow = 60000;
var warningTimerID, timeoutTimerID;

function startTimer() {
  // window.setTimeout returns an Id that can be used to start and stop a timer
  warningTimerID = window.setTimeout(warningInactive, warningTimeout);
}

function warningInactive() {
  window.clearTimeout(warningTimerID);
  timeoutTimerID = window.setTimeout(IdleTimeout, timoutNow);
  var myModal = new bootstrap.Modal(document.getElementById("myModal"), {
    keyboard: false,
  });
  myModal.show();
  // $("#modalAutoLogout").modal("show");
}

function resetTimer() {
  window.clearTimeout(timeoutTimerID);
  window.clearTimeout(warningTimerID);
  startTimer();
}

// Logout the user.
function IdleTimeout() {
  // document.getElementById("logout-form").submit();
  logout();
}

function setupTimers() {
  document.addEventListener("mousemove", resetTimer, false);
  document.addEventListener("mousedown", resetTimer, false);
  document.addEventListener("keypress", resetTimer, false);
  document.addEventListener("touchmove", resetTimer, false);
  document.addEventListener("onscroll", resetTimer, false);
  startTimer();
}

var logout = () => {
  window.location.href = "/Logout";
};

$(document).ready(function () {
  setupTimers();
});
