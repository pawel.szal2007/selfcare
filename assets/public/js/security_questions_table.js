var security_questions_tab = () => {
  return [
    {
      category: "car",
      question: "Twój pierwszy samochód?",
    },
    {
      category: "animal",
      question: "imię Twojego pierwszego zwierzaka?",
    },
    {
      category: "nickname",
      question: "Twój pierwszy pseudonim?",
    },
    {
      category: "school",
      question: "nazwa Twojej pierwszej szkoły?",
    },
    {
      category: "future",
      question: "wymarzony zawód z dzieciństwa?",
    },
    {
      category: "love",
      question: "imię Twojej pierwszej miłości?",
    },
    {
      category: "custom",
      question: "dodaj swoje własne pytanie pomocnicze",
    },
  ];
};
