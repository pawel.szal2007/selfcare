var [
  changePassword,
  changePIN,
  changeQuestion,
  PINVerification,
  passwordVerification,
  questionVerification,
  removePIN,
  removePassword,
  removeSecurityQuestion,
] = (() => {
  $(document).ready(() => {
    const phoneid_btn = $("#phoneid__btn");
    console.log({ phoneid_btn });
    const clone_btn = phoneid_btn.clone(true, true);
    $(".phone__number-manager").append(clone_btn);
    $(clone_btn).css("display", "block");

    // check user data collapsible elements on page
    const settingsDataRows = $(".page .page-content .settings__data-name");
    let currentElement;
    for (const settingsRow of settingsDataRows) {
      $(settingsRow).on("click", (e) => {
        // console.log(e.currentTarget);
        const collapsibleElements = $(
          ".page .page-content .settings__data-collapse"
        );
        for (const element of collapsibleElements) {
          // // console.log($(element).parent().find(".settings__data-label"));
          $(element).prev().removeClass("settings-active");
          $(element).collapse("hide");
        }
        // currentElement != e.currentTarget && $(e.currentTarget).parent().addClass('main-background')
        // const settingLabel = $(e.currentTarget).find(".settings__data-label");

        currentElement != e.currentTarget &&
          $(e.currentTarget).addClass("settings-active");

        $(e.currentTarget)
          .parent()
          .find(".settings__data-collapse")
          .collapse("show");
        if (currentElement != e.currentTarget) {
          currentElement = e.currentTarget;
        } else {
          currentElement = "";
        }
      });
    }
  });

  // PIN SECTION
  const changePIN = async (obj) => {
    const oldPINInput = $(
      ".settings__dropdown-group .settings__dropdown-input.pin__old-input"
    );
    const newPINInput = $(
      ".settings__dropdown-group .settings__dropdown-input.pin__new-input"
    );
    const newPINRepeatedInput = $(
      ".settings__dropdown-group .settings__dropdown-input.pin__new-repeat-input"
    );
    // console.log("change PIN function...");

    const oldPIN = oldPINInput.val();
    const newPIN = newPINInput.val();
    const newPINRepeated = newPINRepeatedInput.val();

    oldPINInput.val("");
    newPINInput.val("");
    newPINRepeatedInput.val("");
    // // console.log({ oldPIN, newPIN, newPINRepeated });

    if (newPIN === "" || newPINRepeated === "") {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "pola nie mogą być puste"
      );
      return;
    }
    if (newPIN != newPINRepeated) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "powtórzony PIN jest nieprawidłowy"
      );
      return;
    }

    const response = await POST("/settings/changePIN", {
      old_pin: oldPIN,
      new_pin: newPIN,
    });

    console.log(JSON.parse(response));
    if (JSON.parse(response).status === "Success") {
      const pin = $(".pin__card-content").find(".text");
      if (pin.length === 0) {
        $(".pin__card-content").html(`PIN: <span class="text">****</span>`);
        $(".pin__card-content").data("defined", "true");
        $(".pin__manager .dropdown-menu").append(
          `<li><a class="dropdown-item" href="javascript:void(0)" data-credential="all" data-active="nav-pin-tab" data-bs-toggle="modal" data-bs-target="#staticCredentialsBackdrop">usuń</a></li>`
        );
        $(".modal .tab-pane .pin__tab-content")
          .prepend(`<div class="settings__dropdown-group">
          <span class="settings__dropdown-label pin__old-text">PIN</span>
          <input class="settings__dropdown-input pin__old-input" type="password" name="" value="">
        </div>`);
      }

      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "ustawienia",
        `PIN został zmieniony`
      );
    } else {
      if (JSON.parse(response).error === "wrong pin") {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "nieprawidłowy stary kod PIN, spróbuj ponownie"
        );
      } else {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "PIN nie został zmieniony"
        );
      }
    }
  };

  // password section
  const changePassword = async (obj) => {
    const oldPasswordInput = $(
      ".settings__dropdown-group .settings__dropdown-input.password__old-input"
    );
    const newPasswordInput = $(
      ".settings__dropdown-group .settings__dropdown-input.password__new-input"
    );
    const newPasswordRepeatedInput = $(
      ".settings__dropdown-group .settings__dropdown-input.password__new-repeat-input"
    );
    // console.log("change password function...");

    const oldPassword = oldPasswordInput.val();
    const newPassword = newPasswordInput.val();
    const newPasswordRepeated = newPasswordRepeatedInput.val();

    oldPasswordInput.val("");
    newPasswordInput.val("");
    newPasswordRepeatedInput.val("");

    if (newPassword === "" || newPasswordRepeated === "") {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "pola nie mogą być puste"
      );
      return;
    }

    if (newPassword != newPasswordRepeated) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "powtórzone hasło jest nieprawidłowe"
      );
      return;
    }

    const response = await POST("/settings/changePassword", {
      old_password: oldPassword,
      new_password: newPassword,
    });
    // console.log(JSON.parse(response));
    // // console.log(JSON.parse(response).status === "Success");
    if (JSON.parse(response).status === "Success") {
      const password = $(".password__card-content").find(".text");
      if (password.length === 0) {
        $(".password__card-content").html(
          `hasło: <span class="text">********</span>`
        );
        $(".password__card-content").data("defined", "true");
        $(".modal .tab-pane .password__tab-content")
          .prepend(`<div class="settings__dropdown-group">
          <span class="settings__dropdown-label password__old-text">hasło</span>
          <input class="settings__dropdown-input password__old-input" type="password" name="" value="">
        </div>`);
        $(".password__manager .dropdown-menu").append(
          `<li><a class="dropdown-item" href="javascript:void(0)" data-credential="all" data-active="nav-psssword-tab" data-bs-toggle="modal" data-bs-target="#staticCredentialsBackdrop">usuń</a></li>`
        );
      }

      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "ustawienia",
        `hasło zostało zmienione`
      );
    } else {
      if (JSON.parse(response).error === "wrong password") {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "podałeś nieprawidłowe hasło, spróbuj jeszcze raz"
        );
      } else {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "hasło nie zostało zmienione"
        );
      }
    }
  };

  const changeQuestion = async (obj) => {
    // question to change is saved in button attribute
    const questionToChange = $(".modal .modal-footer .settings__save").data(
      "question"
    );
    const customQuestionInput = $(
      "#nav-tabContent #nav-questions .custom_question-input"
    );
    const answerInput = $("#nav-tabContent #nav-questions .answer-input");

    const customQuestion = $(customQuestionInput).val();
    const answer = $(answerInput).val();
    console.log({ questionToChange }, { customQuestion }, { answer });

    if (!answer || !customQuestion) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "pola nie mogą pozostać puste"
      );
      return;
    }

    $(customQuestionInput).val("");
    $(answerInput).val("");

    try {
      const response = await POST("/settings/changeQuestion", {
        replaced: questionToChange,
        question: customQuestion,
        answer: answer,
      });
      const jsonRes = JSON.parse(response);

      if (jsonRes.status === "Success") {
        showToast(
          "_add-success",
          '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
          "ustawienia",
          `pytanie pomocnicze zostało zmienione`
        );

        const res = await GET("/settings/securityQuestions");
        const json = JSON.parse(res);

        updateUserDefinedSecurityQuestions(json);
      } else {
        // cos innego sie zadziało i nie zostało zmienione
      }
    } catch (err) {
      console.log({ err });
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "pytanie pomocnicze nie zostało zmienione"
      );
    }
  };

  const PINVerification = async () => {
    return new Promise(async (resolve, reject) => {
      const oldPINInput = $(
        ".settings__dropdown-group .settings__dropdown-input.pin__old-input"
      );
      const pin = oldPINInput.val();
      oldPINInput.val("");
      console.log({ pin });
      if (!pin || pin === "") {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "pola nie mogą być puste"
        );
        reject("empty field");
        return;
      }

      try {
        const response = await POST("/settings/verifyPIN", { pin: pin });
        const jsonRes = JSON.parse(response);
        console.log({ jsonRes });
        if (jsonRes.status === "Success") {
          resolve();
        } else reject();
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  };

  const passwordVerification = () => {
    return new Promise(async (resolve, reject) => {
      const oldPasswordInput = $(
        ".settings__dropdown-group .settings__dropdown-input.password__old-input"
      );
      const password = oldPasswordInput.val();
      oldPasswordInput.val("");
      if (!password || password === "") {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "pola nie mogą być puste"
        );
        reject("empty field");
        return;
      }

      try {
        const response = await POST("/settings/verifyPassword", {
          password: password,
        });
        const jsonRes = JSON.parse(response);
        console.log({ jsonRes });
        if (jsonRes.status === "Success") {
          resolve();
        } else reject();
      } catch (err) {
        reject(err);
      }
    });
  };

  const questionVerification = () => {
    return new Promise(async (resolve, reject) => {
      const questionTag = $("#nav-questions .question__div select").find(
        ":selected"
      );
      const category = $(questionTag).data("category");
      const question = $(questionTag).text();

      const answerTag = $("#nav-questions .answer__div input");
      const answer = $(answerTag).val();
      $(answerTag).val("");

      if (!answer || answer === "") {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "musisz udzielić odpowiedzi na pytanie"
        );
        reject("empty field");
        return;
      }

      const data = {
        category: category,
        question: question,
        answer: answer,
      };
      console.log({ data });

      try {
        const response = await POST("/settings/verifyQuestion", data);
        const jsonRes = JSON.parse(response);
        console.log({ jsonRes });
        if (jsonRes.status === "Success") {
          resolve();
        } else reject();
      } catch (err) {
        reject(err);
      }
    });
  };

  const removePIN = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await DELETE("/settings/userPIN");
        const jsonRes = JSON.parse(response);
        console.log({ jsonRes });
        if (jsonRes.status === "Success") {
          resolve();
        } else reject();
      } catch (err) {
        reject(err);
      }
    });
  };

  const removePassword = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await DELETE("/settings/userPassword");
        const jsonRes = JSON.parse(response);
        if (jsonRes.status === "Success") {
          resolve();
        } else reject();
      } catch (err) {
        reject(err);
      }
    });
  };

  const removeSecurityQuestion = (q) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await DELETE("/settings/securityQuestion", {
          question: q,
        });
        const jsonRes = JSON.parse(response);
        if (jsonRes.status === "Success") {
          resolve();
        } else reject();
      } catch (err) {
        reject(err);
      }
    });
  };

  return [
    changePassword,
    changePIN,
    changeQuestion,
    PINVerification,
    passwordVerification,
    questionVerification,
    removePIN,
    removePassword,
    removeSecurityQuestion,
  ];
})();

var closeTabModal = () => {
  $("#staticCredentialsBackdrop").modal("hide");
};

var removeCredential = async (e = null) => {
  closeTabModal();
  if ($(".modal .modal-footer .settings__save").data("del") === "nav-pin-tab") {
    try {
      await removePIN();
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "ustawienia",
        `PIN został usunięty`
      );
    } catch (err) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "PIN nie został usunięty z bazy danych"
      );
      return;
    }

    const pin = $(".pin__card-content").find(".text");
    if (pin.length > 0) {
      $(".pin__card-content").html(`PIN:`);
      $(".pin__card-content").data("defined", "false");
      $(".modal .tab-pane .pin__tab-content .pin__old-input").parent().remove();
      $(
        ".pin__manager .dropdown-menu .dropdown-item[data-credential='all']"
      ).remove();
    }
  } else if (
    $(".modal .modal-footer .settings__save").data("del") === "nav-password-tab"
  ) {
    try {
      await removePassword();
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "ustawienia",
        `hasło zostało usunięte`
      );
    } catch (err) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "hasło nie zostało usunięty z bazy danych"
      );
      return;
    }

    const password = $(".password__card-content").find(".text");
    if (password.length > 0) {
      $(".password__card-content").html(`hasło:`);
      $(".password__card-content").data("defined", "false");
      $(".modal .tab-pane .password__tab-content .password__old-input")
        .parent()
        .remove();
      $(
        ".password__manager .dropdown-menu .dropdown-item[data-credential='all']"
      ).remove();
    }
  } else if (
    $(".modal .modal-footer .settings__save").data("del") ===
    "nav-questions-tab"
  ) {
    try {
      console.log({ e });
      const question = $(e.currentTarget).data("question");
      console.log(`do usuniecia jest pytanie ${question}`);
      await removeSecurityQuestion(question);
      showToast(
        "_add-success",
        '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
        "ustawienia",
        `pytanie pomocnicze zostało usunięte zostało usunięte`
      );
    } catch (err) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "ustawienia",
        "pytanie pomocnicze nie zostało usunięte z bazy danych"
      );
      return;
    }

    const res = await GET("/settings/securityQuestions");
    const jsonRes = JSON.parse(res);

    updateUserDefinedSecurityQuestions(jsonRes);

    questions = security_questions_tab();
    console.log({ questions });
    removeFromQuestions(jsonRes);
    addSecurityQuestionsSelects(questions);
  }
};

var credentialsVerification = (credential) => {
  return new Promise(async (resolve, reject) => {
    if ($(credential).data("bs-target") === "#nav-pin") {
      try {
        await PINVerification();
        resolve();
      } catch (err) {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "nieudane uwierzytelnienie pinem"
        );
        reject(err);
      }
    } else if ($(credential).data("bs-target") === "#nav-password") {
      try {
        await passwordVerification();
        resolve();
      } catch (err) {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "nieudane uwierzytelnienie hasłem"
        );
        reject(err);
      }
    } else if ($(credential).data("bs-target") === "#nav-questions") {
      try {
        await questionVerification();
        resolve();
      } catch (err) {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "ustawienia",
          "nieudane uwierzytelnienie pytaniem pomocniczym"
        );
        reject(err);
      }
    } else {
      console.log("nie obsluguje jeszcze innych uwierzytelnień...");
      reject();
    }
  });
};

var credentialProcess = async (e) => {
  console.log({ e });
  const credential = $(".modal .nav .nav-link.active");
  if ($("#staticBackdropLabel").data("task") === "remove") {
    // weryfkacja pinu i hasła
    try {
      await credentialsVerification(credential);
    } catch (err) {
      closeTabModal();
      return;
    }
    removeCredential(e);
  } else if ($("#staticBackdropLabel").data("task") === "define") {
    if ($(credential).data("bs-target") === "#nav-pin") {
      changePIN();
    } else if ($(credential).data("bs-target") === "#nav-password") {
      changePassword();
    } else if ($(credential).data("bs-target") === "#nav-questions") {
      changeQuestion();
    }
  }
  closeTabModal();
};

var saveButtons = $(".settings__save");
for (const button of saveButtons) {
  $(button).unbind("click");
  if ($(button).data("name") === "questions") {
    $(button).on("click", saveQuestions);
  }
}

var modalTabSaveButtons = $(".modal .modal-footer .settings__save");
for (const button of modalTabSaveButtons) {
  $(button).unbind("click");
  $(button).on("click", credentialProcess);
}

var resendActivationLink = async (e) => {
  try {
    const data = {
      email: $(e)
        .parent()
        .parent()
        .parent()
        .parent()
        .find(".email__card-content")
        .data("name"),
    };
    console.log({ data }, { e });
    const response = await POST("/settings/emails/resend", data);
    const resJson = JSON.parse(response);

    console.log({ resJson });

    showToast(
      "_add-success",
      '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
      "ustawienia",
      `link aktywacyjny został wysłany`
    );
  } catch (err) {
    console.log(err);
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "ustawienia",
      "link aktywacyjny nie został wysłany"
    );
  }
};
