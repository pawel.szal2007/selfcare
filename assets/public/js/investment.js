(() => {
  $(document).ready(() => {
    const investmentDataRows = $(
      ".page .page-content .survey__data-row .data-row"
    );
    let currentElement;
    for (const invRow of investmentDataRows) {
      $(invRow).unbind("click");
      $(invRow).on("click", (e) => {
        // console.log("on click.... jest");
        const collapsibleElements = $(
          ".page .page-content .investment__data-collapse"
        );
        for (const element of collapsibleElements) {
          // // console.log($(element).parent().find(".settings__data-label"));
          $(element)
            .parent()
            .parent()
            .find(".investment__data-label")
            .removeClass("settings-active");
          $(element).collapse("hide");
        }
        // // currentElement != e.currentTarget && $(e.currentTarget).parent().addClass('main-background')
        const investmentLabel = $(e.currentTarget).find(
          ".investment__data-label"
        );
        currentElement != e.currentTarget &&
          $(investmentLabel).addClass("settings-active");
        // console.log(e.currentTarget);
        $(e.currentTarget)
          .parent()
          .find(".investment__data-collapse")
          .collapse("show");
        if (currentElement != e.currentTarget) {
          currentElement = e.currentTarget;
        } else {
          currentElement = "";
        }
      });
    }
  });

  const surveyObjects = object;

  let i = 0;
  for (const survey of surveyObjects) {
    const surveyContainer = $(`#${survey.category} .survey_container`);
    // // console.log({surveyContainer});
    if (!surveyContainer) {
      // console.log(`category ${survey.category} not correct. NOT FOUND`);
      continue;
    }

    $(surveyContainer).append(`
      <div class="question__box my-4">
        <p class="question" style="margin-bottom: .5rem">${survey.question}</p>
        `);

    const questionBox = $(surveyContainer).find(".question__box").last();
    // // console.log({questionBox});
    if (survey.type === "form_check") {
      for (const answer of survey.answers) {
        i++;
        $(questionBox).append(`
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="${i}">
            <label class="form-check-label" for="${i}">
              ${answer}
            </label>
          </div>
          `);
      }
    } else if (survey.type === "form_select") {
      $(questionBox)
        .append(`<div class="ms-3"><select class="form-select" aria-label="default">
      </select></div>`);
      for (const answer of survey.answers) {
        i++;
        const formSelect = $(questionBox).find(".form-select").last();
        // console.log({ formSelect });
        $(formSelect).append(`<option value="${i}">${answer}</option>`);
      }
    } else {
      // console.log(`type: ${survey.type} is not recognized`);
    }
  }
})();
