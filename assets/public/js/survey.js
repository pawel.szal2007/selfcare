var object = [
  {
    category: "financial_situation",
    // form_check or form_select
    type: "form_check",
    question: "1. Cele i potrzeby finansowe reprezentowanego podmiotu:",
    answers: [
      "inwestowanie nadwyżek finansowych",
      "odsetki, dywidendy i inne regularne dochody z inwestycji",
      "finansowanie potrzeb w zakresie zmiany strategii, struktury kapitałowej oraz łączenia, podziału lub przejęcia przedsiębiorstwa",
      "inne potrzeby finansowe",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "financial_situation",
    // form_check or form_select
    type: "form_check",
    question: "2. Horyzont inwestycyjny reprezentowanego podmiotu:",
    answers: [
      "nie ma preferencji",
      "powyżej 5 lat",
      "3-5 lat",
      "1-3 lata",
      "poniżej 1 roku",
    ],
  },
  {
    category: "financial_situation",
    // form_check or form_select
    type: "form_check",
    question: "3. Źródła przychodów reprezentowanego podmiotu:",
    answers: [
      "działalność gospodarcza lub rolnicza",
      "inne np. subwencje lub dotacje",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "financial_situation",
    // form_check or form_select
    type: "form_select",
    question: "4. Aktywa finansowe reprezentowanego podmiotu:",
    answers: [
      "poniżej 200 tys. zł",
      "200 tys.-1 mln. zł",
      "powyżej 1 mln. zł",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "financial_situation",
    // form_check or form_select
    type: "form_select",
    question: "5. Nastawienie do zysku i ryzyka reprezentowanego podmiotu:",
    answers: [
      "wysokie zyski i ryzyko straty (powyżej 50%)",
      "znaczne zyski i ryzyko straty (25%-50%)",
      "umiarkowane zyski i ryzyko straty (5%-25%)",
      "niewielkie zyski i ryzyko straty (do 5%)",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "financial_situation",
    // form_check or form_select
    type: "form_select",
    question:
      "6. Chcę budować portfel inwestycyjny reprezentowanego podmiotu poprzez instrumenty finansowe o ryzyku wyższym niż wynikający z jego profilu ryzyka (stosuję dywersyfikację portfela):",
    answers: ["tak", "nie"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "7.	Moja praca lub wykształcenie są związane z sektorem finansowym:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "8.	Moje całkowite doświadczenie inwestycyjne:",
    answers: [
      "nie mam",
      "sporadyczne inwestycje (do 10 transakcji)",
      "regularne inwestycje (powyżej 10 transakcji)",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "9.	Moje doświadczenie inwestycyjne związane z instrumentami pochodnymi:",
    answers: [
      "nie mam",
      "sporadyczne inwestycje (do 10 transakcji)",
      "częste inwestycje (10–25 transakcji)",
      "regularne inwestycje (powyżej 25 transakcji)",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "10. Obligacje można sprzedać przed terminem ich wykupu przez emitenta.",
    answers: [
      "nie, zawsze należy doczekać terminu ich wykupu",
      "tak, jeżeli emitent lub inny podmiot jest gotów je kupić",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "11.	Wskaż obligacje o wyższym poziomie ryzyka emitenta:",
    answers: [
      "obligacje Skarbu Państwa",
      "obligacje korporacyjne",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "12.	Cena akcji może spaść poniżej ich ceny emisyjnej:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "13.	Inwestując w akcje, można otrzymać dywidendę:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "14.	Wskaż fundusz inwestycyjny o niższym ryzyku inwestycyjnym:",
    answers: [
      "fundusz obligacji skarbowych",
      "fundusz akcji",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "15.	Fundusze inwestycyjne różnią się oczekiwanym zyskiem i poziomem ryzyka inwestycyjnego:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "16.	Kwota uzyskana ze sprzedaży certyfikatów funduszu zamkniętego notowanych na GPW może być niższa od zainwestowanej kwoty:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "17.	Wysokość świadczenia ubezpieczeniowego z polisy inwestycyjnej:",
    answers: [
      "jest stałą kwotą określoną w polisie",
      " zależy od zysku lub straty z instrumentów finansowych, z którymi polisa inwestycyjna jest powiązana",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "18.	Wypłatę kwoty świadczenia ubezpieczeniowego w wysokości określonej ustawowo w przypadku upadłości firmy ubezpieczeniowej gwarantuje:",
    answers: [
      "Ubezpieczeniowy Fundusz Gwarancyjny",
      "Skarb Państwa",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "19.	Wzrost kursu EUR/PLN dla inwestora, który wcześniej kupił euro, oznacza:",
    answers: ["zysk", "stratę", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "20.	Na cenę produktów strukturyzowanych ma wpływ:",
    answers: [
      "poziom indeksu giełdowego, kursu akcji, ceny surowca, kursu waluty lub innego wskaźnika rynkowego, który stanowi ich instrument bazowy",
      "okres pozostały do wykupu",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "21.	Produkty strukturyzowane zawsze gwarantują zwrot kapitału:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "22.	Zmiana ceny instrumentu bazowego wpływa na wycenę opartego o niego kontraktu terminowego:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "23.	Inwestując w instrumenty pochodne z wbudowaną dźwignią finansową, możesz stracić:",
    answers: [
      "więcej niż wartość zainwestowanych środków własnych",
      "nie więcej niż wartość zainwestowanych środków własnych",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "24.	Wystawienie opcji kupna polega na:",
    answers: [
      "zobowiązaniu się do sprzedaży danego instrumentu bazowego po ustalonej cenie i w określonym terminie, za co otrzymuje się od nabywcy opcji premię",
      "zobowiązaniu się do kupna danego instrumentu bazowego w określonym czasie",
      "nie chcę odpowiadać",
    ],
  },

  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "25.	Ryzyko stopy procentowej to:",
    answers: [
      "ryzyko poniesienia strat w przypadku upadłości kredytobiorcy",
      "ryzyko poniesienia strat w wyniku niekorzystnej zmiany stóp procentowych",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "26. Aby zabezpieczyć się przed spadkiem cen rynkowych wytwarzanych towarów, można zawrzeć:",
    answers: [
      "towarową transakcję pochodną",
      "transakcję sprzedaży obligacji korporacyjnych",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "27. Pieniądze ulokowane na lokacie strukturyzowanej można w każdej chwili wypłacić w bankomacie:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "28.	Lokatę strukturyzowaną można założyć:",
    answers: [
      "w okresie subskrypcji",
      "w każdej chwili",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "30.	Wypłata świadczenia ubezpieczeniowego zależy od:",
    answers: [
      "zgody spadkobierców",
      "zgłoszenia zdarzenia ubezpieczeniowego i złożenia wniosku o wypłatę świadczenia ubezpieczeniowego przez osobę uprawnioną do jego otrzymania",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "31. W ramach usługi przyjmowania i przekazywania zleceń dochodzi do:",
    answers: [
      "realizacji zlecenia przez podmiot, który je przyjął",
      "przekazania zlecenia do wykonania przez inny podmiot np. fundusz, emitenta, wystawcę lub sprzedającego",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "32.	Przedmiotem pierwszej oferty publicznej (IPO) mogą być tylko akcje:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "33.	Warunkiem nabycia prawa do dywidendy jest posiadanie akcji w dniu ustalenia prawa do dywidendy:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "34.	Nabywca instrumentów finansowych notowanych w walucie obcej ponosi:",
    answers: ["ryzyko operacyjne", "ryzyko walutowe", "nie chcę odpowiadać"],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "35.	Wykonywanie zleceń nabycia lub zbycia zagranicznych instrumentów finansowych:",
    answers: [
      "jest dostępne tylko dla nierezydentów",
      "umożliwia składanie zleceń dotyczących instrumentów finansowych w obrocie na rynkach zagranicznych",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "36. Stroną transakcji zawieranej z klientem na rachunek własny firmy inwestycyjnej jest:",
    answers: [
      "inny klient",
      "bank, biuro maklerskie lub inna firma inwestycyjna",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "37.	Usługa wykonywania zleceń nabycia lub zbycia instrumentów pochodnych na tzw. rynku OTC (poza rynkiem regulowanym) polega na:",
    answers: [
      "nabywaniu lub zbywaniu instrumentów pochodnych notowanych na GPW",
      "nabywaniu lub zbywaniu instrumentów pochodnych np. pochodnych instrumentów rynku walutowego, stopy procentowej i towarowych na rynku pozagiełdowym",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "38.	Proponowanie przez emitenta nabycia papierów wartościowych nowej emisji to:",
    answers: [
      "oferowanie instumentów finansowych",
      "doradztwo inwestycyjne",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question: "39.	Wskaż stronę umowy o oferowanie instrumentów finansowych:",
    answers: [
      "emitent lub sprzedający",
      "inwestor zainteresowany zakupem oferowanych instrumentów finansowych",
      "nie chcę odpowiadać",
    ],
  },
  {
    category: "knowledge",
    // form_check or form_select
    type: "form_select",
    question:
      "40.	Firma inwestycyjna może bez zgody klienta korzystać z instrumentów finansowych przechowywanych lub zarejestrowanych na jego rachunku:",
    answers: ["tak", "nie", "nie chcę odpowiadać"],
  },
];
