var questionSelects = $(
  ".question__container .settings__dropdown-group .settings__dropdown-select"
);

var updateSelects = () => {
  questionSelects = $(
    ".question__container .settings__dropdown-group .settings__dropdown-select"
  );
};

var questions = (() => {
  const questions = security_questions_tab();
  return questions;
})();

var removeFromQuestions = (q) => {
  for (const uq of q) {
    for (const q_obj of questions) {
      if (uq.question.includes(q_obj.question)) {
        const index = questions.indexOf(q_obj);
        delete questions.splice(index, 1);
      }
    }
  }
};

var customQuestionUpdate = (e) => {
  const optionText = $(e).val();
  console.log({ optionText });
  const customText = (() => {
    for (const q of questions) {
      if (q.category === "custom") {
        return q.question;
        break;
      }
    }
  })();
  console.log({ customText });
  if (optionText === customText) {
    if (
      $(e)
        .parent()
        .parent()
        .find(`.settings__dropdown-group.answer__block .custom_question-text`)
        .length > 0
    ) {
      console.log("istnieje juz pole z pytaniem customowym, nie dodaje nic");
    } else {
      $(e).parent().parent().find(`.settings__dropdown-group.answer__block`)
        .prepend(`<div class="d-block mb-4 custom__question-div"><span class="settings__dropdown-label custom_question-text">własne pytanie: </span>
      <input class="settings__dropdown-input custom_question-input w-50" type="text" name="" value=""></div>`);
    }
  } else {
    const customQuestionDiv = $(e)
      .parent()
      .parent()
      .find(`.settings__dropdown-group.answer__block .custom__question-div`);
    if (customQuestionDiv.length > 0) {
      $(customQuestionDiv).remove();
    }
  }
};

var addSecurityQuestionsSelects = (questions) => {
  for (const select of questionSelects) {
    $(select).html("");
    questions.forEach((question) => {
      $(select).append(
        `<option class="select-option" data-category="${question.category}">${question.question}</option>`
      );
    });

    $(questionSelects).unbind();
    $(questionSelects).on("change", (e) => {
      customQuestionUpdate(e.currentTarget);
    });
  }
};

var updateUserDefinedSecurityQuestions = (user_questions) => {
  if (
    $(`#questions__settings-content .user__security-questions-defined`)
      .length === 0
  ) {
    $(`#questions__settings-content .question__container`).prepend(
      `<div class="user__security-questions-defined mb-4"><div class="credentials__content py-3"></div></div>`
    );
  } else
    $(
      `#questions__settings-content .user__security-questions-defined .credentials__content`
    ).html("");

  for (let i = 0; i < user_questions.length; i++) {
    $(
      `#questions__settings-content .user__security-questions-defined .credentials__content`
    )
      .append(`<div class="py-2 d-flex justify-content-between align-items-center">
      <span class="question__card-content" data-name="${
        user_questions[i].question
      }">
        ${i + 1}. ${user_questions[i].question}
      </span>
      <div class="btn-group dropstart question__card-content-menu">
        <span id="questionDropdownButton${i}" type="button" class="dropdown-toggle material-icons-outlined menu-icon" data-bs-toggle="dropdown" aria-expanded="false">more_vert</span>
        <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="javascript:void(0)" data-credential="nav-questions-tab" data-bs-toggle="modal" data-bs-target="#staticCredentialsBackdrop">zmień</a></li>
          <li><a class="dropdown-item" href="javascript:void(0)" data-credential="all" data-active="nav-questions-tab" data-bs-toggle="modal" data-bs-target="#staticCredentialsBackdrop">usuń</a></li>
        </ul>
      </div>`);
  }
};

var removeContainersSetLabel = () => {
  const containers = $(".question__container");
  for (var c = 0; c < containers.length - 1; c++) {
    $(containers[c]).remove();
  }
  const label = $(`#questions__settings-content p.settings__dropdown-label`);
  $(label).text(
    `masz już zdefiniowane wymagane minimum 3 pytania, jeśli chcesz możesz dodać kolejne`
  );
};

var questionUpdate = (() => {
  const user_questions = (() => {
    const script = $("#sq_script");
    console.log({ script });
    return $(script).data("questions");
  })();

  console.log({ user_questions });

  if (user_questions.length > 0) {
    updateUserDefinedSecurityQuestions(user_questions);
  }

  if (user_questions.length > 0) {
    removeContainersSetLabel();
    updateSelects();
  }

  removeFromQuestions(user_questions);
  // console.log("pytania po usunięćiu...", { questions });

  addSecurityQuestionsSelects(questions);

  for (const s of questionSelects) {
    customQuestionUpdate(s);
  }

  return questions;
})();

questions = questionUpdate;

var saveQuestions = async (obj) => {
  const data = [];
  for (const select of questionSelects) {
    const category = $(select).find(":selected").data("category");

    let question;
    if (category === "custom") {
      question = $(select)
        .parent()
        .parent()
        .find(
          ".settings__dropdown-group.answer__block .custom__question-div .custom_question-input"
        )
        .val()
        .toLowerCase();
      console.log({ question });

      if (!question) {
        showToast(
          "_danger-no-data",
          '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
          "pytania pomocnicze",
          "musisz uzupełnić swoje własne pytanie!"
        );
        return;
      }
    }

    const answer = $(select)
      .parent()
      .parent()
      .find(
        ".settings__dropdown-group.answer__block .answer__div .answer-input"
      )
      .val()
      .toLowerCase();

    if (!answer) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "pytania pomocnicze",
        "musisz udzielić odpowiedzi na wszystkie pytania!"
      );
      return;
    }

    // put question and answer
    for (const obj of questions) {
      if (obj.category === category) {
        data.push({
          category: category,
          question: category === "custom" ? question : obj.question,
          answer: answer,
        });
        break;
      }
    }
  }

  const duplicates = [];
  for (const dt of data) {
    if (duplicates.includes(dt.question)) {
      showToast(
        "_danger-no-data",
        '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
        "pytania pomocnicze",
        "pytania muszą być różne"
      );
      return;
    }
    duplicates.push(dt.question);
  }

  try {
    const response = await POST("/settings/securityQuestions", data);
    const jsonResponse = JSON.parse(response);
    // console.log({ jsonResponse });

    const res = await GET("/settings/securityQuestions");
    const jsonRes = JSON.parse(res);
    // console.log({ jsonRes });

    showToast(
      "_add-success",
      '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
      "pytania pomocnicze",
      `odpowiedzi zostały zapisane`
    );

    removeContainersSetLabel();

    updateSelects();

    removeFromQuestions(jsonRes);
    // console.log("pytania po usunięćiu...", { questions });
    addSecurityQuestionsSelects(questions);

    updateUserDefinedSecurityQuestions(jsonRes);

    for (const select of questionSelects) {
      customQuestionUpdate(select);
      const answer = $(select)
        .parent()
        .next()
        .find(".settings__dropdown-input")
        .val("");
    }
  } catch (err) {
    // console.log({ err });
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "pytania pomocnicze",
      "serwer odpowiedział kodem błędu... spróbuj ponownie później"
    );
  }
};
