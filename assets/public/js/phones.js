var deletePhone = async (e) => {
  const phone_number = $(e)
    .parent()
    .parent()
    .parent()
    .parent()
    .find(".phone__card-content")
    .data("name");
  // console.log({phone_number});
  const res = await POST("/settings/phones/delete", {
    phone_number: phone_number,
  });
  const jsonRes = JSON.parse(res);

  if (jsonRes.status === 200) {
    showToast(
      "_add-success",
      '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
      "ustawienia",
      `numer telefonu został usunięty`
    );
  } else {
    showToast(
      "_danger-no-data",
      '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
      "ustawienia",
      `numer telefonu nie został usunięty prawidłowo`
    );
  }
  updatePhonesView();
};

var updatePhonesView = async () => {
  const lang = $("nav.navbar .language__active").data("active-language");
  const response = await GET("/settings/phones/get");
  const phones = JSON.parse(response);
  console.log({ phones });

  const phonesContent = $(".phone__number-manager .credentials__content");

  if (phones && phones.length > 0) {
    $(phonesContent).html("");
    for (const phone of phones) {
      $(phonesContent)
        .append(`<div class="py-2 d-flex justify-content-between align-items-center">
        <span class="phone__card-content" data-name="${phone["phone_number"]}">
          ${phone["phone_number"]}
        </span>
        <div class="btn-group dropstart phone__card-content-menu">
          <span id="dropdownMenuButton1" type="button" class="dropdown-toggle material-icons-outlined menu-icon" data-bs-toggle="dropdown" aria-expanded="false">more_vert</span>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="javascript:void(0)" onclick="deletePhone(this)">usuń</a></li>
          </ul>
        </div>
      </div>`);
    }
  } else {
    $(phonesContent).html(
      `<span class="px-4 phone__card-content py-3">${language[lang].phone.empty}</span>`
    );
  }
};

var stopLoading = () => {
  window.stop();
  // window.location.href = "https://selfcare.topid.org/main?lang=pl#settings";
  window.stop();
  // $("#phoneId_iframe_ID").write('<script type="text/undefined">');
};

var addNewPhone = () => {
  let socket = socketConnect();
  socket.onclose = (event) => {
    if (event.wasClean) {
      console.log(
        `[close] Websocket connection closed cleanly, code=${event.code} reason=${event.reason}`
      );
    } else if (event.code === 1006) {
      alert(`websocket connection closed with event code 1006`);
      console.log(`websocket connection closed with event code 1006`);
      socket = socketConnect();
      getWSMessage(socket);
    }
  };
  const getWSMessage = (socket) => {
    socket.onmessage = async (event) => {
      const message = JSON.parse(event.data);
      // console.log({ message });
      if (message.event && message.event === "accept") {
        console.log({ message });
      } else if (message.event === "phoneid") {
        console.log({ message });
        stopLoading();
        $("#phoneId_iframe_ID").remove();
        if (message.status === "done") {
          showToast(
            "_add-success",
            '<svg class="bi flex-shrink-0 me-2 green" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>',
            "ustawienia",
            `numer telefonu został dodany prawidłowo`
          );
        } else if (message.status === "info") {
          showToast(
            "_warning-repeated",
            '<svg class="bi flex-shrink-0 me-2 blue" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>',
            "ustawienia",
            "numer telefonu już istnieje, dodaj inny numer"
          );
        } else {
          showToast(
            "_danger-no-data",
            '<svg class="bi flex-shrink-0 me-2 red" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>',
            "ustawienia",
            `numer telefonu już istnieje lub nie został dodany prawidłowo`
          );
        }
        socket.close(1000);
        updatePhonesView();
      } else socket.close(1000);
    };
  };
  getWSMessage(socket);
};
