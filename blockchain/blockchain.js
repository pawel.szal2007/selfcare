const blockchain_api = require("../configuration").projectVariable
  .BLOCKCHAIN_API;

exports.getConsents = (object) => {
  return new Promise((resolve, reject) => {
    let url = `http://10.204.7.2:49160/api/v1/consent?`;
    for (const property in object) {
      url += `${property}=${object[property]}&`;
    }
    console.log("GET request on: ", { url });
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        // console.log({ err });
        const errObj = err.data;
        console.log({ errObj });
        reject(errObj);
      });
  });
};

exports.getConsentID = (object) => {
  return new Promise((resolve, reject) => {
    let url = `http://10.204.7.2:49160/api/v1/consent?`;
    for (const property in object) {
      url += `${property}=${object[property]}&`;
    }
    console.log("getConsent url:", { url });
    const axios = require("axios");
    axios
      .get(url)
      .then(async (response) => {
        const data = response.data;
        // console.log('getConsent response.data:', {data});
        if (data) {
          let timestamp = 0;
          let consentId;
          for (const consent of data) {
            const consentTimestamp = new Date(consent.CreationDate).getTime();
            if (timestamp < consentTimestamp) {
              timestamp = consentTimestamp;
              consentId = consent.ConsentId;
            }
          }
          if (!consentId)
            reject({ data: "consent not properly found with timestamp" });
          console.log("consent exist", { consentId });
          await putConsentMetadata(consentId);
          resolve(consentId);
        } else {
          console.log("no consent found, creating consent...");
          const consentId = await addConsent(object.userId, object.sourceName);
          resolve(consentId);
        }
      })
      .catch((err) => {
        const errObj = err.data;
        console.log({ errObj });
        reject(errObj);
      });
  });
};

const addConsent = (userId, sourceName) => {
  return new Promise((resolve, reject) => {
    let url = `http://10.204.7.2:49160/api/v1/consent`;
    const data = {
      UserId: userId,
      SourceName: sourceName,
      ValidFrom: new Date(),
      ValidTo: new Date(new Date().getTime() + 100000000000000),
      ExternalConsentId: "string",
      AutocreatedPermissions: true,
      Metadata: "string",
      ActiveNow: true,
    };
    console.log("addConsent post data:", { data });
    const axios = require("axios");
    axios
      .post(url, data)
      .then((response) => {
        const data = response.data;
        console.log("addConsent response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        const errObj = err.data;
        console.log("addConsent response error:", { errObj });
        reject(errObj);
      });
  });
};

const putConsentMetadata = (consentId) => {
  return new Promise((resolve, reject) => {
    let url = `http://10.204.7.2:49160/api/v1/consent/${consentId}/metadata`;
    console.log("put consent metadata url:", { url });
    const axios = require("axios");
    axios
      .put(
        url,
        { Metadata: "updating data" },
        {
          headers: { accept: "text/plain", "Content-Type": "application/json" },
        }
      )
      .then((response) => {
        const data = response.data;
        console.log("putConsentMetadata response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        const errObj = err;
        console.log("putConsentMetadata response error:", { errObj });
        reject(errObj);
      });
  });
};

exports.addDataPackage = (obj) => {
  return new Promise((resolve, reject) => {
    console.log("[DATA PACKAGE POST REQUEST]");
    let url = `http://10.204.7.2:49160/api/v1/dataPackage`;
    const data = JSON.stringify({
      UserId: obj.userId,
      ConsentId: obj.consentId,
      SourceName: obj.sourceName,
      DataFrom: "2021-06-02T13:09:13.821Z",
      DataTo: new Date(new Date().getTime() + 100000000000000).toISOString(),
      ContentHash: obj.dataHash,
      ContentReference: "string",
      Metadata: "string",
    });
    console.log("dataPackage post data: ", { data });
    const axios = require("axios");
    axios
      .post(url, data, { headers: { "Content-Type": "application/json" } })
      .then((response) => {
        const data = response.data;
        // console.log("dataPackage response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        const errObj = err.data;
        console.log("dataPackage response error:", { errObj });
        reject(errObj);
      });
  });
};

const getPermissions = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${blockchain_api.DOMAIN}${blockchain_api.PERMISSIONS_PATH}?`;
    console.log({ url });
    for (const property in obj) {
      url += `${property}=${obj[property]}&`;
    }
    url = url.slice(0, -1);
    // url += "onlyActive=false";
    console.log("GET request on: ", { url });
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        console.log({ err });
        const errObj = err.data;
        console.log({ errObj });
        reject(errObj);
      });
  });
};

const updatePermission = (obj) => {
  return new Promise(async (resolve, reject) => {
    if (obj.permissionId) {
      try {
        const data = await updatePermissionById(obj);
        resolve(data);
      } catch (err) {
        reject(err);
      }
    } else {
      try {
        const response = await getPermissions({
          userId: obj.userId,
          viewerId: obj.viewerId,
          scope: obj.scope,
          permissionStatus: "Suspended",
          onlyActive: false,
        });
        obj.permissionId = response[0].PermissionId;
        const data = await updatePermissionById(obj);
        resolve(data);
      } catch (err) {
        try {
          const data = await addNewPermission(obj);
          resolve(data);
        } catch (err) {
          reject(err);
        }
      }
    }
  });
};

const updatePermissionById = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${blockchain_api.DOMAIN}${blockchain_api.PERMISSIONS_PATH}`;
    if (obj.permissionId) {
      // aktualizacja istniejącej
      if (obj.status) {
        // aktywacja permission o danym id
        url += `/${obj.permissionId}/activate`;
      } else {
        // uniewaznienie permission
        url += `/${obj.permissionId}/suspend`;
      }

      const axios = require("axios");
      axios
        .put(url)
        .then((response) => {
          const data = response.data;
          resolve(data);
        })
        .catch((err) => {
          const errObj = err.data;
          reject(errObj);
        });
    }
  });
};

const addNewPermission = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${blockchain_api.DOMAIN}${blockchain_api.PERMISSIONS_PATH}`;
    const data = JSON.stringify({
      UserId: obj.userId,
      ViewerId: obj.viewerId,
      Scope: obj.scope,
      PermissionType: obj.type,
      ValidFrom: "2021-03-03T19:46:27.141Z",
      ValidTo: new Date(new Date().getTime() + 100000000000000).toISOString(),
      AccessToken: obj.access_token,
      RefreshToken: obj.refresh_token,
      Metadata: "",
    });
    console.log("add permission data: ", { data });
    const axios = require("axios");
    axios
      .post(url, data, { headers: { "Content-Type": "application/json" } })
      .then((response) => {
        // console.log(response);
        const data = response.data;
        // console.log("dataPackage response.data:", { data });
        resolve(data);
      })
      .catch((err) => {
        const errObj = err.data;
        console.log("permission post response error:", { errObj });
        reject(errObj);
      });
  });
};

exports.getAuthorizationHistory = (obj) => {
  return new Promise((resolve, reject) => {
    let url = `${blockchain_api.DOMAIN}${blockchain_api.AUTHORIZATION_HISTORY_PATH}?`;
    // console.log({ url });
    for (const property in obj) {
      url += `${property}=${obj[property]}&`;
    }
    console.log("GET request on: ", { url });
    const axios = require("axios");
    axios
      .get(url)
      .then((response) => {
        const data = response.data;
        // console.log({ data });
        resolve(data);
      })
      .catch((err) => {
        console.log({ err });
        const errObj = err.data;
        console.log({ errObj });
        reject(errObj);
      });
  });
};

exports.addNewPermission = addNewPermission;
exports.getPermissions = getPermissions;
exports.updatePermission = updatePermission;
