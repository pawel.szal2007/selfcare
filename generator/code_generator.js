require("dotenv").config();
const jwt = require("jsonwebtoken");
var LocalStorage = require("node-localstorage").LocalStorage,
  localStorage = new LocalStorage("./generator/codes");

// generate tokens for user to connect external device (local storage as mini database) with 6 digit number
exports.getUserCodeToken = (userID, data, expirationTime = 600) => {
  return new Promise((resolve) => {
    const generator = require("./random_generator");
    let randomNumber = generator.random();
    // const userID = data.user_id;
    console.log({ randomNumber });
    if (localStorage.getItem(userID)) {
      const tokenVerified = jwt.verify(
        localStorage.getItem(userID),
        process.env.SECRET,
        (err, decode) => {
          if (err) {
            console.log("generating new token beacuse of expired...");
            const token = signToken(
              { code: randomNumber, data: data },
              expirationTime
            );
            console.log("token saved in local storage....");
            localStorage.setItem(userID, token);
            resolve(token);
          } else {
            console.log(decode);
            console.log("token is valid...");
            resolve(localStorage.getItem(userID));
          }
        }
      );
    } else {
      console.log("generating new token...");
      const token = signToken(
        { code: randomNumber, data: data },
        expirationTime
      );
      localStorage.setItem(userID, token);
      console.log("token saved in local storage....");
      resolve(token);
    }
  });
};

const signToken = (data, expirationTime) => {
  const token = jwt.sign(data, process.env.SECRET, {
    expiresIn: expirationTime,
  });
  console.log({ token });
  return token;
};

exports.verifyCode = (code) => {
  return new Promise((resolve) => {
    const length = localStorage.length;
    let token;
    for (let i = 0; i < length; i++) {
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);
      console.log({ key });
      console.log({ value });
      if (jwt.decode(value)?.code === code) {
        console.log("==== user found ===");
        token = value;
        localStorage.removeItem(key);
      } else console.log("user not found....");
    }
    resolve(token);
  });
};
