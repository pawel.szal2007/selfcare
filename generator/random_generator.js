exports.random = (number = 6) => {
  if(isNaN(number) || number <= 0) {
    throw new Error(`${number} is not a number/positive number!`)
  }
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
  const random = [];
  for(let i = 0; i < number; i++) {
    random.push(arr[Math.floor(Math.random() * 10)]);
  }
  return random.join("");
}
