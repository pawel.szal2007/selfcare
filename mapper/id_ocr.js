exports.map = (userID, data) => {
  const o = {};
  o["timestamp"] = parseInt(Math.floor(Date.now() / 1000), 10);
  o["user_uid"] = userID;
  o["loa"] = "medium";
  o["method"] = "document_scan";
  data["personal_number"]
    ? (o["personal_number"] = data["personal_number"])
    : (o["personal_number"] = " ");
  data["identity_card_number"]
    ? (o["identity_card_number"] = data["identity_card_number"])
    : (o["identity_card_number"] = " ");
  data["nationality"]
    ? (o["nationality"] = data["nationality"])
    : (o["nationality"] = " ");
  data["place_of_birth"]
    ? (o["place_of_birth"] = data["place_of_birth"])
    : (o["place_of_birth"] = " ");
  data["date_of_issue"]
    ? (o["date_of_issue"] = data["date_of_issue"])
    : (o["date_of_issue"] = " ");
  data["expiry_date"]
    ? (o["expiry_date"] = data["expiry_date"])
    : (o["expiry_date"] = " ");
  data["issuing_authority"]
    ? (o["issuing_authority"] = data["issuing_authority"])
    : (o["issuing_authority"] = " ");
  data["surname"] ? (o["surname"] = data["surname"]) : (o["surname"] = " ");
  data["given_names"]
    ? (o["given_names"] = data["given_names"])
    : (o["given_names"] = " ");
  data["date_of_birth"]
    ? (o["date_of_birth"] = data["date_of_birth"])
    : (o["date_of_birth"] = " ");
  data["sex"] ? (o["sex"] = data["sex"]) : (o["sex"] = " ");
  return o;
};
